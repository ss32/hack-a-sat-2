# -*- coding: utf-8 -*-
"""
Created on Sat Jun 26 18:25:06 2021

@author: mason
"""
import telnetlib
import re

HOST ="main-fleet.satellitesabove.me"
PORT = "5005"
ticket = "ticket{lima965849delta2:GDjgyHFh4IGgirsU5q-WUfvRL1lQLYwt2TxMs1lHPvM0onkGt_7o7fo_1axGME4UVw}"

tn=telnetlib.Telnet(HOST,PORT, timeout = 1)
tn.read_until(b"Ticket please:",timeout=1).decode('ascii')
tn.write(ticket.encode("ascii")+ b"\n")

# Update the below with your inputs
tn.read_until(b"(Format answers as a single float):",timeout=1).decode('ascii')
tn.write(b"0.369\n")

#tn.write(b"r:0,0,0\n")

dat=tn.read_until(b">",timeout=1).decode('ascii')
print("dat=")
print(dat)

posX = float(re.findall(r'\+X-Axis:(\d+.\d+|\d{1,1})',dat)[0])
posY = float(re.findall(r'\+Y-Axis:(\d+.\d+|\d{1,1})',dat)[0])
posZ = float(re.findall(r'\+Z-Axis:(\d+.\d+|\d{1,1})',dat)[0])
negX = float(re.findall(r'-X-Axis:(\d+.\d+|\d{1,1})',dat)[0])
negY = float(re.findall(r'-Y-Axis:(\d+.\d+|\d{1,1})',dat)[0])
negZ = float(re.findall(r'-Z-Axis:(\d+.\d+|\d{1,1})',dat)[0])

sensorVolts = [posX,posY,posZ,negX,negY,negZ]
print(sensorVolts)

