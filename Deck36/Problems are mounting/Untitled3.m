flux = 1360; %W/m^2
area = 1; % in cm^2
area_m2 = area * (1/100)^2
resistance = 10; % ohms
power = flux *.1 * area_m2
voltage = sqrt(power * resistance) % P=IV => V = P/I, I = V/R, V = sqrt(P * R)