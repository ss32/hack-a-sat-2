# -*- coding: utf-8 -*-
"""
Created on Sat Jun 26 18:25:06 2021

@author: mason
"""
import telnetlib
import re
import math 

HOST ="main-fleet.satellitesabove.me"
PORT = "5005"
ticket = "ticket{lima965849delta2:GDjgyHFh4IGgirsU5q-WUfvRL1lQLYwt2TxMs1lHPvM0onkGt_7o7fo_1axGME4UVw}"

tn=telnetlib.Telnet(HOST,PORT, timeout = 1)
tn.read_until(b"Ticket please:",timeout=1).decode('ascii')
tn.write(ticket.encode("ascii")+ b"\n")

# Update the below with your inputs
tn.read_until(b"(Format answers as a single float):",timeout=1).decode('ascii')
tn.write(b"0.369\n")

volt_list = []

# Get baseline
dat=tn.read_until(b">",timeout=1).decode('ascii')
dat=dat+tn.read_until(b">",timeout=1).decode('ascii')
print("dat=")
print(dat)
posX = float(re.findall(r'\+X-Axis:(\d+.\d+|\d{1,1})',dat)[0])
posY = float(re.findall(r'\+Y-Axis:(\d+.\d+|\d{1,1})',dat)[0])
posZ = float(re.findall(r'\+Z-Axis:(\d+.\d+|\d{1,1})',dat)[0])
negX = float(re.findall(r'-X-Axis:(\d+.\d+|\d{1,1})',dat)[0])
negY = float(re.findall(r'-Y-Axis:(\d+.\d+|\d{1,1})',dat)[0])
negZ = float(re.findall(r'-Z-Axis:(\d+.\d+|\d{1,1})',dat)[0])
sensorVolts = [posX,posY,posZ,negX,negY,negZ]
print("baseline = ")
print(sensorVolts)
volt_list.append(sensorVolts)


tn.write(b"r:90,0,0\n")    
dat=tn.read_until(b">",timeout=1).decode('ascii')
dat=dat+tn.read_until(b">",timeout=1).decode('ascii')
print("dat=")
print(dat)    
posX = float(re.findall(r'\+X-Axis:(\d+.\d+|\d{1,1})',dat)[0])
posY = float(re.findall(r'\+Y-Axis:(\d+.\d+|\d{1,1})',dat)[0])
posZ = float(re.findall(r'\+Z-Axis:(\d+.\d+|\d{1,1})',dat)[0])
negX = float(re.findall(r'-X-Axis:(\d+.\d+|\d{1,1})',dat)[0])
negY = float(re.findall(r'-Y-Axis:(\d+.\d+|\d{1,1})',dat)[0])
negZ = float(re.findall(r'-Z-Axis:(\d+.\d+|\d{1,1})',dat)[0])
sensorVolts = [posX,posY,posZ,negX,negY,negZ]        
print(sensorVolts)    
volt_list.append(sensorVolts)
    
tn.write(b"r:180,0,0\n")    
dat=tn.read_until(b">",timeout=1).decode('ascii')
dat=dat+tn.read_until(b">",timeout=1).decode('ascii')
print("dat=")
print(dat)    
posX = float(re.findall(r'\+X-Axis:(\d+.\d+|\d{1,1})',dat)[0])
posY = float(re.findall(r'\+Y-Axis:(\d+.\d+|\d{1,1})',dat)[0])
posZ = float(re.findall(r'\+Z-Axis:(\d+.\d+|\d{1,1})',dat)[0])
negX = float(re.findall(r'-X-Axis:(\d+.\d+|\d{1,1})',dat)[0])
negY = float(re.findall(r'-Y-Axis:(\d+.\d+|\d{1,1})',dat)[0])
negZ = float(re.findall(r'-Z-Axis:(\d+.\d+|\d{1,1})',dat)[0])
sensorVolts = [posX,posY,posZ,negX,negY,negZ]        
print(sensorVolts)    
volt_list.append(sensorVolts)

tn.write(b"r:270,0,0\n")    
dat=tn.read_until(b">",timeout=1).decode('ascii')
dat=dat+tn.read_until(b">",timeout=1).decode('ascii')
print("dat=")
print(dat)    
posX = float(re.findall(r'\+X-Axis:(\d+.\d+|\d{1,1})',dat)[0])
posY = float(re.findall(r'\+Y-Axis:(\d+.\d+|\d{1,1})',dat)[0])
posZ = float(re.findall(r'\+Z-Axis:(\d+.\d+|\d{1,1})',dat)[0])
negX = float(re.findall(r'-X-Axis:(\d+.\d+|\d{1,1})',dat)[0])
negY = float(re.findall(r'-Y-Axis:(\d+.\d+|\d{1,1})',dat)[0])
negZ = float(re.findall(r'-Z-Axis:(\d+.\d+|\d{1,1})',dat)[0])
sensorVolts = [posX,posY,posZ,negX,negY,negZ]        
print(sensorVolts)    
volt_list.append(sensorVolts)
    

tn.write(b"r:0,90,0\n")    
dat=tn.read_until(b">",timeout=1).decode('ascii')
dat=dat+tn.read_until(b">",timeout=1).decode('ascii')
print("dat=")
print(dat)    
posX = float(re.findall(r'\+X-Axis:(\d+.\d+|\d{1,1})',dat)[0])
posY = float(re.findall(r'\+Y-Axis:(\d+.\d+|\d{1,1})',dat)[0])
posZ = float(re.findall(r'\+Z-Axis:(\d+.\d+|\d{1,1})',dat)[0])
negX = float(re.findall(r'-X-Axis:(\d+.\d+|\d{1,1})',dat)[0])
negY = float(re.findall(r'-Y-Axis:(\d+.\d+|\d{1,1})',dat)[0])
negZ = float(re.findall(r'-Z-Axis:(\d+.\d+|\d{1,1})',dat)[0])
sensorVolts = [posX,posY,posZ,negX,negY,negZ]        
print(sensorVolts)    
volt_list.append(sensorVolts)
    
tn.write(b"r:0,180,0\n")    
dat=tn.read_until(b">",timeout=1).decode('ascii')
dat=dat+tn.read_until(b">",timeout=1).decode('ascii')
print("dat=")
print(dat)    
posX = float(re.findall(r'\+X-Axis:(\d+.\d+|\d{1,1})',dat)[0])
posY = float(re.findall(r'\+Y-Axis:(\d+.\d+|\d{1,1})',dat)[0])
posZ = float(re.findall(r'\+Z-Axis:(\d+.\d+|\d{1,1})',dat)[0])
negX = float(re.findall(r'-X-Axis:(\d+.\d+|\d{1,1})',dat)[0])
negY = float(re.findall(r'-Y-Axis:(\d+.\d+|\d{1,1})',dat)[0])
negZ = float(re.findall(r'-Z-Axis:(\d+.\d+|\d{1,1})',dat)[0])
sensorVolts = [posX,posY,posZ,negX,negY,negZ]        
print(sensorVolts)    
volt_list.append(sensorVolts)

tn.write(b"r:0,270,0\n")    
dat=tn.read_until(b">",timeout=1).decode('ascii')
dat=dat+tn.read_until(b">",timeout=1).decode('ascii')
print("dat=")
print(dat)    
posX = float(re.findall(r'\+X-Axis:(\d+.\d+|\d{1,1})',dat)[0])
posY = float(re.findall(r'\+Y-Axis:(\d+.\d+|\d{1,1})',dat)[0])
posZ = float(re.findall(r'\+Z-Axis:(\d+.\d+|\d{1,1})',dat)[0])
negX = float(re.findall(r'-X-Axis:(\d+.\d+|\d{1,1})',dat)[0])
negY = float(re.findall(r'-Y-Axis:(\d+.\d+|\d{1,1})',dat)[0])
negZ = float(re.findall(r'-Z-Axis:(\d+.\d+|\d{1,1})',dat)[0])
sensorVolts = [posX,posY,posZ,negX,negY,negZ]        
print(sensorVolts)    
volt_list.append(sensorVolts)


tn.write(b"r:0,0,90\n")    
dat=tn.read_until(b">",timeout=1).decode('ascii')
dat=dat+tn.read_until(b">",timeout=1).decode('ascii')
print("dat=")
print(dat)    
posX = float(re.findall(r'\+X-Axis:(\d+.\d+|\d{1,1})',dat)[0])
posY = float(re.findall(r'\+Y-Axis:(\d+.\d+|\d{1,1})',dat)[0])
posZ = float(re.findall(r'\+Z-Axis:(\d+.\d+|\d{1,1})',dat)[0])
negX = float(re.findall(r'-X-Axis:(\d+.\d+|\d{1,1})',dat)[0])
negY = float(re.findall(r'-Y-Axis:(\d+.\d+|\d{1,1})',dat)[0])
negZ = float(re.findall(r'-Z-Axis:(\d+.\d+|\d{1,1})',dat)[0])
sensorVolts = [posX,posY,posZ,negX,negY,negZ]        
print(sensorVolts)    
volt_list.append(sensorVolts)
    
tn.write(b"r:0,0,180\n")    
dat=tn.read_until(b">",timeout=1).decode('ascii')
dat=dat+tn.read_until(b">",timeout=1).decode('ascii')
print("dat=")
print(dat)    
posX = float(re.findall(r'\+X-Axis:(\d+.\d+|\d{1,1})',dat)[0])
posY = float(re.findall(r'\+Y-Axis:(\d+.\d+|\d{1,1})',dat)[0])
posZ = float(re.findall(r'\+Z-Axis:(\d+.\d+|\d{1,1})',dat)[0])
negX = float(re.findall(r'-X-Axis:(\d+.\d+|\d{1,1})',dat)[0])
negY = float(re.findall(r'-Y-Axis:(\d+.\d+|\d{1,1})',dat)[0])
negZ = float(re.findall(r'-Z-Axis:(\d+.\d+|\d{1,1})',dat)[0])
sensorVolts = [posX,posY,posZ,negX,negY,negZ]        
print(sensorVolts)    
volt_list.append(sensorVolts)

tn.write(b"r:0,0,270\n")    
dat=tn.read_until(b">",timeout=1).decode('ascii')
dat=dat+tn.read_until(b">",timeout=1).decode('ascii')
print("dat=")
print(dat)    
posX = float(re.findall(r'\+X-Axis:(\d+.\d+|\d{1,1})',dat)[0])
posY = float(re.findall(r'\+Y-Axis:(\d+.\d+|\d{1,1})',dat)[0])
posZ = float(re.findall(r'\+Z-Axis:(\d+.\d+|\d{1,1})',dat)[0])
negX = float(re.findall(r'-X-Axis:(\d+.\d+|\d{1,1})',dat)[0])
negY = float(re.findall(r'-Y-Axis:(\d+.\d+|\d{1,1})',dat)[0])
negZ = float(re.findall(r'-Z-Axis:(\d+.\d+|\d{1,1})',dat)[0])
sensorVolts = [posX,posY,posZ,negX,negY,negZ]        
print(sensorVolts)    
volt_list.append(sensorVolts)

print("\n Val list:")
print("Base",[float(("%.08f" % e)) for e in volt_list[0]])
print("X090",[float(("%.08f" % e)) for e in volt_list[1]])
print("X180",[float(("%.08f" % e)) for e in volt_list[2]])
print("X270",[float(("%.08f" % e)) for e in volt_list[3]])
print("Base",[float(("%.08f" % e)) for e in volt_list[0]])
print("Y090",[float(("%.08f" % e)) for e in volt_list[4]])
print("Y180",[float(("%.08f" % e)) for e in volt_list[5]])
print("Y270",[float(("%.08f" % e)) for e in volt_list[6]])
print("Base",[float(("%.08f" % e)) for e in volt_list[0]])
print("Z090",[float(("%.08f" % e)) for e in volt_list[7]])
print("Z180",[float(("%.08f" % e)) for e in volt_list[8]])
print("Z270",[float(("%.08f" % e)) for e in volt_list[9]])

face_index = 0
if volt_list[0][0]==volt_list[1][0] and volt_list[0][0]==volt_list[2][0]:
    print("not +X")
else:
    print("is +X")
    face_index = 0

if volt_list[0][1]==volt_list[4][1] and volt_list[0][1]==volt_list[5][1]:
    print("not +Y")
else:
    print("is +Y")
    face_index = 1
    
if volt_list[0][2]==volt_list[7][2] and volt_list[0][2]==volt_list[8][2]:
    print("not +Z")
else:
    print("is +Z")
    face_index = 2
    
if volt_list[0][3]==volt_list[1][3] and volt_list[0][3]==volt_list[2][3]:
    print("not -X")
else:
    print("is -X")
    face_index = 3

if volt_list[0][4]==volt_list[4][4] and volt_list[0][4]==volt_list[5][4]:
    print("not -Y")
else:
    print("is -Y")
    face_index = 4
    
if volt_list[0][5]==volt_list[7][5] and volt_list[0][5]==volt_list[8][5]:
    print("not -Z")
else:
    print("is -Z")
    face_index = 5
    

def find_power_list(input_list):
    power_list = []
    for a1 in range(0,len(input_list)):
        power_list.append(pow(input_list[a1],2) / resistance)
    return power_list
    
# Now let's figure out by how much the bad sensor deviates
flux = 1360.0
area = 1.0
area_m2 = area * pow((1.0/100.0),2)
resistance = 10.0
power = flux * .1 * area_m2
voltage = math.sqrt(power * resistance)



def apply_angle(ang_in):    
    tn.write(b"r:" + str.encode(str(ang_in)[1:-1]) + b"\n")    
    dat=tn.read_until(b">",timeout=1).decode('ascii')
    dat=dat+tn.read_until(b">",timeout=1).decode('ascii')
    #print("dat=")
    #print(dat)    
    posX = float(re.findall(r'\+X-Axis:(\d+.\d+|\d{1,1})',dat)[0])
    posY = float(re.findall(r'\+Y-Axis:(\d+.\d+|\d{1,1})',dat)[0])
    posZ = float(re.findall(r'\+Z-Axis:(\d+.\d+|\d{1,1})',dat)[0])
    negX = float(re.findall(r'-X-Axis:(\d+.\d+|\d{1,1})',dat)[0])
    negY = float(re.findall(r'-Y-Axis:(\d+.\d+|\d{1,1})',dat)[0])
    negZ = float(re.findall(r'-Z-Axis:(\d+.\d+|\d{1,1})',dat)[0])
    sensorVolts = [posX,posY,posZ,negX,negY,negZ]        
    #print(sensorVolts)    
    return sensorVolts

# def peak_angles():
#     ang = []
    
# sensor_volt_list = []    
# if face_index == 1:
#     for a1 in range(0,360):
#         ang_in = [a1,0,0]
#         sensor_volt_list.append(apply_angle(ang_in)[1])
    
#     max_val_ind_a1 = sensor_volt_list.index(max(sensor_volt_list))   
    
#     sensor_volt_list = []
#     for a3 in range(0,360):
#         ang_in = [max_val_ind_a1,0,a3]
#         sensor_volt_list.append(apply_angle(ang_in)[1])
        
#     max_val_ind_a3 = sensor_volt_list.index(max(sensor_volt_list)) 
    
#     print('a1=' + str(max_val_ind_a1) + ' a3=' +  str(max_val_ind_a3))


#     a1 = max_val_ind_a1 * (math.pi/180)
#     a3 = max_val_ind_a3 * (math.pi/180)
#     x = math.cos(a3 + (math.pi/2))#math.cos(el) * math.cos(az)
#     y = math.cos(a3) * math.cos(a1)
#     z = math.sin(a1) * math.cos(a3)
#     unit_vec = [x,y,z]
#     print(unit_vec)
    
#     write_bytes = b"s:" + str.encode(str(unit_vec)[1:-1]) + b"\n"
#     tn.write(write_bytes)    
#     dat=tn.read_until(b">",timeout=1).decode('ascii')
#     print(dat)
    
power_base = find_power_list(volt_list[face_index])
power0 = find_power_list(volt_list[0])
power5 = find_power_list(volt_list[5])
ang_about_z = ((math.acos(power0[1]/power) - math.acos(power5[1]/power)) / 2) * (180/math.pi)

power4 = find_power_list(volt_list[4])
power6 = find_power_list(volt_list[6])
ang_about_x = ((math.acos(power4[1]/power) - math.acos(power6[1]/power)) / 2) * (180/math.pi)

a1 = ang_about_x * (math.pi/180)
a3 = ang_about_z * (math.pi/180)
x = math.cos(a3 + (math.pi/2))#math.cos(el) * math.cos(az)
y = math.cos(a3) * math.cos(a1)
z = math.sin(a1) * math.cos(a3)
unit_vec = [x,y,z]
print(unit_vec)

write_bytes = b"s:" + str.encode(str(unit_vec)[1:-1]) + b"\n"
tn.write(write_bytes)    
dat=tn.read_until(b">",timeout=1).decode('ascii')
print(dat)