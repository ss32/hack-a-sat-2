import numpy as np
import sys, time

import telnetlib
import re
import math 

def getVolts(tn):
  
  ticket = "ticket{lima965849delta2:GDjgyHFh4IGgirsU5q-WUfvRL1lQLYwt2TxMs1lHPvM0onkGt_7o7fo_1axGME4UVw}"

  tn.read_until(b"Ticket please:",timeout=1).decode('ascii')
  tn.write(ticket.encode("ascii")+ b"\n")

  # Update the below with your inputs
  tn.read_until(b"(Format answers as a single float):",timeout=1).decode('ascii')
  tn.write(b"0.369\n")

  volt_list = []

  # Get baseline
  dat=tn.read_until(b">",timeout=1).decode('ascii')
  dat=dat+tn.read_until(b">",timeout=1).decode('ascii')
  print("dat=")
  print(dat)
  posX = float(re.findall(r'\+X-Axis:(\d+.\d+|\d{1,1})',dat)[0])
  posY = float(re.findall(r'\+Y-Axis:(\d+.\d+|\d{1,1})',dat)[0])
  posZ = float(re.findall(r'\+Z-Axis:(\d+.\d+|\d{1,1})',dat)[0])
  negX = float(re.findall(r'-X-Axis:(\d+.\d+|\d{1,1})',dat)[0])
  negY = float(re.findall(r'-Y-Axis:(\d+.\d+|\d{1,1})',dat)[0])
  negZ = float(re.findall(r'-Z-Axis:(\d+.\d+|\d{1,1})',dat)[0])
  sensorVolts = [posX,posY,posZ,negX,negY,negZ]
  print("baseline = ")
  print(sensorVolts)
  volt_list.append(sensorVolts)


  tn.write(b"r:180,0,0\n")    
  dat=tn.read_until(b">",timeout=1).decode('ascii')
  dat=dat+tn.read_until(b">",timeout=1).decode('ascii')
  print("dat=")
  print(dat)    
  posX = float(re.findall(r'\+X-Axis:(\d+.\d+|\d{1,1})',dat)[0])
  posY = float(re.findall(r'\+Y-Axis:(\d+.\d+|\d{1,1})',dat)[0])
  posZ = float(re.findall(r'\+Z-Axis:(\d+.\d+|\d{1,1})',dat)[0])
  negX = float(re.findall(r'-X-Axis:(\d+.\d+|\d{1,1})',dat)[0])
  negY = float(re.findall(r'-Y-Axis:(\d+.\d+|\d{1,1})',dat)[0])
  negZ = float(re.findall(r'-Z-Axis:(\d+.\d+|\d{1,1})',dat)[0])
  sensorVolts = [posX,posY,posZ,negX,negY,negZ]        
  print(sensorVolts)    
  volt_list.append(sensorVolts)
  
  tn.write(b"r:0,180,0\n")    
  dat=tn.read_until(b">",timeout=1).decode('ascii')
  dat=dat+tn.read_until(b">",timeout=1).decode('ascii')
  print("dat=")
  print(dat)    
  posX = float(re.findall(r'\+X-Axis:(\d+.\d+|\d{1,1})',dat)[0])
  posY = float(re.findall(r'\+Y-Axis:(\d+.\d+|\d{1,1})',dat)[0])
  posZ = float(re.findall(r'\+Z-Axis:(\d+.\d+|\d{1,1})',dat)[0])
  negX = float(re.findall(r'-X-Axis:(\d+.\d+|\d{1,1})',dat)[0])
  negY = float(re.findall(r'-Y-Axis:(\d+.\d+|\d{1,1})',dat)[0])
  negZ = float(re.findall(r'-Z-Axis:(\d+.\d+|\d{1,1})',dat)[0])
  sensorVolts = [posX,posY,posZ,negX,negY,negZ]        
  print(sensorVolts)    
  volt_list.append(sensorVolts)

  tn.write(b"r:0,0,180\n")    
  dat=tn.read_until(b">",timeout=1).decode('ascii')
  dat=dat+tn.read_until(b">",timeout=1).decode('ascii')
  print("dat=")
  print(dat)    
  posX = float(re.findall(r'\+X-Axis:(\d+.\d+|\d{1,1})',dat)[0])
  posY = float(re.findall(r'\+Y-Axis:(\d+.\d+|\d{1,1})',dat)[0])
  posZ = float(re.findall(r'\+Z-Axis:(\d+.\d+|\d{1,1})',dat)[0])
  negX = float(re.findall(r'-X-Axis:(\d+.\d+|\d{1,1})',dat)[0])
  negY = float(re.findall(r'-Y-Axis:(\d+.\d+|\d{1,1})',dat)[0])
  negZ = float(re.findall(r'-Z-Axis:(\d+.\d+|\d{1,1})',dat)[0])
  sensorVolts = [posX,posY,posZ,negX,negY,negZ]        
  print(sensorVolts)    
  volt_list.append(sensorVolts)

  return volt_list

def sendAnswer(tn, answer):
  tn.write((answer + '\n').encode("ascii")+ b"\n")


# Rotation about X-axis (phi)
def L1(angle):
  T = np.zeros((3,3))
  T[0,0] = 1.0
  T[1,1] = np.cos(angle)
  T[1,2] = np.sin(angle)
  T[2,1] = -np.sin(angle)
  T[2,2] = np.cos(angle)
  return T

# Rotation about Y-axis (theta)
def L2(angle):
  T = np.zeros((3,3))
  T[0,0] = np.cos(angle)
  T[0,2] = -np.sin(angle)
  T[1,1] = 1.0
  T[2,0] = np.sin(angle)
  T[2,2] = np.cos(angle)
  return T

# Rotation about Z-axis (psi)
def L3(angle):
  T = np.zeros((3,3))
  T[0,0] = np.cos(angle)
  T[0,1] = np.sin(angle)
  T[1,0] = -np.sin(angle)
  T[1,1] = np.cos(angle)
  T[2,2] = 1.0;
  return T

# Compute body-frame transform matrix given Euler angles.
def T(phi, theta, psi):
  return np.matmul(L1(phi), np.matmul(L2(theta), L3(psi)))

# Help function to computer sensor vector norms.
def Sensor(which, x):

  phi   = x[0]
  theta = x[1]
  psi   = x[2]

  if which == "Xp":
    sensor = T(phi, theta, psi).dot(np.array([ 1, 0, 0]))
  elif which == "Yp":
    sensor = T(phi, theta, psi).dot(np.array([ 0, 1, 0]))
  elif which == "Zp":
    sensor = T(phi, theta, psi).dot(np.array([ 0, 0, 1]))
  elif which == "Xn":
    sensor = T(phi, theta, psi).dot(np.array([-1, 0, 0]))
  elif which == "Yn":
    sensor = T(phi, theta, psi).dot(np.array([ 0,-1, 0]))
  elif which == "Zn":
    sensor = T(phi, theta, psi).dot(np.array([ 0, 0,-1]))

  return sensor


# Cost function.
def cost(x, V, which):

  ### SUNVEC ###
  SUNVEC = np.array([0.369, 0, 0]) # Assume initially aligned with X+ axis

  # Here 'x' is vector of unknown parameters.
  phi     = x[0] * np.pi / 180.0
  theta   = x[1] * np.pi / 180.0
  psi     = x[2] * np.pi / 180.0
  s_phi   = x[3] * np.pi / 180.0
  s_theta = x[4] * np.pi / 180.0
  s_psi   = x[5] * np.pi / 180.0

  # "Perfect" sensor vectors.
  Xp = np.array([ 1, 0, 0])
  Yp = np.array([ 0, 1, 0])
  Zp = np.array([ 0, 0, 1])
  Xn = np.array([-1, 0, 0])
  Yn = np.array([ 0,-1, 0])
  Zn = np.array([ 0, 0,-1])

  # Sensor corruption.
  if which == "Xp":
    Xp = Sensor("Xp", np.array([s_phi, s_theta, s_psi]))
  elif which == "Yp":
    Yp = Sensor("Yp", np.array([s_phi, s_theta, s_psi]))
  elif which == "Zp":
    Zp = Sensor("Zp", np.array([s_phi, s_theta, s_psi]))
  elif which == "Xn":
    Xn = Sensor("Xn", np.array([s_phi, s_theta, s_psi]))
  elif which == "Yn":
    Yn = Sensor("Yn", np.array([s_phi, s_theta, s_psi]))
  elif which == "Zn":
    Zn = Sensor("Zn", np.array([s_phi, s_theta, s_psi]))

  # Empty residuals vector.
  residuals = np.zeros(len(V))

  # Initial sun vector rotation.
  A = T(phi, theta, psi).dot(SUNVEC)
  residuals[ 0] = max(Xp.dot(A), 0.0) - V[ 0]
  residuals[ 1] = max(Yp.dot(A), 0.0) - V[ 1]
  residuals[ 2] = max(Zp.dot(A), 0.0) - V[ 2]
  residuals[ 3] = max(Xn.dot(A), 0.0) - V[ 3]
  residuals[ 4] = max(Yn.dot(A), 0.0) - V[ 4]
  residuals[ 5] = max(Zn.dot(A), 0.0) - V[ 5]

  # First new orientation.
  A = T(np.pi, 0, 0).dot(T(phi, theta, psi).dot(SUNVEC))
  residuals[ 6] = max(Xp.dot(A), 0.0) - V[ 6]
  residuals[ 7] = max(Yp.dot(A), 0.0) - V[ 7]
  residuals[ 8] = max(Zp.dot(A), 0.0) - V[ 8]
  residuals[ 9] = max(Xn.dot(A), 0.0) - V[ 9]
  residuals[10] = max(Yn.dot(A), 0.0) - V[10]
  residuals[11] = max(Zn.dot(A), 0.0) - V[11]

  # Second new orientation.
  A = T(0, np.pi, 0).dot(T(phi, theta, psi).dot(SUNVEC))
  residuals[12] = max(Xp.dot(A), 0.0) - V[12]
  residuals[13] = max(Yp.dot(A), 0.0) - V[13]
  residuals[14] = max(Zp.dot(A), 0.0) - V[14]
  residuals[15] = max(Xn.dot(A), 0.0) - V[15]
  residuals[16] = max(Yn.dot(A), 0.0) - V[16]
  residuals[17] = max(Zn.dot(A), 0.0) - V[17]

  # Third new orientation.
  A = T(0, 0, np.pi).dot(T(phi, theta, psi).dot(SUNVEC))
  residuals[18] = max(Xp.dot(A), 0.0) - V[18]
  residuals[19] = max(Yp.dot(A), 0.0) - V[19]
  residuals[20] = max(Zp.dot(A), 0.0) - V[20]
  residuals[21] = max(Xn.dot(A), 0.0) - V[21]
  residuals[22] = max(Yn.dot(A), 0.0) - V[22]
  residuals[23] = max(Zn.dot(A), 0.0) - V[23]

  return residuals


if __name__ == "__main__":

  from scipy.optimize import least_squares as lsq

  flatten = lambda t: [item for sublist in t for item in sublist]

  HOST ="main-fleet.satellitesabove.me"
  PORT = "5005"

  tn=telnetlib.Telnet(HOST,PORT, timeout = 1)

  # Vector of known measurements (Volts).
  Volts_ = np.array([
    [0.33940291106353637, 0.06696668481120184, 0.3210367573404211, 0, 0, 0],                 # Initial rotation
    [0, 0, 0, 0, 0.06696668481120188, 0.3210367573404211],                                   # New rotation: +180 about X
    [0, 0.06696668481120184, 0, 0.16921805357566577, 0, 0.3210367573404211],                 # New rotation: +180 about Y
    [0.16267587382123014, 0, 0.3210367573404211, 0.1692180535756658, 0.06696668481120183, 0] # New rotation: +180 about Z
  ])

  Volts_ = np.array([
    [0.22896128671559063, 0.33631745196492613, 0.1503469168446423, 0, 0, 0],
    [0, 0, 0, 0, 0.33631745196492613, 0.15034691684464227],
    [0, 0.33631745196492613, 0, 0.021685529583616994, 0, 0.1503469168446423],
    [0.06058776528229933, 0, 0.1503469168446423, 0.021685529583617053, 0.33631745196492613, 0]
  ])

  Volts_ = np.array([
    [0.2501218108422222,0.200404094682168,0.34028939919665663,0,0,0],
    [0.2501218108422222,0,0,0,0.20040409468216802,0.18292393137536814],
    [0,0.200404094682168,0,0.2501218108422222,0,0.1829239313753682],
    [0,0,0,0.2501218108422222,0.20040409468216797,0]
  ])

  Volts = getVolts(tn)

  V = flatten(Volts)

  # Staring guess for initial Euler angles and displacement (degrees)
  x0 = np.array([ 0.0, 85.0, -65.0,  # Initial rotation
                  0.0,  0.0,   0.0]) # Sensor displacement

  # Euler angle bounds.
  bounds = ([-180.0, -180.0, -180.0, -90.0, -90.0, -90.0], 
            [ 180.0,  180.0,  180.0,  90.0,  90.0,  90.0])

  # Run optimizer for all possible sensors.
  sensors = ['Xp', 'Yp', 'Zp', 'Xn', 'Yn', 'Zn']
  costOut = []
  for sensor in sensors:
    result = lsq(lambda x: cost(x,V,sensor), x0, bounds=bounds, verbose=0)
    costOut.append(result.cost)    
    print("Assumed Bad Sensor:", sensor)
    print("Calculated Cost:", result.cost)
    ans = Sensor(sensor, result.x[3:]*np.pi/180.0)
    print("Sensor Norm: s:{:f},{:f},{:f}\n".format(ans[0], ans[1], ans[2]))
    
  # Get minimum cost, and recompute answer to form response.
  which = int(np.argmin(costOut))
  result = lsq(lambda x: cost(x,V,sensors[which]), x0, bounds=bounds, verbose=0)
  ans = Sensor(sensors[which], result.x[3:]*np.pi/180.0)
  answer = "s:{:.2f},{:.2f},{:.2f}\n".format(ans[0], ans[1], ans[2])
  print(answer)
  sendAnswer(tn, answer)
  time.sleep(1)

  # Read response.
  print(tn.read_all().decode('ascii'))



