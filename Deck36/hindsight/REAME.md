## Revenge of the space book!

Due to budget cutbacks, the optical sensor on your satellite was made with parts from the flea market. This sensor is terrible at finding stars in the field of view. Readings of individual positions of stars is very noisy. To make matters worse, the sensor can't even measure magnitudes of stars either.

Budget cutbacks aside, make your shareholders proud by identifying which stars the sensor is looking at based on the provided star catalog.

### Connecting

Connect to the challenge on:
early-motive.satellitesabove.me:5002

Using netcat, you might run:
nc early-motive.satellitesabove.me 5002
Files

### You'll need these files to solve the challenge.

https://static.2021.hackasat.com/hlvjekbpsfaw66nlrzehak730d3g


