## Quaternion

###Connecting

Connect to the challenge on:
grave-error.satellitesabove.me:5020

Using netcat, you might run:
nc grave-error.satellitesabove.me 5020 

```
            QUATERNION                 
            CHALLANGE                  
                                       
               z                       
               |                       
             __|____                   
            /  |   /|                  
  ______   /______/ |    ______        
 |      |==|      | ====|      |---y   
 |______|  |  /   | /   |______|       
           |_/____|/                   
            /                          
           /                    /x     
          x               z_ _ /       
        Satellite              \       
          Body                  \y     
         Frame             J2000       
                                       
A spacecraft is considered "pointing" in the direction of its z-axis or [0,0,1] vector in the "satellite body frame."
In the J2000 frame, the same spacecraft is pointing at [ 0.14129425 -0.98905974  0.04238827].
Determine the spacecraft attitude quaternion.
```