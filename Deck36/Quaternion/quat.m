v1 = [0,0,1];
v2 = [ 0.14129425 -0.98905974  0.04238827];

axis = cross(v2,v1);

M = [v1;v2;axis]

qw = sqrt(1 + M(1,1) + M(2,2) + M(3,3) /2)
qx = (M(3,2) - M(2,3))/( 4 *qw)
qy = (M(1,3) - M(3,1))/( 4 *qw)
qz = (M(2,1) - M(1,2))/( 4 *qw)

q = [qw,qx,qy,qz]%quaternion([qx,qy,qz]',qw)

res = quatrotate(q,v1)
%%

v1 = [0,0,1];
v2 = [ 0.14129425 -0.98905974  0.04238827];

a = cross(v2, v1)

w = sqrt((norm(v1) ^ 2) * (norm(v2) ^ 2)) + dot(v1, v2)

%%
Qx = -0.57599362
Qy = -0.38345432
Qz = 0.31105993
Qw = 0.65148742

q = [Qw,Qx,Qy,Qz]%quaternion([qx,qy,qz]',qw)

res = quatrotate(q,v1)
res2 = quatrotate(quatinv(q),v2)

%% Adam's working code

clc;
r1=[0 0 1];
r2=[ 0.14129425 -0.98905974  0.04238827];

rot=-cross(r2,r1);
rot=rot/norm(rot);

angl=acos(dot(r1,r2)/norm(r1)/norm(r2));

q(1)=cos(angl/2);
q(2)=rot(1)*sin(angl/2);
q(3)=rot(2)*sin(angl/2);
q(4)=rot(3)*sin(angl/2);

quatrotate(q,r1)

% Scalar first
disp(num2str(q(1),'%8.8f'));
disp(num2str(q(2),'%8.8f'));
disp(num2str(q(3),'%8.8f'));
disp(num2str(q(4),'%8.8f'));

angl/180/pi
