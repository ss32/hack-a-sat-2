% Tests
a = quaternion([1,.5,.75]', 1);
b = quaternion([.5,1,.25]', 0.5);

% Quat norm 
norm = quatNorm(a);
fprintf(strcat('Quaternion Conjugate: \n\t{Quat: ',...
    ' Scalar: %2.3f, Vector: [%2.3f, %2.3f, %2.3f]} \n'),...
    a.scalar,a.vector);
fprintf('\tResult: {Scalar: %2.3f, Vector: [%2.3f, %2.3f, %2.3f]} \n', ...
    norm.scalar,norm.vector);

% Quat Cross Product
crossProd = quatCrossProduct(a, b);
fprintf(strcat('Quaternion Cross Product: \n\t{Quat: ',...
    ' Scalar: %2.3f, Vector: [%2.3f, %2.3f, %2.3f]} + {Quat: ',...
    ' Scalar: %2.3f, Vector: [%2.3f, %2.3f, %2.3f]} \n'),...
    a.scalar,a.vector,b.scalar,b.vector);
fprintf('\tResult: {Scalar: %2.3f, Vector: [%2.3f, %2.3f, %2.3f]} \n', ...
    crossProd.scalar,crossProd.vector);

% Quat Dot Product
dotProduct = quatDotProduct(a,b);
fprintf(strcat('Quaternion Dot Product: \n\t{Quat: ',...
    ' Scalar: %2.3f, Vector: [%2.3f, %2.3f, %2.3f]} + {Quat: ',...
    ' Scalar: %2.3f, Vector: [%2.3f, %2.3f, %2.3f]} \n'),...
    a.scalar,a.vector,b.scalar,b.vector);
fprintf('\tResult: {Scalar: %2.3f} \n', ...
    dotProduct);

% Quat conjugate 
conj = quatConjugate(a);
fprintf(strcat('Quaternion Conjugate: \n\t{Quat: ',...
    ' Scalar: %2.3f, Vector: [%2.3f, %2.3f, %2.3f]} \n'),...
    a.scalar,a.vector);
fprintf('\tResult: {Scalar: %2.3f, Vector: [%2.3f, %2.3f, %2.3f]} \n', ...
    conj.scalar,conj.vector);

% Quat + quat
sum = quatAdd(a, b);
fprintf(strcat('Quaternion-Quaternion Addition: \n\t{Quat: ',...
    ' Scalar: %2.3f, Vector: [%2.3f, %2.3f, %2.3f]} + {Quat: ',...
    ' Scalar: %2.3f, Vector: [%2.3f, %2.3f, %2.3f]} \n'),...
    a.scalar,a.vector,b.scalar,b.vector);
fprintf('\tResult: {Scalar: %2.3f, Vector: [%2.3f, %2.3f, %2.3f]} \n', ...
    sum.scalar,sum.vector);

% Quat x quat
mult = quatMult(a,b);
fprintf(strcat('Quaternion-Quaternion Multiplication: \n\t{Quat: ',...
    ' Scalar: %2.3f, Vector: [%2.3f, %2.3f, %2.3f]} x {Quat: ',...
    ' Scalar: %2.3f, Vector: [%2.3f, %2.3f, %2.3f]} \n'),...
    a.scalar,a.vector,b.scalar,b.vector);
fprintf('\tResult: {Scalar: %2.3f, Vector: [%2.3f, %2.3f, %2.3f]} \n', ...
    mult.scalar,mult.vector);

% Matrix x quat
M = [1,1,1,1;2,2,2,2;3,3,3,3;4,4,4,4];
mult = quatMult(M,a);
fprintf(strcat('\nMatrix-Quaternion Multiplication: \n\t{Matrix: ',...
    ' %2.0f} x {Quat: ',...
    ' Scalar: %2.3f, Vector: [%2.3f, %2.3f, %2.3f]} \n'),...
    M,a.scalar,a.vector);
fprintf('\tResult: {Scalar: %2.3f, Vector: [%2.3f, %2.3f, %2.3f]} \n', ...
    mult.scalar,mult.vector);

% Verification
mult1 = quatMult(M,a);
mult = dot([mult1.vector', mult1.scalar]', [b.vector', b.scalar]');
fprintf('\n\t(M*a).b = %2.3f\n', mult);

mult1 = quatMult(M',b);
mult = dot([a.vector', a.scalar]', [mult1.vector', mult1.scalar]');
fprintf('\n\ta.(M''*b) = %2.3f\n', mult);

% scalar x quat
alp = 3;
mult = quatMult(alp,a);
fprintf(strcat('\nQuaternion-Scalar Multiplication: \n\t{Scalar: %2.0f} x {Quat: ',...
    ' Scalar: %2.3f, Vector: [%2.3f, %2.3f, %2.3f]} \n'),alp,a.scalar,a.vector);
fprintf('\tResult: {Scalar: %2.3f, Vector: [%2.3f, %2.3f, %2.3f]} \n', ...
    mult.scalar,mult.vector);

% Function definitions
function quaternion = quaternion(vector, scalar)
% *** Add check for proper inputs - vector must be 3x1. Lookup: inputparser
% Note: For vector inputs, the 4th param is the scalar
 if ~exist('scalar','var') % scalar parameter does not exist     
    quaternion.scalar = vector(4);
    quaternion.vector = vector(1:3,1);
    quaternion.R4 = vector;
 else
    quaternion.vector = vector;
    quaternion.scalar = scalar;
    quaternion.R4 = [vector', scalar]';
 end
end

function product = quatMult(a,b)
% *** Add check for proper inputs - both must be quaternions
% Note: Second input should always be the quat

% Three cases: 1) scalar x quat, 2) quat x quat, 3) matrix x quat
matrixInput = false;
quatInput = false;
scalarInput = false;
if(length(a(1,:))==4) && (length(a(:,1))==4)
    matrixInput = true;
elseif(length(a) && isnumeric(a))
    scalarInput = true;
elseif((length(a.vector)==3) && (length(a.scalar)==1))
    quatInput = true;
else
    throw('Improper input into quaternion product function.')
    return;
end
    
if(quatInput) % quat x quat
    vector = a.scalar * b.vector + b.scalar * a.vector + ...
        cross(a.vector, b.vector);
    scalar = a.scalar * b.scalar - a.vector' * b.vector;
    product = quaternion(vector, scalar);    
elseif (scalarInput) % scalar x quat
    product = quaternion(b.vector * a, b.scalar * a);
elseif(matrixInput) % 
    % *** Need to parse up the matrix appropriately
%     M11 = a(1:3,1:3);
%     M12 = a(1:3,4);
%     M21 = a(4,1:3);
%     M22 = a(4,4);    
    M11 = a(1,1);
    M12 = a(1,2:4);
    M21 = a(2:4,1);
    M22 = a(2:4,2:4);  
    product = quaternion(M21*b.scalar + M22*b.vector, ...
        M11*b.scalar + M12*b.vector);
else
    throw('Improper input into quaternion product function.')
end
end

function sum = quatAdd(a, b)
    sum = quaternion(a.vector+b.vector, a.scalar+b.scalar);
end

function conj = quatConjugate(a)
    conj = quaternion(-a.vector, a.scalar);
end

function dotProduct = quatDotProduct(a,b)
    dotProduct = a.scalar*b.scalar + dot(a.vector,b.vector);
end

function crossProd = quatCrossProduct(a, b)
    crossProd = quaternion(b.scalar*a.vector + a.scalar*b.vector + ...
        cross(a.vector,b.vector),0);
end

function norm = quatNorm(a)
    norm = quaternion([0,0,0]', a.scalar^2 + dot(a.vector,a.vector))
%     norm = quaternion([0,0,0]',quatDotProduct(a,a))
end
