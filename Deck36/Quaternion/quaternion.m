%% ===Function definitions===
function quaternion = quaternion(vector, scalar)
% *** Add check for proper inputs - vector must be 3x1. Lookup: inputparser
% Note: For vector inputs, the 4th param is the scalar
 if ~exist('scalar','var') % scalar parameter does not exist     
    quaternion.scalar = vector(4);
    quaternion.vector = vector(1:3,1);
    quaternion.R4 = vector;
 else
    quaternion.vector = vector;
    quaternion.scalar = scalar;
    quaternion.R4 = [vector', scalar]';
 end
end

function product = quatMult(a,b)
% *** Add check for proper inputs - both must be quaternions
% Note: Second input should always be the quat

% Three cases: 1) scalar x quat, 2) quat x quat, 3) matrix x quat
matrixInput = false;
quatInput = false;
scalarInput = false;
if(length(a(1,:))==4) && (length(a(:,1))==4)
    matrixInput = true;
elseif(length(a) && isnumeric(a))
    scalarInput = true;
elseif((length(a.vector)==3) && (length(a.scalar)==1))
    quatInput = true;
else
    throw('Improper input into quaternion product function.')
    return;
end
    
if(quatInput) % quat x quat
    vector = a.scalar * b.vector + b.scalar * a.vector + ...
        cross(a.vector, b.vector);
    scalar = a.scalar * b.scalar - a.vector' * b.vector;
    product = quaternion(vector, scalar);    
elseif (scalarInput) % scalar x quat
    product = quaternion(b.vector * a, b.scalar * a);
elseif(matrixInput) % 
    % *** Need to parse up the matrix appropriately
%     M11 = a(1:3,1:3);
%     M12 = a(1:3,4);
%     M21 = a(4,1:3);
%     M22 = a(4,4);    
    M11 = a(1,1);
    M12 = a(1,2:4);
    M21 = a(2:4,1);
    M22 = a(2:4,2:4);  
    product = quaternion(M21*b.scalar + M22*b.vector, ...
        M11*b.scalar + M12*b.vector);
else
    throw('Improper input into quaternion product function.')
end
end

function sum = quatAdd(a, b)
    sum = quaternion(a.vector+b.vector, a.scalar+b.scalar);
end

function conj = quatConjugate(a)
    conj = quaternion(-a.vector, a.scalar);
end

function dotProduct = quatDotProduct(a,b)
    dotProduct = a.scalar*b.scalar + dot(a.vector,b.vector);
end

function crossProd = quatCrossProduct(a, b)
    crossProd = quaternion(b.scalar*a.vector + a.scalar*b.vector + ...
        cross(a.vector,b.vector),0);
end

function norm = quatNorm(a)
    norm_ = quatMult(a,quatConjugate(a));
    norm = sqrt(norm_.R4);
%     norm = quaternion([0,0,0]', a.scalar^2 + dot(a.vector,a.vector))
%     norm = quaternion([0,0,0]',quatDotProduct(a,a))
end