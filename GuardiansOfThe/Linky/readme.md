Problem Statement 
Using sparse satellite data link information, determine link parameters. 

Function Statement
The first part of the problem entails determining the receiver gain, which is achieved by accounting for all of the gains and losses within the Friis transmission equation and solving for the appropriate gain. The second part involves calculating the G/T for the receiver. This is found by taking the receiver gain and subtracting the line losses in decibels and then subtracting the noise temperature in decibels. Finally, the transmitted power at the spacecraft is found by calculating the entire link margin to achieve the desired gain margin of 10dB by using various transmitted powers until the resultant margin is as requested.