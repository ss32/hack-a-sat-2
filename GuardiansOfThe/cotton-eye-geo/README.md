## Where do you go?

### Connecting

Connect to the challenge on:
visual-sun.satellitesabove.me:5014

Using netcat, you might run:
nc visual-sun.satellitesabove.me 5014 

```
        KEPLER 2 GEO    
           t, Δv             
         CHALLANGE          
          .  .   .         
       .            .      
     .      .  .      .    
    .   ,'   ,=,  .    .   
   .  ,    /     \  .   .  
   . .    |       | .   .  
   ..      \     / .    .  
   ..        '='  .    .   
    .          .'     .    
     Δ . .  '       .      
       '  .  .   .         
Your spacecraft from the first Kepler challenge is in a GEO transfer orbit.
Determine the maneuver (time and Δv vector) required to put the spacecraft into GEO-strationary orbit: a=42164+/-10km, e<0.001, i<1deg.
Assume two-body orbit dynamics and an instantaneous Δv in ICRF coordinates.
Pos (km):   [8449.401305, 9125.794363, -17.461357]
Vel (km/s): [-1.419072, 6.780149, 0.002865]
Time:       2021-06-26-19:20:00.000000-UTC

What maneuver is required?
Enter maneuver time in UTC following the example format.
Time: 2021-06-26-00:00:00.000-UTC
```