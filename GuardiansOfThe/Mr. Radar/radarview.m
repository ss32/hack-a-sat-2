az=radardata.az;
el = radardata.el;
R = radardata.range_km;

x=R.*cos(el).*cos(az);
y=R.*cos(el).*sin(az);
z=R.*sin(el);
plot3(x,y,z)