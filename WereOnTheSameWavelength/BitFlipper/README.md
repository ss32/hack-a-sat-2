
## Bit Flipper
We're On the Same Wavelength, 36 points

Back to Scoreboard

mom said it's my turn flipping bits in the thermal protection system

dad says don't touch the thermostat

## Connecting

Connect to the challenge on:
dying-word.satellitesabove.me:5004

Using netcat, you might run:
nc dying-word.satellitesabove.me 5004 

```
   BIT FLIPPER
    CHALLENGE

     10111100

You've been provided a special capability to target ionizing radiation on a region of memory used by a spacecraft's thermal protection system (TPS). Use this ability to flip the right combination of 3 bits that will pass SECDED checks, and change the behavior of the spacecraft's TPS when decoded from memory. Refering to the memory in its encoded form (encoded.bin), you must select a byte (0-404) and then which bit (0-7) in the byte to flip. You are allowed 3 bit flips.

Effect a change in the TPS such that the spacecraft exceeds its operating temperature range of 0-70C to obtain the flag.

Bitflip #1
   Select byte to ionize (0-404): 
```