
## iq
We're On the Same Wavelength, 22 points

Back to Scoreboard

Convert the provided series of transmit bits into in-phase quadrature samples.

Ticket


### Connecting

Connect to the challenge on:
unique-permit.satellitesabove.me:5006

Using netcat, you might run:
nc unique-permit.satellitesabove.me 5006 

```
IQ Challenge
   QPSK Modulation   
          Q
          |          
    01    |     11   
    o     |+1   o    
          |          
          |          
    -1    |     +1   
===================== I
          |          
          |          
    00    |     10   
    o     |-1   o    
          |          
          |          
Convert the provided series of transmit bits into QPSK I/Q samples
                  |Start here
                  v
Bits to transmit: 01000011 01110010 01101111 01101101 01110101 01101100 01100101 01101110 01110100 00001010
Provide as interleaved I/Q e.g. 1.0 -1.0 -1.0  1.0 ... 
                                 I    Q    I    Q  ...
Input samples: 
```