# -*- coding: utf-8 -*-
"""
Created on Sun Jun 27 13:55:12 2021

@author: mason
"""
with open('intermediate.bin', 'rb') as in_file:
    dat = in_file.read()
    
#print(dat)

# for a1 in range(0,len(dat)):
#     print(str(dat(a1)))
    
import hashlib

result = hashlib.md5(dat[0:14])
  
# printing the equivalent hexadecimal value.
print("The hexadecimal equivalent of hash is : ", end ="")
print(result.hexdigest())