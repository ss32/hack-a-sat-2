
## Error Correction
We're On the Same Wavelength, 287 points

Back to Scoreboard

Our spacecraft just downlinked some important telemetry but a bit error is preventing us from decoding it.

Files

You'll need these files to solve the challenge.

    https://generated.2021.hackasat.com/errcorr/errcorr-zulu140097whiskey2.tar.bz2
