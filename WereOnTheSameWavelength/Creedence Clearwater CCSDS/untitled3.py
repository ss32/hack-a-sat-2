# -*- coding: utf-8 -*-
"""
Created on Sun Jun 27 14:21:23 2021

@author: mason
"""
import reedsolomon as rs
import fieldmath
from numpy import sign, mean

f = open('iqdata.txt','r')
data = f.readlines()

data = [complex(ele) for ele in data]
#data = data[::4]
temp = []
for i in range(int(len(data)/4)):
    temp.append(mean(data[i*4:i*4+4]))
#print(temp)
data = temp

filtered = '' 

'''
for i in range(len(data)-1):
    if sign(data[i].real) == sign(data[i+1].real) and sign(data[i].imag) == sign(data[i+1].imag):
        filtered += '00'
    if sign(data[i].real) == sign(data[i+1].real) and sign(data[i].imag) != sign(data[i+1].imag):
        filtered += '01'
    if sign(data[i].real) != sign(data[i+1].real) and sign(data[i].imag) == sign(data[i+1].imag):
        filtered += '10'
    if sign(data[i].real) != sign(data[i+1].real) and sign(data[i].imag) != sign(data[i+1].imag):
        filtered += '11'
'''

for ele in data:
    if ele.real > 0 and ele.imag > 0:
        filtered += '00'
    elif ele.real < 0 and ele.imag > 0:
        filtered += '01'
    elif ele.real > 0 and ele.imag < 0:
        filtered += '10'
    else:
        filtered += '11'
print(filtered)
#print(len(filtered)/8)

symbols = []
for i in range(int(len(filtered)/8)):
    symbols.append(int(filtered[i:i+8],2))


#symbols = [111, 16, 1, 98, 85, 86, 21, 85, 85, 133, 187, 177, 182, 184, 140, 189, 176, 137, 186, 177, 73, 70, 72, 73, 68, 69, 138, 179, 190, 187, 176, 135, 178, 71, 79, 152, 158, 150, 191, 164, 173, 175, 190, 160, 160, 70, 136, 168, 136, 70, 159, 174, 77, 177, 176, 169, 136, 72, 78, 147, 142, 185, 189, 186, 151, 155, 147, 166, 160, 156, 156, 74, 132, 178, 145, 68, 165, 160, 74, 166, 144, 189, 73, 188, 132, 178, 69, 155, 188, 145, 170, 77, 72, 176, 143, 138, 146, 74, 168, 191, 135, 156, 183, 170, 156, 114, 69, 182, 114, 134, 146, 74, 188, 142, 167, 155, 68, 174, 175, 179, 177, 73, 130]

print(symbols)
print(len(symbols))

r = rs.ReedSolomon(fieldmath.BinaryField(8),'0b1011',len(symbols[:122])-32,32)
r.decode(symbols[:122])