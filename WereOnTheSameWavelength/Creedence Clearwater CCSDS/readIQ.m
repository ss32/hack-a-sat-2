clear; clc;

filename = 'iqdata.txt';
fileID = fopen(filename);
C = textscan(fileID,'%f');
fclose(fileID);
% whos C
plot(C{:,1}, '*');

val = '';%zeros(1,length(C{:,1}));
prev_val = 0;
for a1 = 1:length(C{:,1})    
    if real(C{1}(a1)) < 0
        if imag(C{1}(a1)) > 0
            if new_state(prev_val,'00')
                val = strcat(val, '00');
                
            end
            prev_val = '00';
        else % imag(C{a1}(1)) > 0
            if new_state(prev_val,'01')
                val = strcat(val, '01');
            end
            prev_val ='01';
        end
    else % real(C{a1}(1)) > 0
        if imag(C{1}(a1)) > 0
            if new_state(prev_val,'10')
                val = strcat(val, '10');
                             
            end
            prev_val = '10';   
        else % imag(C{a1}(1)) > 0
            if new_state(prev_val,'11')
                val = strcat(val, '11');
                              
            end
            prev_val = '11';  
        end            
    end
end

binarray = [];
for a1 = 1:length(val)
    binarray = [binarray str2num(val(a1))];
end



% out=pskdemod(binarray,4)
% figure; plot(val)
char(bin2dec(reshape(char(val),4,[]).'));


function ret = new_state(old,new)
ret = 1;
%     if old == new
%         ret = 0;
%     else
%         disp(new)
%         ret =  1;
%     end
end