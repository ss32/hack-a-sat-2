
## credence clearwater space data systems
We're On the Same Wavelength, 155 points


We've captured this noisy IQ data from a satellite and need to decode it. Figure out how to filter the noise while maintaining the signal characteristics, then demodulate and decode the signal to get the flag. The satellite is transmitting using asynchronous markers in CCSDS space packets and an unknown modulation.

Files

You'll need these files to solve the challenge.

    https://generated.2021.hackasat.com/noise/noise-hotel417430uniform2.tar.bz2
