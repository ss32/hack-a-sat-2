
## King's Ransom
Presents from Marco, 205 points

Back to Scoreboard

A vulnerable service with your "bank account" information running on the target system. Too bad it has already been exploited by a piece of ransomware. The ransomware took over the target, encrypted some files on the file system, and resumed the executive loop.

Follow the footsteps.


### Connecting

Connect to the challenge on:
wealthy-rock.satellitesabove.me:5010

Using netcat, you might run:
nc wealthy-rock.satellitesabove.me 5010
Files

You'll need these files to solve the challenge.

    https://static.2021.hackasat.com/lyj5rhtjfw992dn0f976ra4aglss
    https://static.2021.hackasat.com/t5qefn4567j7zpld6xuqrhay6hlj
