typedef unsigned char   undefined;

typedef unsigned char    bool;
typedef unsigned char    byte;
typedef unsigned char    dwfenc;
typedef unsigned int    dword;
typedef unsigned long    qword;
typedef unsigned char    uchar;
typedef unsigned int    uint;
typedef unsigned long    ulong;
typedef unsigned char    undefined1;
typedef unsigned int    undefined4;
typedef unsigned long    undefined8;
typedef unsigned short    ushort;
typedef unsigned short    word;
typedef struct eh_frame_hdr eh_frame_hdr, *Peh_frame_hdr;

struct eh_frame_hdr {
    byte eh_frame_hdr_version; // Exception Handler Frame Header Version
    dwfenc eh_frame_pointer_encoding; // Exception Handler Frame Pointer Encoding
    dwfenc eh_frame_desc_entry_count_encoding; // Encoding of # of Exception Handler FDEs
    dwfenc eh_frame_table_encoding; // Exception Handler Table Encoding
};

typedef struct fde_table_entry fde_table_entry, *Pfde_table_entry;

struct fde_table_entry {
    dword initial_loc; // Initial Location
    dword data_loc; // Data location
};

typedef void _IO_lock_t;

typedef struct _IO_marker _IO_marker, *P_IO_marker;

typedef struct _IO_FILE _IO_FILE, *P_IO_FILE;

typedef long __off_t;

typedef long __off64_t;

typedef ulong size_t;

struct _IO_FILE {
    int _flags;
    char * _IO_read_ptr;
    char * _IO_read_end;
    char * _IO_read_base;
    char * _IO_write_base;
    char * _IO_write_ptr;
    char * _IO_write_end;
    char * _IO_buf_base;
    char * _IO_buf_end;
    char * _IO_save_base;
    char * _IO_backup_base;
    char * _IO_save_end;
    struct _IO_marker * _markers;
    struct _IO_FILE * _chain;
    int _fileno;
    int _flags2;
    __off_t _old_offset;
    ushort _cur_column;
    char _vtable_offset;
    char _shortbuf[1];
    _IO_lock_t * _lock;
    __off64_t _offset;
    void * __pad1;
    void * __pad2;
    void * __pad3;
    void * __pad4;
    size_t __pad5;
    int _mode;
    char _unused2[15];
};

struct _IO_marker {
    struct _IO_marker * _next;
    struct _IO_FILE * _sbuf;
    int _pos;
};

typedef struct _IO_FILE FILE;

typedef struct __uninitialized_copy<true> __uninitialized_copy<true>, *P__uninitialized_copy<true>;

struct __uninitialized_copy<true> { // PlaceHolder Class Structure
};

typedef struct __copy_move<true,false,std::random_access_iterator_tag> __copy_move<true,false,std::random_access_iterator_tag>, *P__copy_move<true,false,std::random_access_iterator_tag>;

struct __copy_move<true,false,std::random_access_iterator_tag> { // PlaceHolder Class Structure
};

typedef struct move_iterator<std::_Deque_iterator<unsigned_char,unsigned_char&,unsigned_char*>> move_iterator<std::_Deque_iterator<unsigned_char,unsigned_char&,unsigned_char*>>, *Pmove_iterator<std::_Deque_iterator<unsigned_char,unsigned_char&,unsigned_char*>>;

struct move_iterator<std::_Deque_iterator<unsigned_char,unsigned_char&,unsigned_char*>> { // PlaceHolder Class Structure
};

typedef struct __copy_move<false,false,std::random_access_iterator_tag> __copy_move<false,false,std::random_access_iterator_tag>, *P__copy_move<false,false,std::random_access_iterator_tag>;

struct __copy_move<false,false,std::random_access_iterator_tag> { // PlaceHolder Class Structure
};

typedef long __ssize_t;

typedef __ssize_t ssize_t;

typedef uint __useconds_t;

typedef struct evp_pkey_ctx_st evp_pkey_ctx_st, *Pevp_pkey_ctx_st;

struct evp_pkey_ctx_st {
};

typedef struct evp_pkey_ctx_st EVP_PKEY_CTX;

typedef struct Queue Queue, *PQueue;

struct Queue { // PlaceHolder Structure
};

typedef dword _Ios_Fmtflags;

typedef struct _Deque_iterator _Deque_iterator, *P_Deque_iterator;

struct _Deque_iterator { // PlaceHolder Structure
};

typedef dword move_iterator;

typedef dword random_access_iterator_tag;

typedef struct allocator<char> allocator<char>, *Pallocator<char>;

struct allocator<char> { // PlaceHolder Structure
};

typedef struct deque deque, *Pdeque;

struct deque { // PlaceHolder Structure
};

typedef struct allocator allocator, *Pallocator;

struct allocator { // PlaceHolder Structure
};

typedef struct queue queue, *Pqueue;

struct queue { // PlaceHolder Structure
};

typedef struct ios_base ios_base, *Pios_base;

struct ios_base { // PlaceHolder Structure
};

typedef struct runtime_error runtime_error, *Pruntime_error;

struct runtime_error { // PlaceHolder Structure
};

typedef struct basic_ostream basic_ostream, *Pbasic_ostream;

struct basic_ostream { // PlaceHolder Structure
};

typedef dword integral_constant;

typedef struct fpos fpos, *Pfpos;

struct fpos { // PlaceHolder Structure
};

typedef struct deque<unsigned_char,std::allocator<unsigned_char>> deque<unsigned_char,std::allocator<unsigned_char>>, *Pdeque<unsigned_char,std::allocator<unsigned_char>>;

struct deque<unsigned_char,std::allocator<unsigned_char>> { // PlaceHolder Structure
};

typedef dword _Setfill;

typedef struct allocator<unsigned_char> allocator<unsigned_char>, *Pallocator<unsigned_char>;

struct allocator<unsigned_char> { // PlaceHolder Structure
};

typedef struct _Deque_iterator<unsigned_char,unsigned_char&,unsigned_char*> _Deque_iterator<unsigned_char,unsigned_char&,unsigned_char*>, *P_Deque_iterator<unsigned_char,unsigned_char&,unsigned_char*>;

struct _Deque_iterator<unsigned_char,unsigned_char&,unsigned_char*> { // PlaceHolder Structure
};

typedef struct queue<unsigned_char,std::deque<unsigned_char,std::allocator<unsigned_char>>> queue<unsigned_char,std::deque<unsigned_char,std::allocator<unsigned_char>>>, *Pqueue<unsigned_char,std::deque<unsigned_char,std::allocator<unsigned_char>>>;

struct queue<unsigned_char,std::deque<unsigned_char,std::allocator<unsigned_char>>> { // PlaceHolder Structure
};

typedef dword _Ios_Seekdir;

typedef struct _Deque_iterator<unsigned_char,unsigned_char_const&,unsigned_char_const*> _Deque_iterator<unsigned_char,unsigned_char_const&,unsigned_char_const*>, *P_Deque_iterator<unsigned_char,unsigned_char_const&,unsigned_char_const*>;

struct _Deque_iterator<unsigned_char,unsigned_char_const&,unsigned_char_const*> { // PlaceHolder Structure
};

typedef struct _Deque_base<unsigned_char,std::allocator<unsigned_char>> _Deque_base<unsigned_char,std::allocator<unsigned_char>>, *P_Deque_base<unsigned_char,std::allocator<unsigned_char>>;

struct _Deque_base<unsigned_char,std::allocator<unsigned_char>> { // PlaceHolder Structure
};

typedef dword _Setw;

typedef dword forward_iterator_tag;

typedef struct allocator<unsigned_char*> allocator<unsigned_char*>, *Pallocator<unsigned_char*>;

struct allocator<unsigned_char*> { // PlaceHolder Structure
};

typedef struct basic_ostream<char,std::char_traits<char>> basic_ostream<char,std::char_traits<char>>, *Pbasic_ostream<char,std::char_traits<char>>;

struct basic_ostream<char,std::char_traits<char>> { // PlaceHolder Structure
};

typedef dword type;


// WARNING! conflicting data type names: /Demangler/std/remove_reference<std::allocator<unsigned_char>>/type - /Demangler/std/enable_if<std::__and_<std::__not_<std::__is_tuple_like<unsigned_char**>>,std::is_move_constructible<unsigned_char**>,std::is_move_assignable<unsigned_char**>>::value,void>/type


// WARNING! conflicting data type names: /Demangler/std/remove_reference<std::deque<unsigned_char,std::allocator<unsigned_char>>const&>/type - /Demangler/std/enable_if<std::__and_<std::__not_<std::__is_tuple_like<unsigned_char**>>,std::is_move_constructible<unsigned_char**>,std::is_move_assignable<unsigned_char**>>::value,void>/type


// WARNING! conflicting data type names: /Demangler/std/remove_reference<unsigned_char&>/type - /Demangler/std/enable_if<std::__and_<std::__not_<std::__is_tuple_like<unsigned_char**>>,std::is_move_constructible<unsigned_char**>,std::is_move_assignable<unsigned_char**>>::value,void>/type

typedef dword iterator_category;

typedef dword difference_type;


// WARNING! conflicting data type names: /Demangler/std/remove_reference<unsigned_char_const&>/type - /Demangler/std/enable_if<std::__and_<std::__not_<std::__is_tuple_like<unsigned_char**>>,std::is_move_constructible<unsigned_char**>,std::is_move_assignable<unsigned_char**>>::value,void>/type


// WARNING! conflicting data type names: /Demangler/std/remove_reference<unsigned_char**&>/type - /Demangler/std/enable_if<std::__and_<std::__not_<std::__is_tuple_like<unsigned_char**>>,std::is_move_constructible<unsigned_char**>,std::is_move_assignable<unsigned_char**>>::value,void>/type

typedef dword basic_string;

typedef struct basic_string<char,std::char_traits<char>,std::allocator<char>> basic_string<char,std::char_traits<char>,std::allocator<char>>, *Pbasic_string<char,std::char_traits<char>,std::allocator<char>>;

struct basic_string<char,std::char_traits<char>,std::allocator<char>> { // PlaceHolder Structure
};

typedef struct basic_stringstream<char,std::char_traits<char>,std::allocator<char>> basic_stringstream<char,std::char_traits<char>,std::allocator<char>>, *Pbasic_stringstream<char,std::char_traits<char>,std::allocator<char>>;

struct basic_stringstream<char,std::char_traits<char>,std::allocator<char>> { // PlaceHolder Structure
};

typedef struct _Deque_impl _Deque_impl, *P_Deque_impl;

struct _Deque_impl { // PlaceHolder Structure
};


// WARNING! conflicting data type names: /Demangler/std/remove_reference<std::_Deque_iterator<unsigned_char,unsigned_char&,unsigned_char*>&>/type - /Demangler/std/enable_if<std::__and_<std::__not_<std::__is_tuple_like<unsigned_char**>>,std::is_move_constructible<unsigned_char**>,std::is_move_assignable<unsigned_char**>>::value,void>/type

typedef struct Init Init, *PInit;

struct Init { // PlaceHolder Structure
};


// WARNING! conflicting data type names: /Demangler/std/remove_reference<unsigned_long&>/type - /Demangler/std/enable_if<std::__and_<std::__not_<std::__is_tuple_like<unsigned_char**>>,std::is_move_constructible<unsigned_char**>,std::is_move_assignable<unsigned_char**>>::value,void>/type

typedef struct new_allocator new_allocator, *Pnew_allocator;

struct new_allocator { // PlaceHolder Structure
};

typedef struct new_allocator<unsigned_char> new_allocator<unsigned_char>, *Pnew_allocator<unsigned_char>;

struct new_allocator<unsigned_char> { // PlaceHolder Structure
};

typedef struct new_allocator<unsigned_char*> new_allocator<unsigned_char*>, *Pnew_allocator<unsigned_char*>;

struct new_allocator<unsigned_char*> { // PlaceHolder Structure
};

typedef struct __normal_iterator __normal_iterator, *P__normal_iterator;

struct __normal_iterator { // PlaceHolder Structure
};

typedef struct __normal_iterator<char*,std::__cxx11::basic_string<char,std::char_traits<char>,std::allocator<char>>> __normal_iterator<char*,std::__cxx11::basic_string<char,std::char_traits<char>,std::allocator<char>>>, *P__normal_iterator<char*,std::__cxx11::basic_string<char,std::char_traits<char>,std::allocator<char>>>;

struct __normal_iterator<char*,std::__cxx11::basic_string<char,std::char_traits<char>,std::allocator<char>>> { // PlaceHolder Structure
};

typedef enum Elf64_DynTag {
    DT_ANDROID_REL=1610612751,
    DT_ANDROID_RELA=1610612753,
    DT_ANDROID_RELASZ=1610612754,
    DT_ANDROID_RELR=1879040000,
    DT_ANDROID_RELRENT=1879040003,
    DT_ANDROID_RELRSZ=1879040001,
    DT_ANDROID_RELSZ=1610612752,
    DT_AUDIT=1879047932,
    DT_AUXILIARY=2147483645,
    DT_BIND_NOW=24,
    DT_CHECKSUM=1879047672,
    DT_CONFIG=1879047930,
    DT_DEBUG=21,
    DT_DEPAUDIT=1879047931,
    DT_FEATURE_1=1879047676,
    DT_FILTER=2147483647,
    DT_FINI=13,
    DT_FINI_ARRAY=26,
    DT_FINI_ARRAYSZ=28,
    DT_FLAGS=30,
    DT_FLAGS_1=1879048187,
    DT_GNU_CONFLICT=1879047928,
    DT_GNU_CONFLICTSZ=1879047670,
    DT_GNU_HASH=1879047925,
    DT_GNU_LIBLIST=1879047929,
    DT_GNU_LIBLISTSZ=1879047671,
    DT_GNU_PRELINKED=1879047669,
    DT_HASH=4,
    DT_INIT=12,
    DT_INIT_ARRAY=25,
    DT_INIT_ARRAYSZ=27,
    DT_JMPREL=23,
    DT_MOVEENT=1879047674,
    DT_MOVESZ=1879047675,
    DT_MOVETAB=1879047934,
    DT_NEEDED=1,
    DT_NULL=0,
    DT_PLTGOT=3,
    DT_PLTPAD=1879047933,
    DT_PLTPADSZ=1879047673,
    DT_PLTREL=20,
    DT_PLTRELSZ=2,
    DT_POSFLAG_1=1879047677,
    DT_PREINIT_ARRAY=32,
    DT_PREINIT_ARRAYSZ=33,
    DT_REL=17,
    DT_RELA=7,
    DT_RELACOUNT=1879048185,
    DT_RELAENT=9,
    DT_RELASZ=8,
    DT_RELCOUNT=1879048186,
    DT_RELENT=19,
    DT_RELR=36,
    DT_RELRENT=37,
    DT_RELRSZ=35,
    DT_RELSZ=18,
    DT_RPATH=15,
    DT_RUNPATH=29,
    DT_SONAME=14,
    DT_STRSZ=10,
    DT_STRTAB=5,
    DT_SYMBOLIC=16,
    DT_SYMENT=11,
    DT_SYMINENT=1879047679,
    DT_SYMINFO=1879047935,
    DT_SYMINSZ=1879047678,
    DT_SYMTAB=6,
    DT_TEXTREL=22,
    DT_TLSDESC_GOT=1879047927,
    DT_TLSDESC_PLT=1879047926,
    DT_VERDEF=1879048188,
    DT_VERDEFNUM=1879048189,
    DT_VERNEED=1879048190,
    DT_VERNEEDNUM=1879048191,
    DT_VERSYM=1879048176
} Elf64_DynTag;

typedef struct Elf64_Rela Elf64_Rela, *PElf64_Rela;

struct Elf64_Rela {
    qword r_offset; // location to apply the relocation action
    qword r_info; // the symbol table index and the type of relocation
    qword r_addend; // a constant addend used to compute the relocatable field value
};

typedef enum Elf_ProgramHeaderType {
    PT_DYNAMIC=2,
    PT_GNU_EH_FRAME=1685382480,
    PT_GNU_RELRO=1685382482,
    PT_GNU_STACK=1685382481,
    PT_INTERP=3,
    PT_LOAD=1,
    PT_NOTE=4,
    PT_NULL=0,
    PT_PHDR=6,
    PT_SHLIB=5,
    PT_TLS=7
} Elf_ProgramHeaderType;

typedef struct Elf64_Shdr Elf64_Shdr, *PElf64_Shdr;

typedef enum Elf_SectionHeaderType {
    SHT_ANDROID_REL=1610612737,
    SHT_ANDROID_RELA=1610612738,
    SHT_CHECKSUM=1879048184,
    SHT_DYNAMIC=6,
    SHT_DYNSYM=11,
    SHT_FINI_ARRAY=15,
    SHT_GNU_ATTRIBUTES=1879048181,
    SHT_GNU_HASH=1879048182,
    SHT_GNU_LIBLIST=1879048183,
    SHT_GNU_verdef=1879048189,
    SHT_GNU_verneed=1879048190,
    SHT_GNU_versym=1879048191,
    SHT_GROUP=17,
    SHT_HASH=5,
    SHT_INIT_ARRAY=14,
    SHT_NOBITS=8,
    SHT_NOTE=7,
    SHT_NULL=0,
    SHT_PREINIT_ARRAY=16,
    SHT_PROGBITS=1,
    SHT_REL=9,
    SHT_RELA=4,
    SHT_SHLIB=10,
    SHT_STRTAB=3,
    SHT_SUNW_COMDAT=1879048187,
    SHT_SUNW_move=1879048186,
    SHT_SUNW_syminfo=1879048188,
    SHT_SYMTAB=2,
    SHT_SYMTAB_SHNDX=18
} Elf_SectionHeaderType;

struct Elf64_Shdr {
    dword sh_name;
    enum Elf_SectionHeaderType sh_type;
    qword sh_flags;
    qword sh_addr;
    qword sh_offset;
    qword sh_size;
    dword sh_link;
    dword sh_info;
    qword sh_addralign;
    qword sh_entsize;
};

typedef struct Elf64_Phdr Elf64_Phdr, *PElf64_Phdr;

struct Elf64_Phdr {
    enum Elf_ProgramHeaderType p_type;
    dword p_flags;
    qword p_offset;
    qword p_vaddr;
    qword p_paddr;
    qword p_filesz;
    qword p_memsz;
    qword p_align;
};

typedef struct Elf64_Dyn Elf64_Dyn, *PElf64_Dyn;

struct Elf64_Dyn {
    enum Elf64_DynTag d_tag;
    qword d_val;
};

typedef struct Elf64_Sym Elf64_Sym, *PElf64_Sym;

struct Elf64_Sym {
    dword st_name;
    byte st_info;
    byte st_other;
    word st_shndx;
    qword st_value;
    qword st_size;
};

typedef struct Gnu_BuildId Gnu_BuildId, *PGnu_BuildId;

struct Gnu_BuildId {
    dword namesz; // Length of name field
    dword descsz; // Length of description field
    dword type; // Vendor specific type
    char name[4]; // Build-id vendor name
    byte description[20]; // Build-id value
};

typedef struct Elf64_Ehdr Elf64_Ehdr, *PElf64_Ehdr;

struct Elf64_Ehdr {
    byte e_ident_magic_num;
    char e_ident_magic_str[3];
    byte e_ident_class;
    byte e_ident_data;
    byte e_ident_version;
    byte e_ident_osabi;
    byte e_ident_abiversion;
    byte e_ident_pad[7];
    word e_type;
    word e_machine;
    dword e_version;
    qword e_entry;
    qword e_phoff;
    qword e_shoff;
    dword e_flags;
    word e_ehsize;
    word e_phentsize;
    word e_phnum;
    word e_shentsize;
    word e_shnum;
    word e_shstrndx;
};




int _init(EVP_PKEY_CTX *ctx)

{
  int iVar1;
  
  iVar1 = __gmon_start__();
  return iVar1;
}



void FUN_00102020(void)

{
                    // WARNING: Treating indirect jump as call
  (*(code *)(undefined *)0x0)();
  return;
}



void __cxa_finalize(void)

{
  __cxa_finalize();
  return;
}



void thrd_join(void)

{
  thrd_join();
  return;
}



// WARNING: Unknown calling convention yet parameter storage is locked

ulong strtoul(char *__nptr,char **__endptr,int __base)

{
  ulong uVar1;
  
  uVar1 = strtoul(__nptr,__endptr,__base);
  return uVar1;
}



void __thiscall
std::basic_ostream<char,std::char_traits<char>>::operator__
          (basic_ostream_char_std__char_traits_char__ *this,FuncDef0 *param_1)

{
  operator__(this,param_1);
  return;
}



// WARNING: Unknown calling convention yet parameter storage is locked

void std::__throw_bad_alloc(void)

{
  __throw_bad_alloc();
  return;
}



void __thiscall std::runtime_error::runtime_error(runtime_error *this,char *param_1)

{
  runtime_error(this,param_1);
  return;
}



void __cxa_begin_catch(void)

{
  __cxa_begin_catch();
  return;
}



// WARNING: Unknown calling convention yet parameter storage is locked

void std::__cxx11::basic_string<char,std::char_traits<char>,std::allocator<char>>::c_str(void)

{
  c_str();
  return;
}



// WARNING: Unknown calling convention yet parameter storage is locked

int memcmp(void *__s1,void *__s2,size_t __n)

{
  int iVar1;
  
  iVar1 = memcmp(__s1,__s2,__n);
  return iVar1;
}



void __cxa_allocate_exception(void)

{
  __cxa_allocate_exception();
  return;
}



// WARNING: Unknown calling convention yet parameter storage is locked

void std::__throw_length_error(char *param_1)

{
  __throw_length_error(param_1);
  return;
}



// WARNING: Unknown calling convention yet parameter storage is locked

void std::__cxx11::basic_string<char,std::char_traits<char>,std::allocator<char>>::end(void)

{
  end();
  return;
}



void __thiscall
std::__cxx11::basic_string<char,std::char_traits<char>,std::allocator<char>>::_basic_string
          (basic_string_char_std__char_traits_char__std__allocator_char__ *this)

{
  _basic_string(this);
  return;
}



// WARNING: Unknown calling convention yet parameter storage is locked

void std::__cxx11::basic_string<char,std::char_traits<char>,std::allocator<char>>::size(void)

{
  size();
  return;
}



// WARNING: Unknown calling convention yet parameter storage is locked

void std::__cxx11::basic_stringstream<char,std::char_traits<char>,std::allocator<char>>::
     basic_stringstream(void)

{
  basic_stringstream();
  return;
}



void __cxa_free_exception(void)

{
  __cxa_free_exception();
  return;
}



void __thiscall
std::__cxx11::basic_stringstream<char,std::char_traits<char>,std::allocator<char>>::
_basic_stringstream(basic_stringstream_char_std__char_traits_char__std__allocator_char__ *this)

{
  _basic_stringstream(this);
  return;
}



// WARNING: Unknown calling convention yet parameter storage is locked

uint sleep(uint __seconds)

{
  uint uVar1;
  
  uVar1 = sleep(__seconds);
  return uVar1;
}



// WARNING: Unknown calling convention yet parameter storage is locked

void std::__cxx11::basic_string<char,std::char_traits<char>,std::allocator<char>>::begin(void)

{
  begin();
  return;
}



void __cxa_atexit(void)

{
  __cxa_atexit();
  return;
}



// WARNING: Unknown calling convention yet parameter storage is locked

basic_ostream * std::operator__(basic_ostream *param_1,basic_string *param_2)

{
  basic_ostream *pbVar1;
  
  pbVar1 = operator__(param_1,param_2);
  return pbVar1;
}



// WARNING: Unknown calling convention yet parameter storage is locked

void operator_delete(void *param_1)

{
  operator_delete(param_1);
  return;
}



// WARNING: Unknown calling convention yet parameter storage is locked

void std::__cxx11::basic_stringstream<char,std::char_traits<char>,std::allocator<char>>::str(void)

{
  str();
  return;
}



// WARNING: Unknown calling convention yet parameter storage is locked

basic_ostream * std::operator__(basic_ostream *param_1,char *param_2)

{
  basic_ostream *pbVar1;
  
  pbVar1 = operator__(param_1,param_2);
  return pbVar1;
}



// WARNING: Unknown calling convention yet parameter storage is locked

void * operator_new(ulong param_1)

{
  void *pvVar1;
  
  pvVar1 = operator_new(param_1);
  return pvVar1;
}



// WARNING: Unknown calling convention yet parameter storage is locked

void std::__cxx11::basic_string<char,std::char_traits<char>,std::allocator<char>>::substr
               (ulong param_1,ulong param_2)

{
  substr(param_1,param_2);
  return;
}



void __thiscall
std::basic_ostream<char,std::char_traits<char>>::operator__
          (basic_ostream_char_std__char_traits_char__ *this,FuncDef1 *param_1)

{
  operator__(this,param_1);
  return;
}



void __thiscall std::allocator<char>::_allocator(allocator_char_ *this)

{
  _allocator(this);
  return;
}



void __stack_chk_fail(void)

{
                    // WARNING: Subroutine does not return
  __stack_chk_fail();
}



// WARNING: Unknown calling convention yet parameter storage is locked

void std::__cxx11::basic_stringstream<char,std::char_traits<char>,std::allocator<char>>::str
               (basic_string *param_1)

{
  str(param_1);
  return;
}



// WARNING: Unknown calling convention yet parameter storage is locked

basic_ostream * std::operator__(basic_ostream *param_1,_Setw param_2)

{
  basic_ostream *pbVar1;
  
  pbVar1 = operator__(param_1,param_2);
  return pbVar1;
}



// WARNING: Unknown calling convention yet parameter storage is locked

int fflush(FILE *__stream)

{
  int iVar1;
  
  iVar1 = fflush(__stream);
  return iVar1;
}



// WARNING: Unknown calling convention yet parameter storage is locked

void exit(int __status)

{
                    // WARNING: Subroutine does not return
  exit(__status);
}



// WARNING: Unknown calling convention yet parameter storage is locked

char * getenv(char *__name)

{
  char *pcVar1;
  
  pcVar1 = getenv(__name);
  return pcVar1;
}



void __thiscall
std::__cxx11::basic_string<char,std::char_traits<char>,std::allocator<char>>::operator_
          (basic_string_char_std__char_traits_char__std__allocator_char__ *this,
          basic_string *param_1)

{
  operator_(this,param_1);
  return;
}



void thrd_exit(void)

{
  thrd_exit();
  return;
}



// WARNING: Unknown calling convention yet parameter storage is locked

void std::__cxx11::basic_string<char,std::char_traits<char>,std::allocator<char>>::clear(void)

{
  clear();
  return;
}



void __thiscall
std::basic_ostream<char,std::char_traits<char>>::operator__
          (basic_ostream_char_std__char_traits_char__ *this,uint param_1)

{
  operator__(this,param_1);
  return;
}



// WARNING: Unknown calling convention yet parameter storage is locked

void std::basic_istream<char,std::char_traits<char>>::seekg(long param_1,_Ios_Seekdir param_2)

{
  seekg(param_1,param_2);
  return;
}



// WARNING: Unknown calling convention yet parameter storage is locked

void std::__cxx11::basic_string<char,std::char_traits<char>,std::allocator<char>>::basic_string
               (char *param_1,allocator *param_2)

{
  basic_string(param_1,param_2);
  return;
}



// WARNING: Unknown calling convention yet parameter storage is locked

void std::__cxx11::basic_string<char,std::char_traits<char>,std::allocator<char>>::resize
               (ulong param_1)

{
  resize(param_1);
  return;
}



// WARNING: Unknown calling convention yet parameter storage is locked

void std::__cxx11::basic_string<char,std::char_traits<char>,std::allocator<char>>::basic_string
               (void)

{
  basic_string();
  return;
}



// WARNING: Unknown calling convention yet parameter storage is locked

int putchar(int __c)

{
  int iVar1;
  
  iVar1 = putchar(__c);
  return iVar1;
}



// WARNING: Unknown calling convention yet parameter storage is locked

int usleep(__useconds_t __useconds)

{
  int iVar1;
  
  iVar1 = usleep(__useconds);
  return iVar1;
}



// WARNING: Unknown calling convention yet parameter storage is locked

ssize_t read(int __fd,void *__buf,size_t __nbytes)

{
  ssize_t sVar1;
  
  sVar1 = read(__fd,__buf,__nbytes);
  return sVar1;
}



void __cxa_rethrow(void)

{
  __cxa_rethrow();
  return;
}



void __thiscall std::ios_base::Init::Init(Init *this)

{
  Init(this);
  return;
}



// WARNING: Unknown calling convention yet parameter storage is locked

int puts(char *__s)

{
  int iVar1;
  
  iVar1 = puts(__s);
  return iVar1;
}



// WARNING: Unknown calling convention yet parameter storage is locked

basic_ostream * std::operator__(basic_ostream *param_1,_Setfill param_2)

{
  basic_ostream *pbVar1;
  
  pbVar1 = operator__(param_1,param_2);
  return pbVar1;
}



// WARNING: Unknown calling convention yet parameter storage is locked

void * memmove(void *__dest,void *__src,size_t __n)

{
  void *pvVar1;
  
  pvVar1 = memmove(__dest,__src,__n);
  return pvVar1;
}



void __cxa_end_catch(void)

{
  __cxa_end_catch();
  return;
}



void __cxa_throw(void)

{
                    // WARNING: Subroutine does not return
  __cxa_throw();
}



void __thiscall
std::basic_ostream<char,std::char_traits<char>>::operator__
          (basic_ostream_char_std__char_traits_char__ *this,int param_1)

{
  operator__(this,param_1);
  return;
}



// WARNING: Unknown calling convention yet parameter storage is locked

void std::basic_istream<char,std::char_traits<char>>::tellg(void)

{
  tellg();
  return;
}



void _Unwind_Resume(void)

{
                    // WARNING: Subroutine does not return
  _Unwind_Resume();
}



// WARNING: Unknown calling convention yet parameter storage is locked

void std::allocator<char>::allocator(void)

{
  allocator();
  return;
}



void thrd_create(void)

{
  thrd_create();
  return;
}



void _start(undefined8 param_1,undefined8 param_2,undefined8 param_3)

{
  undefined8 in_stack_00000000;
  undefined auStack8 [8];
  
  __libc_start_main(main,in_stack_00000000,&stack0x00000008,__libc_csu_init,__libc_csu_fini,param_3,
                    auStack8);
  do {
                    // WARNING: Do nothing block with infinite loop
  } while( true );
}



// WARNING: Removing unreachable block (ram,0x00102783)
// WARNING: Removing unreachable block (ram,0x0010278f)

void deregister_tm_clones(void)

{
  return;
}



// WARNING: Removing unreachable block (ram,0x001027c4)
// WARNING: Removing unreachable block (ram,0x001027d0)

void register_tm_clones(void)

{
  return;
}



void __do_global_dtors_aux(void)

{
  if (completed_8060 != '\0') {
    return;
  }
  __cxa_finalize(__dso_handle);
  deregister_tm_clones();
  completed_8060 = 1;
  return;
}



void frame_dummy(void)

{
  register_tm_clones();
  return;
}



// printDebug(std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char>>,
// unsigned int)

void printDebug(basic_string param_1,uint param_2)

{
  _Setw _Var1;
  _Setfill _Var2;
  char *pcVar3;
  basic_ostream *pbVar4;
  basic_ostream_char_std__char_traits_char__ *this;
  long in_FS_OFFSET;
  basic_string_char_std__char_traits_char__std__allocator_char__ local_1e8 [32];
  basic_string local_1c8 [8];
  basic_string local_1a8 [4];
  basic_ostream_char_std__char_traits_char__ abStack408 [376];
  long local_20;
  
  local_20 = *(long *)(in_FS_OFFSET + 0x28);
  std::__cxx11::basic_stringstream<char,std::char_traits<char>,std::allocator<char>>::
  basic_stringstream();
  std::__cxx11::basic_string<char,std::char_traits<char>,std::allocator<char>>::basic_string();
  std::__cxx11::basic_string<char,std::char_traits<char>,std::allocator<char>>::basic_string();
                    // try { // try from 00102896 to 0010289a has its CatchHandler @ 001029d7
  std::__cxx11::basic_stringstream<char,std::char_traits<char>,std::allocator<char>>::str(local_1a8)
  ;
  std::__cxx11::basic_string<char,std::char_traits<char>,std::allocator<char>>::_basic_string
            ((basic_string_char_std__char_traits_char__std__allocator_char__ *)local_1c8);
  std::__cxx11::basic_string<char,std::char_traits<char>,std::allocator<char>>::clear();
                    // try { // try from 001028cf to 001029a6 has its CatchHandler @ 001029ef
  std::basic_ostream<char,std::char_traits<char>>::operator__(abStack408,param_2);
  std::__cxx11::basic_stringstream<char,std::char_traits<char>,std::allocator<char>>::str();
  std::__cxx11::basic_string<char,std::char_traits<char>,std::allocator<char>>::operator_
            (local_1e8,local_1c8);
  std::__cxx11::basic_string<char,std::char_traits<char>,std::allocator<char>>::_basic_string
            ((basic_string_char_std__char_traits_char__std__allocator_char__ *)local_1c8);
  pcVar3 = (char *)std::__cxx11::basic_string<char,std::char_traits<char>,std::allocator<char>>::
                   c_str();
  pbVar4 = std::operator__((basic_ostream *)std::cout,pcVar3);
  pbVar4 = std::operator__(pbVar4,": ");
  pbVar4 = (basic_ostream *)
           std::basic_ostream<char,std::char_traits<char>>::operator__
                     ((basic_ostream_char_std__char_traits_char__ *)pbVar4,std::hex);
  _Var1 = std::setw(8);
  pbVar4 = std::operator__(pbVar4,_Var1);
  _Var2 = std::setfill_char_('0');
  pbVar4 = std::operator__(pbVar4,_Var2);
  this = (basic_ostream_char_std__char_traits_char__ *)
         std::basic_ostream<char,std::char_traits<char>>::operator__
                   ((basic_ostream_char_std__char_traits_char__ *)pbVar4,param_2);
  std::basic_ostream<char,std::char_traits<char>>::operator__
            (this,std::endl_char_std__char_traits_char__);
  std::__cxx11::basic_string<char,std::char_traits<char>,std::allocator<char>>::_basic_string
            (local_1e8);
  std::__cxx11::basic_stringstream<char,std::char_traits<char>,std::allocator<char>>::
  _basic_stringstream((basic_stringstream_char_std__char_traits_char__std__allocator_char__ *)
                      local_1a8);
  if (local_20 != *(long *)(in_FS_OFFSET + 0x28)) {
                    // WARNING: Subroutine does not return
    __stack_chk_fail();
  }
  return;
}



// WARNING: Unknown calling convention yet parameter storage is locked
// printIntroArt()

void printIntroArt(void)

{
  basic_ostream *pbVar1;
  basic_ostream_char_std__char_traits_char__ *pbVar2;
  
  pbVar1 = std::operator__((basic_ostream *)std::cout,"");
  pbVar2 = (basic_ostream_char_std__char_traits_char__ *)
           std::basic_ostream<char,std::char_traits<char>>::operator__
                     ((basic_ostream_char_std__char_traits_char__ *)pbVar1,
                      std::endl_char_std__char_traits_char__);
  pbVar2 = (basic_ostream_char_std__char_traits_char__ *)
           std::basic_ostream<char,std::char_traits<char>>::operator__
                     (pbVar2,std::endl_char_std__char_traits_char__);
  std::basic_ostream<char,std::char_traits<char>>::operator__
            (pbVar2,std::endl_char_std__char_traits_char__);
  pbVar1 = std::operator__((basic_ostream *)std::cout,
                           "     .-.                                                         -----------"
                          );
  std::basic_ostream<char,std::char_traits<char>>::operator__
            ((basic_ostream_char_std__char_traits_char__ *)pbVar1,
             std::endl_char_std__char_traits_char__);
  pbVar1 = std::operator__((basic_ostream *)std::cout,
                           "   (;;;)                                                        /            \'"
                          );
  std::basic_ostream<char,std::char_traits<char>>::operator__
            ((basic_ostream_char_std__char_traits_char__ *)pbVar1,
             std::endl_char_std__char_traits_char__);
  pbVar1 = std::operator__((basic_ostream *)std::cout,
                           "    |_|                                  ,-------,-|           |C>      )    |"
                          );
  std::basic_ostream<char,std::char_traits<char>>::operator__
            ((basic_ostream_char_std__char_traits_char__ *)pbVar1,
             std::endl_char_std__char_traits_char__);
  pbVar1 = std::operator__((basic_ostream *)std::cout,
                           "      \' _.--l--._                       \'      ,\',\'|          /    || ,\'     |"
                          );
  std::basic_ostream<char,std::char_traits<char>>::operator__
            ((basic_ostream_char_std__char_traits_char__ *)pbVar1,
             std::endl_char_std__char_traits_char__);
  pbVar1 = std::operator__((basic_ostream *)std::cout,
                           "     .      |     `.                   \'-----,\',\'  |         (,    ||        ,"
                          );
  std::basic_ostream<char,std::char_traits<char>>::operator__
            ((basic_ostream_char_std__char_traits_char__ *)pbVar1,
             std::endl_char_std__char_traits_char__);
  pbVar1 = std::operator__((basic_ostream *)std::cout,
                           "   .` `.    |    .` `.                 |     ||    |           -- ||||      |"
                          );
  std::basic_ostream<char,std::char_traits<char>>::operator__
            ((basic_ostream_char_std__char_traits_char__ *)pbVar1,
             std::endl_char_std__char_traits_char__);
  pbVar1 = std::operator__((basic_ostream *)std::cout,
                           " .`     `   |  .`     `.               |     ||    |           |||||||     _|"
                          );
  std::basic_ostream<char,std::char_traits<char>>::operator__
            ((basic_ostream_char_std__char_traits_char__ *)pbVar1,
             std::endl_char_std__char_traits_char__);
  pbVar1 = std::operator__((basic_ostream *)std::cout,
                           "\' __      `.|.`      __ `              |     ||    |______      `````|____/ |"
                          );
  std::basic_ostream<char,std::char_traits<char>>::operator__
            ((basic_ostream_char_std__char_traits_char__ *)pbVar1,
             std::endl_char_std__char_traits_char__);
  pbVar1 = std::operator__((basic_ostream *)std::cout,
                           "|   \'\'--._  V  _.--\'\'   |              |     ||    |     ,|         _/_____/ |"
                          );
  std::basic_ostream<char,std::char_traits<char>>::operator__
            ((basic_ostream_char_std__char_traits_char__ *)pbVar1,
             std::endl_char_std__char_traits_char__);
  pbVar1 = std::operator__((basic_ostream *)std::cout,
                           "|        _ ( ) _        |              |     ||  ,\'    ,\' |        /          |"
                          );
  std::basic_ostream<char,std::char_traits<char>>::operator__
            ((basic_ostream_char_std__char_traits_char__ *)pbVar1,
             std::endl_char_std__char_traits_char__);
  pbVar1 = std::operator__((basic_ostream *)std::cout,
                           "| __..--\'   ^   \'--..__ | _           \'|     ||,\'    ,\'   |       |            |"
                          );
  std::basic_ostream<char,std::char_traits<char>>::operator__
            ((basic_ostream_char_std__char_traits_char__ *)pbVar1,
             std::endl_char_std__char_traits_char__);
  pbVar1 = std::operator__((basic_ostream *)std::cout,
                           "\'         .`|`.         \'-.)        ,  |_____|\'    ,\'     |       /           | |"
                          );
  std::basic_ostream<char,std::char_traits<char>>::operator__
            ((basic_ostream_char_std__char_traits_char__ *)pbVar1,
             std::endl_char_std__char_traits_char__);
  pbVar1 = std::operator__((basic_ostream *)std::cout,
                           " `.     .`  |  `.     .`           , _____________,\'      ,\',_____|      |    | |"
                          );
  std::basic_ostream<char,std::char_traits<char>>::operator__
            ((basic_ostream_char_std__char_traits_char__ *)pbVar1,
             std::endl_char_std__char_traits_char__);
  pbVar1 = std::operator__((basic_ostream *)std::cout,
                           "   `. .`    |    `. .`             |             |     ,\',\'      |      |    | |"
                          );
  std::basic_ostream<char,std::char_traits<char>>::operator__
            ((basic_ostream_char_std__char_traits_char__ *)pbVar1,
             std::endl_char_std__char_traits_char__);
  pbVar1 = std::operator__((basic_ostream *)std::cout,
                           "     `._    |    _.`|       -------|             |   ,\',\'    ____|_____/    /  |"
                          );
  std::basic_ostream<char,std::char_traits<char>>::operator__
            ((basic_ostream_char_std__char_traits_char__ *)pbVar1,
             std::endl_char_std__char_traits_char__);
  pbVar1 = std::operator__((basic_ostream *)std::cout,
                           "         `--l--`  | |     ;        |             | ,\',\'  /  |              /   |"
                          );
  std::basic_ostream<char,std::char_traits<char>>::operator__
            ((basic_ostream_char_std__char_traits_char__ *)pbVar1,
             std::endl_char_std__char_traits_char__);
  pbVar1 = std::operator__((basic_ostream *)std::cout,
                           "                  | |    ;         |_____________|\',\'    |   -------------/   |"
                          );
  std::basic_ostream<char,std::char_traits<char>>::operator__
            ((basic_ostream_char_std__char_traits_char__ *)pbVar1,
             std::endl_char_std__char_traits_char__);
  pbVar1 = std::operator__((basic_ostream *)std::cout,
                           "               ---| |-------.                      |===========,\'");
  std::basic_ostream<char,std::char_traits<char>>::operator__
            ((basic_ostream_char_std__char_traits_char__ *)pbVar1,
             std::endl_char_std__char_traits_char__);
  pbVar1 = std::operator__((basic_ostream *)std::cout,
                           "             /    ..       / |                                   ");
  std::basic_ostream<char,std::char_traits<char>>::operator__
            ((basic_ostream_char_std__char_traits_char__ *)pbVar1,
             std::endl_char_std__char_traits_char__);
  pbVar1 = std::operator__((basic_ostream *)std::cout,"            --------------   |");
  std::basic_ostream<char,std::char_traits<char>>::operator__
            ((basic_ostream_char_std__char_traits_char__ *)pbVar1,
             std::endl_char_std__char_traits_char__);
  pbVar1 = std::operator__((basic_ostream *)std::cout,"           |              | /");
  std::basic_ostream<char,std::char_traits<char>>::operator__
            ((basic_ostream_char_std__char_traits_char__ *)pbVar1,
             std::endl_char_std__char_traits_char__);
  pbVar1 = std::operator__((basic_ostream *)std::cout,"           |              |/");
  std::basic_ostream<char,std::char_traits<char>>::operator__
            ((basic_ostream_char_std__char_traits_char__ *)pbVar1,
             std::endl_char_std__char_traits_char__);
  pbVar1 = std::operator__((basic_ostream *)std::cout,"            --------------");
  std::basic_ostream<char,std::char_traits<char>>::operator__
            ((basic_ostream_char_std__char_traits_char__ *)pbVar1,
             std::endl_char_std__char_traits_char__);
  return;
}



// WARNING: Unknown calling convention yet parameter storage is locked
// printIntroText()

void printIntroText(void)

{
  putchar(10);
  putchar(10);
  puts("Ground Station ONLINE\n");
  putchar(0x3e);
  fflush(stdout);
  return;
}



// startTimer(void*)

uint startTimer(void *param_1)

{
  int in_ESI;
  int iVar1;
  
  sleep(0xb4);
  puts("Time is up. Goodbye.");
  fflush(stdout);
  iVar1 = 0;
  thrd_exit();
  return (in_ESI - iVar1) + 1U | iVar1 << 0x10;
}



// pktPrimHdrBitDefn(unsigned int, unsigned int)

uint pktPrimHdrBitDefn(uint param_1,uint param_2)

{
  return (param_2 - param_1) + 1 | param_1 << 0x10;
}



// readPktPrimHdrBitField(unsigned int, unsigned int)

uint readPktPrimHdrBitField(uint param_1,uint param_2)

{
  return param_2 >> ((byte)(param_1 >> 0x10) & 0x1f) & (1 << ((byte)param_1 & 0x1f)) - 1U;
}



// char2int(char)

int char2int(char param_1)

{
  int iVar1;
  runtime_error *this;
  
  if ((param_1 < '0') || ('9' < param_1)) {
    if ((param_1 < 'A') || ('F' < param_1)) {
      if ((param_1 < 'a') || ('f' < param_1)) {
        this = (runtime_error *)__cxa_allocate_exception(0x10);
                    // try { // try from 00102f74 to 00102f78 has its CatchHandler @ 00102f92
        std::runtime_error::runtime_error(this,"Incorrect symbol in hex string");
                    // WARNING: Subroutine does not return
        __cxa_throw(this,std::runtime_error::typeinfo,std::runtime_error::_runtime_error);
      }
      iVar1 = (byte)param_1 - 0x57;
    }
    else {
      iVar1 = (byte)param_1 - 0x37;
    }
  }
  else {
    iVar1 = (byte)param_1 - 0x30;
  }
  return iVar1;
}



// hex2str(std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char>>)

ulong hex2str(basic_string param_1)

{
  undefined uVar1;
  bool bVar2;
  char cVar3;
  byte bVar4;
  uint uVar5;
  char *pcVar6;
  undefined *puVar7;
  byte *pbVar8;
  undefined4 in_register_0000003c;
  long in_FS_OFFSET;
  undefined8 local_40;
  undefined8 local_38;
  undefined8 local_30;
  undefined8 local_28;
  long local_20;
  
  local_20 = *(long *)(in_FS_OFFSET + 0x28);
  std::__cxx11::basic_string<char,std::char_traits<char>,std::allocator<char>>::basic_string();
  std::__cxx11::basic_string<char,std::char_traits<char>,std::allocator<char>>::size();
  std::__cxx11::basic_string<char,std::char_traits<char>,std::allocator<char>>::size();
                    // try { // try from 00103016 to 00103139 has its CatchHandler @ 0010317a
  std::__cxx11::basic_string<char,std::char_traits<char>,std::allocator<char>>::resize
            (CONCAT44(in_register_0000003c,param_1));
  local_40 = std::__cxx11::basic_string<char,std::char_traits<char>,std::allocator<char>>::begin();
  local_38 = std::__cxx11::basic_string<char,std::char_traits<char>,std::allocator<char>>::begin();
  uVar5 = std::__cxx11::basic_string<char,std::char_traits<char>,std::allocator<char>>::size();
  if ((uVar5 & 1) != 0) {
    local_28 = __gnu_cxx::
               __normal_iterator<char*,std::__cxx11::basic_string<char,std::char_traits<char>,std::allocator<char>>>
               ::operator__((__normal_iterator_char__std____cxx11__basic_string_char_std__char_traits_char__std__allocator_char___
                             *)&local_40,0);
    pcVar6 = (char *)__gnu_cxx::
                     __normal_iterator<char*,std::__cxx11::basic_string<char,std::char_traits<char>,std::allocator<char>>>
                     ::operator_((__normal_iterator_char__std____cxx11__basic_string_char_std__char_traits_char__std__allocator_char___
                                  *)&local_28);
    cVar3 = *pcVar6;
    local_30 = __gnu_cxx::
               __normal_iterator<char*,std::__cxx11::basic_string<char,std::char_traits<char>,std::allocator<char>>>
               ::operator__((__normal_iterator_char__std____cxx11__basic_string_char_std__char_traits_char__std__allocator_char___
                             *)&local_38,0);
    puVar7 = (undefined *)
             __gnu_cxx::
             __normal_iterator<char*,std::__cxx11::basic_string<char,std::char_traits<char>,std::allocator<char>>>
             ::operator_((__normal_iterator_char__std____cxx11__basic_string_char_std__char_traits_char__std__allocator_char___
                          *)&local_30);
    uVar1 = char2int(cVar3);
    *puVar7 = uVar1;
  }
  while( true ) {
    local_30 = std::__cxx11::basic_string<char,std::char_traits<char>,std::allocator<char>>::end();
    local_28 = __gnu_cxx::
               __normal_iterator<char*,std::__cxx11::basic_string<char,std::char_traits<char>,std::allocator<char>>>
               ::operator_((__normal_iterator_char__std____cxx11__basic_string_char_std__char_traits_char__std__allocator_char___
                            *)&local_30,1);
    bVar2 = __gnu_cxx::operator_((__normal_iterator *)&local_40,(__normal_iterator *)&local_28);
    if (bVar2 == false) break;
    local_28 = __gnu_cxx::
               __normal_iterator<char*,std::__cxx11::basic_string<char,std::char_traits<char>,std::allocator<char>>>
               ::operator__((__normal_iterator_char__std____cxx11__basic_string_char_std__char_traits_char__std__allocator_char___
                             *)&local_40,0);
    pcVar6 = (char *)__gnu_cxx::
                     __normal_iterator<char*,std::__cxx11::basic_string<char,std::char_traits<char>,std::allocator<char>>>
                     ::operator_((__normal_iterator_char__std____cxx11__basic_string_char_std__char_traits_char__std__allocator_char___
                                  *)&local_28);
    cVar3 = char2int(*pcVar6);
    pcVar6 = (char *)__gnu_cxx::
                     __normal_iterator<char*,std::__cxx11::basic_string<char,std::char_traits<char>,std::allocator<char>>>
                     ::operator_((__normal_iterator_char__std____cxx11__basic_string_char_std__char_traits_char__std__allocator_char___
                                  *)&local_40);
    bVar4 = char2int(*pcVar6);
    local_30 = __gnu_cxx::
               __normal_iterator<char*,std::__cxx11::basic_string<char,std::char_traits<char>,std::allocator<char>>>
               ::operator__((__normal_iterator_char__std____cxx11__basic_string_char_std__char_traits_char__std__allocator_char___
                             *)&local_38,0);
    pbVar8 = (byte *)__gnu_cxx::
                     __normal_iterator<char*,std::__cxx11::basic_string<char,std::char_traits<char>,std::allocator<char>>>
                     ::operator_((__normal_iterator_char__std____cxx11__basic_string_char_std__char_traits_char__std__allocator_char___
                                  *)&local_30);
    *pbVar8 = (byte)((int)cVar3 << 4) | bVar4;
    __gnu_cxx::
    __normal_iterator<char*,std::__cxx11::basic_string<char,std::char_traits<char>,std::allocator<char>>>
    ::operator__((__normal_iterator_char__std____cxx11__basic_string_char_std__char_traits_char__std__allocator_char___
                  *)&local_40,0);
  }
  if (local_20 != *(long *)(in_FS_OFFSET + 0x28)) {
                    // WARNING: Subroutine does not return
    __stack_chk_fail();
  }
  return CONCAT44(in_register_0000003c,param_1);
}



// WARNING: Could not reconcile some variable overlaps
// getSatellitePacketBytes(void*)

void getSatellitePacketBytes(void *param_1)

{
  _Setw _Var1;
  _Setfill _Var2;
  ssize_t sVar3;
  basic_ostream *pbVar4;
  allocator *paVar5;
  char *pcVar6;
  ulong uVar7;
  basic_ostream_char_std__char_traits_char__ *pbVar8;
  long lVar9;
  undefined8 *puVar10;
  long in_FS_OFFSET;
  byte bVar11;
  undefined auVar12 [16];
  allocator_char_ local_55e;
  uchar local_55d;
  int local_55c;
  int local_558;
  int local_554;
  int local_550;
  int local_54c;
  basic_string local_528 [8];
  basic_string local_508 [8];
  basic_string_char_std__char_traits_char__std__allocator_char__ local_4e8 [32];
  basic_string local_4c8 [8];
  basic_string local_4a8 [8];
  basic_string_char_std__char_traits_char__std__allocator_char__ local_488 [32];
  undefined local_468 [32];
  basic_string local_448 [4];
  basic_ostream_char_std__char_traits_char__ abStack1080 [384];
  basic_string local_2b8 [4];
  basic_ostream abStack680 [384];
  undefined8 local_128 [33];
  undefined8 local_20;
  
  bVar11 = 0;
  local_20 = *(undefined8 *)(in_FS_OFFSET + 0x28);
  local_55d = '\0';
  puVar10 = local_128;
  for (lVar9 = 0x20; lVar9 != 0; lVar9 = lVar9 + -1) {
    *puVar10 = 0;
    puVar10 = puVar10 + 1;
  }
  std::__cxx11::basic_stringstream<char,std::char_traits<char>,std::allocator<char>>::
  basic_stringstream();
  std::__cxx11::basic_string<char,std::char_traits<char>,std::allocator<char>>::basic_string();
  std::__cxx11::basic_string<char,std::char_traits<char>,std::allocator<char>>::basic_string();
                    // try { // try from 00103245 to 00103249 has its CatchHandler @ 001037b1
  std::__cxx11::basic_stringstream<char,std::char_traits<char>,std::allocator<char>>::str(local_448)
  ;
  std::__cxx11::basic_string<char,std::char_traits<char>,std::allocator<char>>::_basic_string
            ((basic_string_char_std__char_traits_char__std__allocator_char__ *)local_2b8);
  std::__cxx11::basic_string<char,std::char_traits<char>,std::allocator<char>>::clear();
  std::allocator<char>::allocator();
                    // try { // try from 0010328f to 00103293 has its CatchHandler @ 001037cc
  std::__cxx11::basic_string<char,std::char_traits<char>,std::allocator<char>>::basic_string
            ((char *)local_528,(allocator *)"1acffc1d");
  std::allocator<char>::_allocator(&local_55e);
                    // try { // try from 001032ad to 001032b1 has its CatchHandler @ 00103902
  std::__cxx11::basic_stringstream<char,std::char_traits<char>,std::allocator<char>>::
  basic_stringstream();
  std::__cxx11::basic_string<char,std::char_traits<char>,std::allocator<char>>::basic_string();
                    // try { // try from 001032d5 to 001032d9 has its CatchHandler @ 001037e7
  std::__cxx11::basic_stringstream<char,std::char_traits<char>,std::allocator<char>>::str(local_2b8)
  ;
  std::__cxx11::basic_string<char,std::char_traits<char>,std::allocator<char>>::_basic_string
            ((basic_string_char_std__char_traits_char__std__allocator_char__ *)local_468);
  do {
    local_55c = 0;
    while (local_55c < 2) {
                    // try { // try from 00103310 to 0010339c has its CatchHandler @ 001038ea
      sVar3 = read(0,local_128,0x100);
      local_55c = (int)sVar3;
    }
    for (local_558 = 0; local_558 < local_55c; local_558 = local_558 + 1) {
      pbVar4 = (basic_ostream *)
               std::basic_ostream<char,std::char_traits<char>>::operator__(abStack1080,std::hex);
      _Var1 = std::setw(2);
      pbVar4 = std::operator__(pbVar4,_Var1);
      _Var2 = std::setfill_char_('0');
      pbVar4 = std::operator__(pbVar4,_Var2);
      std::basic_ostream<char,std::char_traits<char>>::operator__
                ((basic_ostream_char_std__char_traits_char__ *)pbVar4,
                 (uint)*(byte *)((long)local_128 + (long)local_558));
    }
    std::__cxx11::basic_string<char,std::char_traits<char>,std::allocator<char>>::basic_string();
    local_550 = 0;
    std::__cxx11::basic_string<char,std::char_traits<char>,std::allocator<char>>::basic_string();
    std::allocator<char>::allocator();
                    // try { // try from 001033f1 to 001033f5 has its CatchHandler @ 00103832
    std::__cxx11::basic_stringstream<char,std::char_traits<char>,std::allocator<char>>::str();
    paVar5 = (allocator *)
             std::__cxx11::basic_string<char,std::char_traits<char>,std::allocator<char>>::c_str();
                    // try { // try from 0010341c to 00103420 has its CatchHandler @ 0010381a
    std::__cxx11::basic_string<char,std::char_traits<char>,std::allocator<char>>::basic_string
              (local_468,paVar5);
                    // try { // try from 00103435 to 00103439 has its CatchHandler @ 00103802
    hex2str((int)register0x00000020 - 0x4c8);
    std::__cxx11::basic_string<char,std::char_traits<char>,std::allocator<char>>::operator_
              ((basic_string_char_std__char_traits_char__std__allocator_char__ *)local_508,local_4c8
              );
    std::__cxx11::basic_string<char,std::char_traits<char>,std::allocator<char>>::_basic_string
              ((basic_string_char_std__char_traits_char__std__allocator_char__ *)local_4c8);
    std::__cxx11::basic_string<char,std::char_traits<char>,std::allocator<char>>::_basic_string
              ((basic_string_char_std__char_traits_char__std__allocator_char__ *)local_468);
    std::__cxx11::basic_string<char,std::char_traits<char>,std::allocator<char>>::_basic_string
              (local_488);
    std::allocator<char>::_allocator(&local_55e);
    pcVar6 = (char *)std::__cxx11::basic_string<char,std::char_traits<char>,std::allocator<char>>::
                     c_str();
    uVar7 = strtoul(pcVar6,(char **)0x0,0x10);
    local_550 = (int)uVar7;
                    // try { // try from 001034d1 to 001034d5 has its CatchHandler @ 0010384d
    std::__cxx11::basic_string<char,std::char_traits<char>,std::allocator<char>>::substr
              ((ulong)local_4a8,(ulong)local_508);
    std::__cxx11::basic_string<char,std::char_traits<char>,std::allocator<char>>::operator_
              (local_4e8,local_4a8);
    std::__cxx11::basic_string<char,std::char_traits<char>,std::allocator<char>>::_basic_string
              ((basic_string_char_std__char_traits_char__std__allocator_char__ *)local_4a8);
    pcVar6 = (char *)std::__cxx11::basic_string<char,std::char_traits<char>,std::allocator<char>>::
                     c_str();
    uVar7 = strtoul(pcVar6,(char **)0x0,0x10);
    local_550 = (int)uVar7;
    if (local_550 == 7) {
                    // try { // try from 0010354a to 001035fc has its CatchHandler @ 001038c3
      pbVar4 = std::operator__(abStack680,local_528);
      std::operator__(pbVar4,local_508);
      std::basic_istream<char,std::char_traits<char>>::seekg((long)local_2b8,0);
      auVar12 = std::basic_istream<char,std::char_traits<char>>::tellg();
      local_468._0_16_ = auVar12;
      lVar9 = std::fpos::operator_cast_to_long((fpos *)local_468);
      local_54c = (int)lVar9;
      std::basic_istream<char,std::char_traits<char>>::seekg((long)local_2b8,0);
      for (local_554 = 0; local_554 < local_54c; local_554 = local_554 + 2) {
        std::__cxx11::basic_stringstream<char,std::char_traits<char>,std::allocator<char>>::str();
                    // try { // try from 0010361c to 00103620 has its CatchHandler @ 00103863
        std::__cxx11::basic_string<char,std::char_traits<char>,std::allocator<char>>::substr
                  ((ulong)local_488,(ulong)local_468);
        std::__cxx11::basic_string<char,std::char_traits<char>,std::allocator<char>>::_basic_string
                  ((basic_string_char_std__char_traits_char__std__allocator_char__ *)local_468);
        pcVar6 = (char *)std::__cxx11::
                         basic_string<char,std::char_traits<char>,std::allocator<char>>::c_str();
        uVar7 = strtoul(pcVar6,(char **)0x0,0x10);
        local_55d = (uchar)uVar7;
                    // try { // try from 00103667 to 0010366b has its CatchHandler @ 0010387b
        Queue::enQueue((Queue *)q,local_55d);
        std::__cxx11::basic_string<char,std::char_traits<char>,std::allocator<char>>::_basic_string
                  (local_488);
      }
    }
    else {
                    // try { // try from 00103698 to 001036ee has its CatchHandler @ 001038c3
      pbVar8 = (basic_ostream_char_std__char_traits_char__ *)
               std::basic_ostream<char,std::char_traits<char>>::operator__
                         ((basic_ostream_char_std__char_traits_char__ *)std::cout,
                          std::endl_char_std__char_traits_char__);
      std::basic_ostream<char,std::char_traits<char>>::operator__
                (pbVar8,std::endl_char_std__char_traits_char__);
      pbVar4 = std::operator__((basic_ostream *)std::cout,
                               "That sequence of hex characters did not work. Try again.");
      pbVar8 = (basic_ostream_char_std__char_traits_char__ *)
               std::basic_ostream<char,std::char_traits<char>>::operator__
                         ((basic_ostream_char_std__char_traits_char__ *)pbVar4,
                          std::endl_char_std__char_traits_char__);
      std::basic_ostream<char,std::char_traits<char>>::operator__
                (pbVar8,std::endl_char_std__char_traits_char__);
    }
    std::__cxx11::basic_string<char,std::char_traits<char>,std::allocator<char>>::basic_string();
                    // try { // try from 00103712 to 00103716 has its CatchHandler @ 00103893
    std::__cxx11::basic_stringstream<char,std::char_traits<char>,std::allocator<char>>::str
              (local_2b8);
    std::__cxx11::basic_string<char,std::char_traits<char>,std::allocator<char>>::_basic_string
              ((basic_string_char_std__char_traits_char__std__allocator_char__ *)local_468);
    std::__cxx11::basic_string<char,std::char_traits<char>,std::allocator<char>>::basic_string();
                    // try { // try from 00103749 to 0010374d has its CatchHandler @ 001038ab
    std::__cxx11::basic_stringstream<char,std::char_traits<char>,std::allocator<char>>::str
              (local_448);
    std::__cxx11::basic_string<char,std::char_traits<char>,std::allocator<char>>::_basic_string
              ((basic_string_char_std__char_traits_char__std__allocator_char__ *)local_468);
    puVar10 = local_128;
    for (lVar9 = 0x20; lVar9 != 0; lVar9 = lVar9 + -1) {
      *puVar10 = 0;
      puVar10 = puVar10 + (ulong)bVar11 * -2 + 1;
    }
    local_55c = 0;
                    // try { // try from 00103789 to 0010385d has its CatchHandler @ 001038c3
    usleep(500000);
    std::__cxx11::basic_string<char,std::char_traits<char>,std::allocator<char>>::_basic_string
              (local_4e8);
    std::__cxx11::basic_string<char,std::char_traits<char>,std::allocator<char>>::_basic_string
              ((basic_string_char_std__char_traits_char__std__allocator_char__ *)local_508);
  } while( true );
}



// processSatellitePacketBytes(void*)

void processSatellitePacketBytes(void *param_1)

{
  char cVar1;
  _Setw _Var2;
  _Setfill _Var3;
  int iVar4;
  basic_ostream *pbVar5;
  char *pcVar6;
  ulong uVar7;
  byte *pbVar8;
  void *__s2;
  void *__s1;
  basic_ostream_char_std__char_traits_char__ *pbVar9;
  long in_FS_OFFSET;
  byte local_69b;
  allocator_char_ local_69a;
  byte local_699;
  char local_698;
  char local_697;
  char local_696;
  char local_695;
  byte local_694;
  undefined local_693;
  undefined local_692;
  undefined local_691;
  undefined local_690;
  undefined local_68f;
  undefined local_68e;
  undefined local_68d;
  undefined local_68c;
  undefined local_68b;
  undefined local_68a;
  undefined local_689;
  uint local_688;
  uint local_684;
  uint local_680;
  uint local_67c;
  uint local_678;
  uint local_674;
  uint local_670;
  uint local_66c;
  undefined4 local_668;
  uint local_664;
  uint local_660;
  int local_65c;
  queue_unsigned_char_std__deque_unsigned_char_std__allocator_unsigned_char___ local_658 [80];
  queue_unsigned_char_std__deque_unsigned_char_std__allocator_unsigned_char___ local_608 [80];
  basic_string_char_std__char_traits_char__std__allocator_char__ local_5b8 [32];
  basic_string_char_std__char_traits_char__std__allocator_char__ local_598 [32];
  basic_string_char_std__char_traits_char__std__allocator_char__ local_578 [32];
  basic_string_char_std__char_traits_char__std__allocator_char__ local_558 [32];
  basic_string local_538 [8];
  basic_string local_518 [8];
  basic_string local_4f8 [8];
  basic_string local_4d8 [4];
  basic_ostream_char_std__char_traits_char__ abStack1224 [384];
  basic_string local_348 [4];
  basic_ostream_char_std__char_traits_char__ abStack824 [384];
  basic_string local_1b8 [4];
  basic_ostream_char_std__char_traits_char__ abStack424 [382];
  undefined4 local_2a;
  byte local_26 [6];
  undefined8 local_20;
  
  local_20 = *(undefined8 *)(in_FS_OFFSET + 0x28);
  std::queue<unsigned_char,std::deque<unsigned_char,std::allocator<unsigned_char>>>::
  queue_std__deque_unsigned_char_std__allocator_unsigned_char___void_(local_658);
                    // try { // try from 00103980 to 00103984 has its CatchHandler @ 00104a81
  std::queue<unsigned_char,std::deque<unsigned_char,std::allocator<unsigned_char>>>::
  queue_std__deque_unsigned_char_std__allocator_unsigned_char___void_(local_608);
  local_699 = 0;
                    // try { // try from 00103996 to 0010399a has its CatchHandler @ 00104a69
  std::__cxx11::basic_stringstream<char,std::char_traits<char>,std::allocator<char>>::
  basic_stringstream();
  std::__cxx11::basic_string<char,std::char_traits<char>,std::allocator<char>>::basic_string();
                    // try { // try from 001039be to 001039c2 has its CatchHandler @ 001047a8
  std::__cxx11::basic_stringstream<char,std::char_traits<char>,std::allocator<char>>::str(local_4d8)
  ;
  std::__cxx11::basic_string<char,std::char_traits<char>,std::allocator<char>>::_basic_string
            ((basic_string_char_std__char_traits_char__std__allocator_char__ *)local_1b8);
                    // try { // try from 001039dc to 001039e0 has its CatchHandler @ 00104a51
  std::__cxx11::basic_stringstream<char,std::char_traits<char>,std::allocator<char>>::
  basic_stringstream();
  std::__cxx11::basic_string<char,std::char_traits<char>,std::allocator<char>>::basic_string();
                    // try { // try from 00103a04 to 00103a08 has its CatchHandler @ 001047c3
  std::__cxx11::basic_stringstream<char,std::char_traits<char>,std::allocator<char>>::str(local_348)
  ;
  std::__cxx11::basic_string<char,std::char_traits<char>,std::allocator<char>>::_basic_string
            ((basic_string_char_std__char_traits_char__std__allocator_char__ *)local_1b8);
  local_698 = '\0';
  local_697 = '\0';
  local_696 = '\0';
  local_693 = 0;
  local_67c = 0;
  local_678 = 0;
  local_674 = 0;
  local_688 = 0;
  local_670 = 0;
  local_66c = 0;
  local_684 = 0;
  local_668 = 0;
  local_680 = 0;
  std::__cxx11::basic_string<char,std::char_traits<char>,std::allocator<char>>::basic_string();
  std::__cxx11::basic_string<char,std::char_traits<char>,std::allocator<char>>::basic_string();
  std::__cxx11::basic_string<char,std::char_traits<char>,std::allocator<char>>::basic_string();
  std::__cxx11::basic_string<char,std::char_traits<char>,std::allocator<char>>::basic_string();
  local_664 = 0;
  local_660 = 0;
  local_65c = 0;
  local_695 = '\0';
  local_692 = 0;
  local_691 = 0;
  local_690 = 0;
  local_68f = 0;
  local_68e = 0;
  local_68d = 0;
  local_68c = 0;
  local_68b = 0;
  local_68a = 0;
  local_689 = 0;
  while( true ) {
    while( true ) {
      while( true ) {
        local_693 = 0;
        cVar1 = Queue::isEmpty((Queue *)q);
        if (cVar1 == '\0') break;
        if (local_695 == '\x01') {
          pbVar9 = (basic_ostream_char_std__char_traits_char__ *)
                   std::basic_ostream<char,std::char_traits<char>>::operator__
                             ((basic_ostream_char_std__char_traits_char__ *)std::cout,
                              std::endl_char_std__char_traits_char__);
          std::basic_ostream<char,std::char_traits<char>>::operator__
                    (pbVar9,std::endl_char_std__char_traits_char__);
          pbVar5 = std::operator__((basic_ostream *)std::cout,
                                   "That sequence of hex characters did not work. Try again.");
          pbVar9 = (basic_ostream_char_std__char_traits_char__ *)
                   std::basic_ostream<char,std::char_traits<char>>::operator__
                             ((basic_ostream_char_std__char_traits_char__ *)pbVar5,
                              std::endl_char_std__char_traits_char__);
          std::basic_ostream<char,std::char_traits<char>>::operator__
                    (pbVar9,std::endl_char_std__char_traits_char__);
          local_695 = '\0';
        }
        usleep(100000);
      }
                    // try { // try from 00103b5b to 00103cb7 has its CatchHandler @ 001049fd
      local_69b = Queue::deQueue((Queue *)q);
      local_695 = '\x01';
      if (local_696 != '\x01') break;
      if (local_680 < local_684) {
        pbVar5 = (basic_ostream *)
                 std::basic_ostream<char,std::char_traits<char>>::operator__(abStack824,std::hex);
        _Var2 = std::setw(2);
        pbVar5 = std::operator__(pbVar5,_Var2);
        _Var3 = std::setfill_char_('0');
        pbVar5 = std::operator__(pbVar5,_Var3);
        std::basic_ostream<char,std::char_traits<char>>::operator__
                  ((basic_ostream_char_std__char_traits_char__ *)pbVar5,(uint)local_69b);
        if (local_680 == local_684 - 1) {
          if (local_688 == 1) {
            std::__cxx11::basic_stringstream<char,std::char_traits<char>,std::allocator<char>>::str
                      ();
                    // try { // try from 00103cd3 to 00103cd7 has its CatchHandler @ 001047de
            std::__cxx11::basic_string<char,std::char_traits<char>,std::allocator<char>>::substr
                      ((ulong)local_4f8,(ulong)local_1b8);
            std::__cxx11::basic_string<char,std::char_traits<char>,std::allocator<char>>::
            _basic_string((basic_string_char_std__char_traits_char__std__allocator_char__ *)
                          local_1b8);
            pcVar6 = (char *)std::__cxx11::
                             basic_string<char,std::char_traits<char>,std::allocator<char>>::c_str()
            ;
            uVar7 = strtoul(pcVar6,(char **)0x0,0x10);
            local_668 = (undefined4)uVar7;
            std::__cxx11::basic_string<char,std::char_traits<char>,std::allocator<char>>::
            basic_string();
                    // try { // try from 00103d31 to 00103d35 has its CatchHandler @ 001047f9
            std::__cxx11::basic_stringstream<char,std::char_traits<char>,std::allocator<char>>::str
                      (local_348);
            std::__cxx11::basic_string<char,std::char_traits<char>,std::allocator<char>>::
            _basic_string((basic_string_char_std__char_traits_char__std__allocator_char__ *)
                          local_1b8);
            switch(local_668) {
            default:
              local_693 = 1;
              break;
            case 1:
                    // try { // try from 00103d80 to 00103e2f has its CatchHandler @ 00104811
              puts("Handling Power Telemetry");
              break;
            case 2:
              puts("Handling Guidance Telemetry");
              break;
            case 3:
              puts("Handling CDH Telemetry");
              break;
            case 4:
              puts("Handling Communications Telemetry");
              break;
            case 5:
              puts("Handling Payload Telemetry");
              break;
            case 6:
              puts("Handling Attitude Telemetry");
              break;
            case 7:
              puts("Handling Test Telemetry");
              break;
            case 8:
              puts("EMERGENCY_MODE: THE SPACECRAFT IS IN EMERGENCY_MODE");
              puts("You made it!\nHere\'s your flag:");
              puts(flag);
                    // WARNING: Subroutine does not return
              exit(0);
            }
            fflush(stdout);
            usleep(500000);
            std::__cxx11::basic_string<char,std::char_traits<char>,std::allocator<char>>::
            _basic_string((basic_string_char_std__char_traits_char__std__allocator_char__ *)
                          local_4f8);
          }
          local_696 = '\0';
          local_680 = 0;
          local_684 = 0;
        }
        else {
          local_680 = local_680 + 1;
        }
      }
    }
                    // try { // try from 00103e7f to 00103ee9 has its CatchHandler @ 001049fd
    std::queue<unsigned_char,std::deque<unsigned_char,std::allocator<unsigned_char>>>::push
              (local_658,&local_69b);
    uVar7 = std::queue<unsigned_char,std::deque<unsigned_char,std::allocator<unsigned_char>>>::size
                      (local_658);
    if (4 < uVar7) {
      std::queue<unsigned_char,std::deque<unsigned_char,std::allocator<unsigned_char>>>::pop
                (local_658);
      std::queue<unsigned_char,std::deque<unsigned_char,std::allocator<unsigned_char>>>::operator_
                (local_608,(queue *)local_658);
      local_2a = 0;
      local_694 = 0;
      std::__cxx11::basic_stringstream<char,std::char_traits<char>,std::allocator<char>>::
      basic_stringstream();
      while (cVar1 = std::
                     queue<unsigned_char,std::deque<unsigned_char,std::allocator<unsigned_char>>>::
                     empty(local_608), cVar1 != '\x01') {
                    // try { // try from 00103f19 to 00103fca has its CatchHandler @ 00104853
        pbVar5 = (basic_ostream *)
                 std::basic_ostream<char,std::char_traits<char>>::operator__(abStack424,std::hex);
        _Var2 = std::setw(2);
        pbVar5 = std::operator__(pbVar5,_Var2);
        _Var3 = std::setfill_char_('0');
        pbVar5 = std::operator__(pbVar5,_Var3);
        pbVar8 = (byte *)std::
                         queue<unsigned_char,std::deque<unsigned_char,std::allocator<unsigned_char>>>
                         ::front();
        std::basic_ostream<char,std::char_traits<char>>::operator__
                  ((basic_ostream_char_std__char_traits_char__ *)pbVar5,(uint)*pbVar8);
        pbVar8 = (byte *)std::
                         queue<unsigned_char,std::deque<unsigned_char,std::allocator<unsigned_char>>>
                         ::front();
        local_26[(long)(int)(uint)local_694 + -4] = *pbVar8;
        std::queue<unsigned_char,std::deque<unsigned_char,std::allocator<unsigned_char>>>::pop
                  (local_608);
        local_694 = local_694 + 1;
      }
      std::__cxx11::basic_stringstream<char,std::char_traits<char>,std::allocator<char>>::str();
      std::allocator<char>::allocator();
                    // try { // try from 00103ff2 to 00103ff6 has its CatchHandler @ 0010482c
      std::__cxx11::basic_string<char,std::char_traits<char>,std::allocator<char>>::basic_string
                ((char *)local_4f8,(allocator *)"1acffc1d");
      std::allocator<char>::_allocator(&local_69a);
      __s2 = (void *)std::__cxx11::basic_string<char,std::char_traits<char>,std::allocator<char>>::
                     c_str();
      __s1 = (void *)std::__cxx11::basic_string<char,std::char_traits<char>,std::allocator<char>>::
                     c_str();
      iVar4 = memcmp(__s1,__s2,4);
      if (iVar4 == 0) {
        local_698 = '\x01';
        local_697 = '\x01';
      }
      std::__cxx11::basic_string<char,std::char_traits<char>,std::allocator<char>>::_basic_string
                ((basic_string_char_std__char_traits_char__std__allocator_char__ *)local_4f8);
      std::__cxx11::basic_string<char,std::char_traits<char>,std::allocator<char>>::_basic_string
                ((basic_string_char_std__char_traits_char__std__allocator_char__ *)local_518);
      std::__cxx11::basic_stringstream<char,std::char_traits<char>,std::allocator<char>>::
      _basic_stringstream((basic_stringstream_char_std__char_traits_char__std__allocator_char__ *)
                          local_1b8);
    }
    if (local_698 != '\x01') break;
    if (local_697 == '\x01') {
      local_697 = '\0';
      goto LAB_0010478d;
    }
    if (5 < local_699) goto LAB_0010478d;
                    // try { // try from 001040c5 to 001042f9 has its CatchHandler @ 001049fd
    pbVar5 = (basic_ostream *)
             std::basic_ostream<char,std::char_traits<char>>::operator__(abStack1224,std::hex);
    _Var2 = std::setw(2);
    pbVar5 = std::operator__(pbVar5,_Var2);
    _Var3 = std::setfill_char_('0');
    pbVar5 = std::operator__(pbVar5,_Var3);
    std::basic_ostream<char,std::char_traits<char>>::operator__
              ((basic_ostream_char_std__char_traits_char__ *)pbVar5,(uint)local_69b);
    local_26[local_699] = local_69b;
    if (local_699 != 5) {
      local_692 = 0;
      local_699 = local_699 + 1;
      goto LAB_0010478d;
    }
    local_698 = '\0';
    local_699 = 0;
    std::__cxx11::basic_stringstream<char,std::char_traits<char>,std::allocator<char>>::str();
    std::__cxx11::basic_string<char,std::char_traits<char>,std::allocator<char>>::operator_
              (local_5b8,local_538);
    std::__cxx11::basic_string<char,std::char_traits<char>,std::allocator<char>>::_basic_string
              ((basic_string_char_std__char_traits_char__std__allocator_char__ *)local_538);
    std::__cxx11::basic_string<char,std::char_traits<char>,std::allocator<char>>::substr
              ((ulong)local_518,(ulong)local_5b8);
    std::__cxx11::basic_string<char,std::char_traits<char>,std::allocator<char>>::operator_
              (local_598,local_518);
    std::__cxx11::basic_string<char,std::char_traits<char>,std::allocator<char>>::_basic_string
              ((basic_string_char_std__char_traits_char__std__allocator_char__ *)local_518);
    std::__cxx11::basic_string<char,std::char_traits<char>,std::allocator<char>>::substr
              ((ulong)local_4f8,(ulong)local_5b8);
    std::__cxx11::basic_string<char,std::char_traits<char>,std::allocator<char>>::operator_
              (local_578,local_4f8);
    std::__cxx11::basic_string<char,std::char_traits<char>,std::allocator<char>>::_basic_string
              ((basic_string_char_std__char_traits_char__std__allocator_char__ *)local_4f8);
    std::__cxx11::basic_string<char,std::char_traits<char>,std::allocator<char>>::substr
              ((ulong)local_1b8,(ulong)local_5b8);
    std::__cxx11::basic_string<char,std::char_traits<char>,std::allocator<char>>::operator_
              (local_558,local_1b8);
    std::__cxx11::basic_string<char,std::char_traits<char>,std::allocator<char>>::_basic_string
              ((basic_string_char_std__char_traits_char__std__allocator_char__ *)local_1b8);
    pcVar6 = (char *)std::__cxx11::basic_string<char,std::char_traits<char>,std::allocator<char>>::
                     c_str();
    uVar7 = strtoul(pcVar6,(char **)0x0,0x10);
    local_664 = (uint)uVar7;
    pcVar6 = (char *)std::__cxx11::basic_string<char,std::char_traits<char>,std::allocator<char>>::
                     c_str();
    uVar7 = strtoul(pcVar6,(char **)0x0,0x10);
    local_660 = (uint)uVar7;
    pcVar6 = (char *)std::__cxx11::basic_string<char,std::char_traits<char>,std::allocator<char>>::
                     c_str();
    uVar7 = strtoul(pcVar6,(char **)0x0,0x10);
    local_65c = (int)uVar7;
    pbVar9 = (basic_ostream_char_std__char_traits_char__ *)
             std::basic_ostream<char,std::char_traits<char>>::operator__
                       ((basic_ostream_char_std__char_traits_char__ *)std::cout,
                        std::endl_char_std__char_traits_char__);
    std::basic_ostream<char,std::char_traits<char>>::operator__
              (pbVar9,std::endl_char_std__char_traits_char__);
    std::__cxx11::basic_string<char,std::char_traits<char>,std::allocator<char>>::basic_string();
                    // try { // try from 0010431d to 00104321 has its CatchHandler @ 0010486e
    std::__cxx11::basic_stringstream<char,std::char_traits<char>,std::allocator<char>>::str
              (local_4d8);
    std::__cxx11::basic_string<char,std::char_traits<char>,std::allocator<char>>::_basic_string
              ((basic_string_char_std__char_traits_char__std__allocator_char__ *)local_1b8);
    std::__cxx11::basic_string<char,std::char_traits<char>,std::allocator<char>>::clear();
    std::__cxx11::basic_string<char,std::char_traits<char>,std::allocator<char>>::clear();
    std::__cxx11::basic_string<char,std::char_traits<char>,std::allocator<char>>::clear();
    std::__cxx11::basic_string<char,std::char_traits<char>,std::allocator<char>>::clear();
    local_67c = readPktPrimHdrBitField(APID,local_664);
    local_678 = readPktPrimHdrBitField(PVN,local_664);
    local_674 = readPktPrimHdrBitField(PKT_TYPE,local_664);
    local_688 = readPktPrimHdrBitField(SEC_HDR_FLAG,local_664);
    local_670 = readPktPrimHdrBitField(SEQ_FLAGS,local_660);
    local_66c = readPktPrimHdrBitField(PKT_SEQ_CNT_OR_PKT_NAME,local_660);
    local_684 = local_65c + 1;
    std::allocator<char>::allocator();
                    // try { // try from 00104445 to 00104449 has its CatchHandler @ 001048a1
    std::__cxx11::basic_string<char,std::char_traits<char>,std::allocator<char>>::basic_string
              ((char *)local_1b8,(allocator *)"Packet Version Number");
    iVar4 = (int)register0x00000020;
                    // try { // try from 0010445c to 00104460 has its CatchHandler @ 00104889
    printDebug(iVar4 - 0x1b8,local_678);
    std::__cxx11::basic_string<char,std::char_traits<char>,std::allocator<char>>::_basic_string
              ((basic_string_char_std__char_traits_char__std__allocator_char__ *)local_1b8);
    std::allocator<char>::_allocator(&local_69a);
    std::allocator<char>::allocator();
                    // try { // try from 001044a6 to 001044aa has its CatchHandler @ 001048d4
    std::__cxx11::basic_string<char,std::char_traits<char>,std::allocator<char>>::basic_string
              ((char *)local_1b8,(allocator *)"Packet Type");
                    // try { // try from 001044bd to 001044c1 has its CatchHandler @ 001048bc
    printDebug(iVar4 - 0x1b8,local_674);
    std::__cxx11::basic_string<char,std::char_traits<char>,std::allocator<char>>::_basic_string
              ((basic_string_char_std__char_traits_char__std__allocator_char__ *)local_1b8);
    std::allocator<char>::_allocator(&local_69a);
    std::allocator<char>::allocator();
                    // try { // try from 00104507 to 0010450b has its CatchHandler @ 00104907
    std::__cxx11::basic_string<char,std::char_traits<char>,std::allocator<char>>::basic_string
              ((char *)local_1b8,(allocator *)"Secondary Header Flag");
                    // try { // try from 0010451e to 00104522 has its CatchHandler @ 001048ef
    printDebug(iVar4 - 0x1b8,local_688);
    std::__cxx11::basic_string<char,std::char_traits<char>,std::allocator<char>>::_basic_string
              ((basic_string_char_std__char_traits_char__std__allocator_char__ *)local_1b8);
    std::allocator<char>::_allocator(&local_69a);
    std::allocator<char>::allocator();
                    // try { // try from 00104568 to 0010456c has its CatchHandler @ 0010493a
    std::__cxx11::basic_string<char,std::char_traits<char>,std::allocator<char>>::basic_string
              ((char *)local_1b8,(allocator *)"Application Process Identifier");
                    // try { // try from 0010457f to 00104583 has its CatchHandler @ 00104922
    printDebug(iVar4 - 0x1b8,local_67c);
    std::__cxx11::basic_string<char,std::char_traits<char>,std::allocator<char>>::_basic_string
              ((basic_string_char_std__char_traits_char__std__allocator_char__ *)local_1b8);
    std::allocator<char>::_allocator(&local_69a);
    std::allocator<char>::allocator();
                    // try { // try from 001045c9 to 001045cd has its CatchHandler @ 0010496d
    std::__cxx11::basic_string<char,std::char_traits<char>,std::allocator<char>>::basic_string
              ((char *)local_1b8,(allocator *)"Sequence Flags");
                    // try { // try from 001045e0 to 001045e4 has its CatchHandler @ 00104955
    printDebug(iVar4 - 0x1b8,local_670);
    std::__cxx11::basic_string<char,std::char_traits<char>,std::allocator<char>>::_basic_string
              ((basic_string_char_std__char_traits_char__std__allocator_char__ *)local_1b8);
    std::allocator<char>::_allocator(&local_69a);
    std::allocator<char>::allocator();
                    // try { // try from 0010462a to 0010462e has its CatchHandler @ 0010499d
    std::__cxx11::basic_string<char,std::char_traits<char>,std::allocator<char>>::basic_string
              ((char *)local_1b8,(allocator *)"Packet Sequence Count or Packet Name");
                    // try { // try from 00104641 to 00104645 has its CatchHandler @ 00104985
    printDebug(iVar4 - 0x1b8,local_66c);
    std::__cxx11::basic_string<char,std::char_traits<char>,std::allocator<char>>::_basic_string
              ((basic_string_char_std__char_traits_char__std__allocator_char__ *)local_1b8);
    std::allocator<char>::_allocator(&local_69a);
    std::allocator<char>::allocator();
                    // try { // try from 0010468b to 0010468f has its CatchHandler @ 001049cd
    std::__cxx11::basic_string<char,std::char_traits<char>,std::allocator<char>>::basic_string
              ((char *)local_1b8,(allocator *)"Packet Data Length");
                    // try { // try from 001046a2 to 001046a6 has its CatchHandler @ 001049b5
    printDebug(iVar4 - 0x1b8,local_684);
    std::__cxx11::basic_string<char,std::char_traits<char>,std::allocator<char>>::_basic_string
              ((basic_string_char_std__char_traits_char__std__allocator_char__ *)local_1b8);
    std::allocator<char>::_allocator(&local_69a);
                    // try { // try from 001046d6 to 001046ef has its CatchHandler @ 001049fd
    pbVar9 = (basic_ostream_char_std__char_traits_char__ *)
             std::basic_ostream<char,std::char_traits<char>>::operator__
                       ((basic_ostream_char_std__char_traits_char__ *)std::cout,
                        std::endl_char_std__char_traits_char__);
    std::basic_ostream<char,std::char_traits<char>>::operator__
              (pbVar9,std::endl_char_std__char_traits_char__);
    local_692 = 1;
    local_691 = 0;
    if (((local_678 == 0) && (local_674 == 0)) && ((local_67c != 0x7ff || (local_688 != 1)))) {
      local_696 = '\x01';
      local_698 = '\0';
LAB_0010478d:
                    // try { // try from 00104792 to 00104796 has its CatchHandler @ 001049fd
      usleep(500000);
    }
  }
  std::__cxx11::basic_string<char,std::char_traits<char>,std::allocator<char>>::basic_string();
                    // try { // try from 00104779 to 0010477d has its CatchHandler @ 001049e5
  std::__cxx11::basic_stringstream<char,std::char_traits<char>,std::allocator<char>>::str(local_4d8)
  ;
  std::__cxx11::basic_string<char,std::char_traits<char>,std::allocator<char>>::_basic_string
            ((basic_string_char_std__char_traits_char__std__allocator_char__ *)local_1b8);
  goto LAB_0010478d;
}



void main(void)

{
  long in_FS_OFFSET;
  undefined4 local_30;
  int local_2c;
  undefined local_28 [8];
  undefined auStack32 [8];
  undefined8 local_18;
  undefined8 local_10;
  
  local_10 = *(undefined8 *)(in_FS_OFFSET + 0x28);
  printIntroArt();
  printIntroText();
  local_2c = thrd_create(local_28,getSatellitePacketBytes,0);
  if (local_2c == 2) {
    puts("ERROR; thrd_create() call failed for thread 0.");
                    // WARNING: Subroutine does not return
    exit(1);
  }
  local_2c = thrd_create(auStack32,processSatellitePacketBytes,0);
  if (local_2c == 2) {
    puts("ERROR; thrd_create() call failed for thread 1.");
                    // WARNING: Subroutine does not return
    exit(1);
  }
  local_2c = thrd_create(&local_18,startTimer,0);
  if (local_2c == 2) {
    puts("ERROR; thrd_create() call failed for thread 2.");
                    // WARNING: Subroutine does not return
    exit(1);
  }
  local_30 = 0;
  thrd_join(local_18,&local_30);
                    // WARNING: Subroutine does not return
  exit(0);
}



// __static_initialization_and_destruction_0(int, int)

void __static_initialization_and_destruction_0(int param_1,int param_2)

{
  if ((param_1 == 1) && (param_2 == 0xffff)) {
    std::ios_base::Init::Init((Init *)&std::__ioinit);
    __cxa_atexit(std::ios_base::Init::_Init,&std::__ioinit,&__dso_handle);
    flag = getenv("FLAG");
    PVN = pktPrimHdrBitDefn(0xd,0xf);
    PKT_TYPE = pktPrimHdrBitDefn(0xc,0xc);
    SEC_HDR_FLAG = pktPrimHdrBitDefn(0xb,0xb);
    APID = pktPrimHdrBitDefn(0,10);
    SEQ_FLAGS = pktPrimHdrBitDefn(0xe,0xf);
    PKT_SEQ_CNT_OR_PKT_NAME = pktPrimHdrBitDefn(0,0xd);
    Queue::Queue((Queue *)q);
  }
  return;
}



void _GLOBAL__sub_I_flag(void)

{
  __static_initialization_and_destruction_0(1,0xffff);
  return;
}



// operator new(unsigned long, void*)

void * operator_new(ulong param_1,void *param_2)

{
  return param_2;
}



// WARNING: Unknown calling convention yet parameter storage is locked
// std::TEMPNAMEPLACEHOLDERVALUE(std::_Ios_Fmtflags, std::_Ios_Fmtflags)

uint std::operator_(_Ios_Fmtflags param_1,_Ios_Fmtflags param_2)

{
  return param_1 & param_2;
}



// WARNING: Unknown calling convention yet parameter storage is locked
// std::TEMPNAMEPLACEHOLDERVALUE(std::_Ios_Fmtflags, std::_Ios_Fmtflags)

uint std::operator_(_Ios_Fmtflags param_1,_Ios_Fmtflags param_2)

{
  return param_1 | param_2;
}



// WARNING: Unknown calling convention yet parameter storage is locked
// std::TEMPNAMEPLACEHOLDERVALUE(std::_Ios_Fmtflags)

uint std::operator_(_Ios_Fmtflags param_1)

{
  return ~param_1;
}



// WARNING: Unknown calling convention yet parameter storage is locked
// std::TEMPNAMEPLACEHOLDERVALUE(std::_Ios_Fmtflags&, std::_Ios_Fmtflags)

_Ios_Fmtflags * std::operator__(_Ios_Fmtflags *param_1,_Ios_Fmtflags param_2)

{
  _Ios_Fmtflags _Var1;
  
  _Var1 = operator_(*param_1,param_2);
  *param_1 = _Var1;
  return param_1;
}



// WARNING: Unknown calling convention yet parameter storage is locked
// std::TEMPNAMEPLACEHOLDERVALUE(std::_Ios_Fmtflags&, std::_Ios_Fmtflags)

_Ios_Fmtflags * std::operator__(_Ios_Fmtflags *param_1,_Ios_Fmtflags param_2)

{
  _Ios_Fmtflags _Var1;
  
  _Var1 = operator_(*param_1,param_2);
  *param_1 = _Var1;
  return param_1;
}



// std::ios_base::setf(std::_Ios_Fmtflags, std::_Ios_Fmtflags)

undefined4 __thiscall
std::ios_base::setf(ios_base *this,_Ios_Fmtflags param_1,_Ios_Fmtflags param_2)

{
  undefined4 uVar1;
  _Ios_Fmtflags _Var2;
  
  uVar1 = *(undefined4 *)(this + 0x18);
  _Var2 = operator_(param_2);
  operator__((_Ios_Fmtflags *)(this + 0x18),_Var2);
  _Var2 = operator_(param_1,param_2);
  operator__((_Ios_Fmtflags *)(this + 0x18),_Var2);
  return uVar1;
}



// WARNING: Unknown calling convention yet parameter storage is locked
// std::hex(std::ios_base&)

ios_base * std::hex(ios_base *param_1)

{
  ios_base::setf(param_1,8,0x4a);
  return param_1;
}



// WARNING: Unknown calling convention yet parameter storage is locked
// std::setw(int)

int std::setw(int param_1)

{
  return param_1;
}



// WARNING: Unknown calling convention yet parameter storage is locked
// std::__deque_buf_size(unsigned long)

undefined8 std::__deque_buf_size(ulong param_1)

{
  undefined8 uVar1;
  
  if (param_1 < 0x200) {
    uVar1 = SUB168((ZEXT816(0) << 0x40 | ZEXT816(0x200)) / ZEXT816(param_1),0);
  }
  else {
    uVar1 = 1;
  }
  return uVar1;
}



// Queue::Queue()

void __thiscall Queue::Queue(Queue *this)

{
  this[0x800] = (Queue)0xff;
  this[0x801] = (Queue)0xff;
  return;
}



// Queue::isFull()

bool __thiscall Queue::isFull(Queue *this)

{
  return (int)(char)this[0x800] == (char)this[0x801] + 1;
}



// Queue::isEmpty()

bool __thiscall Queue::isEmpty(Queue *this)

{
  return this[0x800] == (Queue)0xff;
}



// Queue::enQueue(unsigned char)

void __thiscall Queue::enQueue(Queue *this,uchar param_1)

{
  char cVar1;
  basic_ostream *pbVar2;
  
  cVar1 = isFull(this);
  if (cVar1 == '\0') {
    if (this[0x800] == (Queue)0xff) {
      this[0x800] = (Queue)0x0;
    }
    this[0x801] = (Queue)((char)this[0x801] + '\x01');
    this[(int)(char)this[0x801]] = (Queue)param_1;
  }
  else {
    pbVar2 = (basic_ostream *)
             std::basic_ostream<char,std::char_traits<char>>::operator__
                       ((basic_ostream_char_std__char_traits_char__ *)std::cout,
                        std::endl_char_std__char_traits_char__);
    pbVar2 = std::operator__(pbVar2,"Queue is full");
    std::basic_ostream<char,std::char_traits<char>>::operator__
              ((basic_ostream_char_std__char_traits_char__ *)pbVar2,
               std::endl_char_std__char_traits_char__);
  }
  return;
}



// Queue::deQueue()

ulong __thiscall Queue::deQueue(Queue *this)

{
  Queue QVar1;
  char cVar2;
  basic_ostream *pbVar3;
  ulong uVar4;
  
  cVar2 = isEmpty(this);
  if (cVar2 == '\0') {
    QVar1 = this[(int)(char)this[0x800]];
    if (this[0x800] == this[0x801]) {
      this[0x800] = (Queue)0xff;
      this[0x801] = (Queue)0xff;
    }
    else {
      this[0x800] = (Queue)((char)this[0x800] + '\x01');
    }
    uVar4 = (ulong)(byte)QVar1;
  }
  else {
    pbVar3 = (basic_ostream *)
             std::basic_ostream<char,std::char_traits<char>>::operator__
                       ((basic_ostream_char_std__char_traits_char__ *)std::cout,
                        std::endl_char_std__char_traits_char__);
    pbVar3 = std::operator__(pbVar3,"Queue is empty");
    std::basic_ostream<char,std::char_traits<char>>::operator__
              ((basic_ostream_char_std__char_traits_char__ *)pbVar3,
               std::endl_char_std__char_traits_char__);
    uVar4 = 0xffffffff;
  }
  return uVar4;
}



// std::queue<unsigned char, std::deque<unsigned char, std::allocator<unsigned char>>>::~queue()

void __thiscall
std::queue<unsigned_char,std::deque<unsigned_char,std::allocator<unsigned_char>>>::_queue
          (queue_unsigned_char_std__deque_unsigned_char_std__allocator_unsigned_char___ *this)

{
  deque<unsigned_char,std::allocator<unsigned_char>>::_deque
            ((deque_unsigned_char_std__allocator_unsigned_char__ *)this);
  return;
}



// std::queue<unsigned char, std::deque<unsigned char, std::allocator<unsigned
// char>>>::TEMPNAMEPLACEHOLDERVALUE(std::queue<unsigned char, std::deque<unsigned char,
// std::allocator<unsigned char>>> const&)

queue_unsigned_char_std__deque_unsigned_char_std__allocator_unsigned_char___ * __thiscall
std::queue<unsigned_char,std::deque<unsigned_char,std::allocator<unsigned_char>>>::operator_
          (queue_unsigned_char_std__deque_unsigned_char_std__allocator_unsigned_char___ *this,
          queue *param_1)

{
  deque<unsigned_char,std::allocator<unsigned_char>>::operator_
            ((deque_unsigned_char_std__allocator_unsigned_char__ *)this,(deque *)param_1);
  return this;
}



// WARNING: Unknown calling convention yet parameter storage is locked
// std::_Setfill<char> std::setfill<char>(char)

_Setfill std::setfill_char_(char param_1)

{
  return (_Setfill)(uint)(byte)param_1;
}



// __gnu_cxx::__normal_iterator<char*, std::__cxx11::basic_string<char, std::char_traits<char>,
// std::allocator<char>>>::TEMPNAMEPLACEHOLDERVALUE(int)

undefined8 __thiscall
__gnu_cxx::
__normal_iterator<char*,std::__cxx11::basic_string<char,std::char_traits<char>,std::allocator<char>>>
::operator__(__normal_iterator_char__std____cxx11__basic_string_char_std__char_traits_char__std__allocator_char___
             *this,int param_1)

{
  long in_FS_OFFSET;
  char *local_20;
  undefined8 local_18;
  long local_10;
  
  local_10 = *(long *)(in_FS_OFFSET + 0x28);
  local_20 = *(char **)this;
  *(char **)this = local_20 + 1;
  __normal_iterator((__normal_iterator_char__std____cxx11__basic_string_char_std__char_traits_char__std__allocator_char___
                     *)&local_18,&local_20);
  if (local_10 != *(long *)(in_FS_OFFSET + 0x28)) {
                    // WARNING: Subroutine does not return
    __stack_chk_fail();
  }
  return local_18;
}



// __gnu_cxx::__normal_iterator<char*, std::__cxx11::basic_string<char, std::char_traits<char>,
// std::allocator<char>>>::TEMPNAMEPLACEHOLDERVALUE() const

undefined8 __thiscall
__gnu_cxx::
__normal_iterator<char*,std::__cxx11::basic_string<char,std::char_traits<char>,std::allocator<char>>>
::operator_(__normal_iterator_char__std____cxx11__basic_string_char_std__char_traits_char__std__allocator_char___
            *this)

{
  return *(undefined8 *)this;
}



// __gnu_cxx::__normal_iterator<char*, std::__cxx11::basic_string<char, std::char_traits<char>,
// std::allocator<char>>>::TEMPNAMEPLACEHOLDERVALUE(long) const

undefined8 __thiscall
__gnu_cxx::
__normal_iterator<char*,std::__cxx11::basic_string<char,std::char_traits<char>,std::allocator<char>>>
::operator_(__normal_iterator_char__std____cxx11__basic_string_char_std__char_traits_char__std__allocator_char___
            *this,long param_1)

{
  long in_FS_OFFSET;
  char *local_20;
  undefined8 local_18;
  long local_10;
  
  local_10 = *(long *)(in_FS_OFFSET + 0x28);
  local_20 = (char *)(*(long *)this - param_1);
  __normal_iterator((__normal_iterator_char__std____cxx11__basic_string_char_std__char_traits_char__std__allocator_char___
                     *)&local_18,&local_20);
  if (local_10 != *(long *)(in_FS_OFFSET + 0x28)) {
                    // WARNING: Subroutine does not return
    __stack_chk_fail();
  }
  return local_18;
}



// bool __gnu_cxx::TEMPNAMEPLACEHOLDERVALUE(__gnu_cxx::__normal_iterator<char*,
// std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char>>> const&,
// __gnu_cxx::__normal_iterator<char*, std::__cxx11::basic_string<char, std::char_traits<char>,
// std::allocator<char>>> const&)

bool __gnu_cxx::operator_(__normal_iterator *param_1,__normal_iterator *param_2)

{
  ulong uVar1;
  ulong *puVar2;
  
  puVar2 = (ulong *)__normal_iterator<char*,std::__cxx11::basic_string<char,std::char_traits<char>,std::allocator<char>>>
                    ::base((__normal_iterator_char__std____cxx11__basic_string_char_std__char_traits_char__std__allocator_char___
                            *)param_1);
  uVar1 = *puVar2;
  puVar2 = (ulong *)__normal_iterator<char*,std::__cxx11::basic_string<char,std::char_traits<char>,std::allocator<char>>>
                    ::base((__normal_iterator_char__std____cxx11__basic_string_char_std__char_traits_char__std__allocator_char___
                            *)param_2);
  return uVar1 < *puVar2;
}



// std::fpos<__mbstate_t>::operator long() const

long __thiscall std::fpos::operator_cast_to_long(fpos *this)

{
  return *(long *)this;
}



// std::queue<unsigned char, std::deque<unsigned char, std::allocator<unsigned
// char>>>::queue<std::deque<unsigned char, std::allocator<unsigned char>>, void>()

void __thiscall
std::queue<unsigned_char,std::deque<unsigned_char,std::allocator<unsigned_char>>>::
queue_std__deque_unsigned_char_std__allocator_unsigned_char___void_
          (queue_unsigned_char_std__deque_unsigned_char_std__allocator_unsigned_char___ *this)

{
  deque<unsigned_char,std::allocator<unsigned_char>>::deque
            ((deque_unsigned_char_std__allocator_unsigned_char__ *)this);
  return;
}



// std::deque<unsigned char, std::allocator<unsigned char>>::~deque()

void __thiscall
std::deque<unsigned_char,std::allocator<unsigned_char>>::_deque
          (deque_unsigned_char_std__allocator_unsigned_char__ *this)

{
  long in_FS_OFFSET;
  allocator local_48 [40];
  long local_20;
  
  local_20 = *(long *)(in_FS_OFFSET + 0x28);
  _Deque_base<unsigned_char,std::allocator<unsigned_char>>::_M_get_Tp_allocator
            ((_Deque_base_unsigned_char_std__allocator_unsigned_char__ *)this);
  end();
  begin();
  _M_destroy_data(SUB81(this,0),(_Deque_iterator)0x98,local_48);
  _Deque_base<unsigned_char,std::allocator<unsigned_char>>::__Deque_base
            ((_Deque_base_unsigned_char_std__allocator_unsigned_char__ *)this);
  if (local_20 != *(long *)(in_FS_OFFSET + 0x28)) {
                    // WARNING: Subroutine does not return
    __stack_chk_fail();
  }
  return;
}



// std::queue<unsigned char, std::deque<unsigned char, std::allocator<unsigned
// char>>>::push(unsigned char const&)

void __thiscall
std::queue<unsigned_char,std::deque<unsigned_char,std::allocator<unsigned_char>>>::push
          (queue_unsigned_char_std__deque_unsigned_char_std__allocator_unsigned_char___ *this,
          uchar *param_1)

{
  deque<unsigned_char,std::allocator<unsigned_char>>::push_back
            ((deque_unsigned_char_std__allocator_unsigned_char__ *)this,param_1);
  return;
}



// std::queue<unsigned char, std::deque<unsigned char, std::allocator<unsigned char>>>::size() const

void __thiscall
std::queue<unsigned_char,std::deque<unsigned_char,std::allocator<unsigned_char>>>::size
          (queue_unsigned_char_std__deque_unsigned_char_std__allocator_unsigned_char___ *this)

{
  deque<unsigned_char,std::allocator<unsigned_char>>::size
            ((deque_unsigned_char_std__allocator_unsigned_char__ *)this);
  return;
}



// std::queue<unsigned char, std::deque<unsigned char, std::allocator<unsigned char>>>::pop()

void __thiscall
std::queue<unsigned_char,std::deque<unsigned_char,std::allocator<unsigned_char>>>::pop
          (queue_unsigned_char_std__deque_unsigned_char_std__allocator_unsigned_char___ *this)

{
  deque<unsigned_char,std::allocator<unsigned_char>>::pop_front
            ((deque_unsigned_char_std__allocator_unsigned_char__ *)this);
  return;
}



// __gnu_cxx::__alloc_traits<std::allocator<unsigned char>, unsigned
// char>::_S_propagate_on_copy_assign()

undefined8
__gnu_cxx::__alloc_traits<std::allocator<unsigned_char>,unsigned_char>::_S_propagate_on_copy_assign
          (void)

{
  return 0;
}



// __gnu_cxx::__alloc_traits<std::allocator<unsigned char>, unsigned char>::_S_always_equal()

undefined8
__gnu_cxx::__alloc_traits<std::allocator<unsigned_char>,unsigned_char>::_S_always_equal(void)

{
  return 1;
}



// std::deque<unsigned char, std::allocator<unsigned
// char>>::TEMPNAMEPLACEHOLDERVALUE(std::deque<unsigned char, std::allocator<unsigned char>> const&)

deque_unsigned_char_std__allocator_unsigned_char__ * __thiscall
std::deque<unsigned_char,std::allocator<unsigned_char>>::operator_
          (deque_unsigned_char_std__allocator_unsigned_char__ *this,deque *param_1)

{
  bool bVar1;
  char cVar2;
  allocator *paVar3;
  allocator *paVar4;
  ulong uVar5;
  ulong uVar6;
  long in_FS_OFFSET;
  _Deque_iterator_unsigned_char_unsigned_char_const__unsigned_char_const__ local_c8 [64];
  _Deque_iterator_unsigned_char_unsigned_char_const__unsigned_char_const__ local_88 [32];
  _Deque_iterator_unsigned_char_unsigned_char__unsigned_char__ local_68 [32];
  deque_unsigned_char_std__allocator_unsigned_char__ local_48 [40];
  long local_20;
  
  local_20 = *(long *)(in_FS_OFFSET + 0x28);
  if (param_1 == (deque *)this) goto LAB_00105633;
  cVar2 = __gnu_cxx::__alloc_traits<std::allocator<unsigned_char>,unsigned_char>::
          _S_propagate_on_copy_assign();
  if (cVar2 != '\0') {
    cVar2 = __gnu_cxx::__alloc_traits<std::allocator<unsigned_char>,unsigned_char>::_S_always_equal
                      ();
    if (cVar2 == '\x01') {
LAB_001053d3:
      bVar1 = false;
    }
    else {
      paVar3 = (allocator *)
               _Deque_base<unsigned_char,std::allocator<unsigned_char>>::_M_get_Tp_allocator
                         ((_Deque_base_unsigned_char_std__allocator_unsigned_char__ *)param_1);
      paVar4 = (allocator *)
               _Deque_base<unsigned_char,std::allocator<unsigned_char>>::_M_get_Tp_allocator
                         ((_Deque_base_unsigned_char_std__allocator_unsigned_char__ *)this);
      cVar2 = operator__(paVar4,paVar3);
      if (cVar2 == '\0') goto LAB_001053d3;
      bVar1 = true;
    }
    if (bVar1) {
      get_allocator(local_48);
                    // try { // try from 0010540a to 0010540e has its CatchHandler @ 00105644
      _M_replace_map_std__deque_unsigned_char_std__allocator_unsigned_char__const__std__allocator_unsigned_char__
                (this,param_1,(allocator *)local_48);
      allocator<unsigned_char>::_allocator((allocator_unsigned_char_ *)local_48);
      paVar3 = (allocator *)
               _Deque_base<unsigned_char,std::allocator<unsigned_char>>::_M_get_Tp_allocator
                         ((_Deque_base_unsigned_char_std__allocator_unsigned_char__ *)param_1);
      paVar4 = (allocator *)
               _Deque_base<unsigned_char,std::allocator<unsigned_char>>::_M_get_Tp_allocator
                         ((_Deque_base_unsigned_char_std__allocator_unsigned_char__ *)this);
      __alloc_on_copy_std__allocator_unsigned_char__(paVar4,paVar3);
      goto LAB_00105633;
    }
    paVar3 = (allocator *)
             _Deque_base<unsigned_char,std::allocator<unsigned_char>>::_M_get_Tp_allocator
                       ((_Deque_base_unsigned_char_std__allocator_unsigned_char__ *)param_1);
    paVar4 = (allocator *)
             _Deque_base<unsigned_char,std::allocator<unsigned_char>>::_M_get_Tp_allocator
                       ((_Deque_base_unsigned_char_std__allocator_unsigned_char__ *)this);
    __alloc_on_copy_std__allocator_unsigned_char__(paVar4,paVar3);
  }
  uVar5 = size(this);
  uVar6 = size((deque_unsigned_char_std__allocator_unsigned_char__ *)param_1);
  if (uVar5 < uVar6) {
    begin();
    _Deque_iterator<unsigned_char,unsigned_char_const&,unsigned_char_const*>::operator_
              (local_c8,(long)local_48);
    _Deque_iterator<unsigned_char,unsigned_char&,unsigned_char*>::_Deque_iterator
              (local_68,(_Deque_iterator *)(this + 0x10));
    _Deque_iterator<unsigned_char,unsigned_char_const&,unsigned_char_const*>::_Deque_iterator
              (local_88,(_Deque_iterator *)local_c8);
    begin();
    copy_unsigned_char_((_Deque_iterator)0xb8,(_Deque_iterator)0x58,(_Deque_iterator)0x78);
    end();
    _Deque_iterator<unsigned_char,unsigned_char_const&,unsigned_char_const*>::_Deque_iterator
              ((_Deque_iterator_unsigned_char_unsigned_char_const__unsigned_char_const__ *)local_48,
               (_Deque_iterator *)local_c8);
    _Deque_iterator<unsigned_char,unsigned_char&,unsigned_char*>::_Deque_iterator
              (local_68,(_Deque_iterator *)(this + 0x30));
    _M_range_insert_aux_std___Deque_iterator_unsigned_char_unsigned_char_const__unsigned_char_const___
              (SUB81(this,0),(_Deque_iterator)0x98,(_Deque_iterator)0xb8,
               (int)register0x00000020 - 0x88);
  }
  else {
    _Deque_iterator<unsigned_char,unsigned_char&,unsigned_char*>::_Deque_iterator
              (local_68,(_Deque_iterator *)(this + 0x10));
    end();
    begin();
                    // try { // try from 00105511 to 0010552b has its CatchHandler @ 00105662
    copy_unsigned_char_((_Deque_iterator)0xb8,(_Deque_iterator)0x58,(_Deque_iterator)0x78);
    _M_erase_at_end(this,(_Deque_iterator)0xb8);
  }
LAB_00105633:
  if (local_20 == *(long *)(in_FS_OFFSET + 0x28)) {
    return this;
  }
                    // WARNING: Subroutine does not return
  __stack_chk_fail();
}



// std::queue<unsigned char, std::deque<unsigned char, std::allocator<unsigned char>>>::empty()
// const

void __thiscall
std::queue<unsigned_char,std::deque<unsigned_char,std::allocator<unsigned_char>>>::empty
          (queue_unsigned_char_std__deque_unsigned_char_std__allocator_unsigned_char___ *this)

{
  deque<unsigned_char,std::allocator<unsigned_char>>::empty
            ((deque_unsigned_char_std__allocator_unsigned_char__ *)this);
  return;
}



// std::queue<unsigned char, std::deque<unsigned char, std::allocator<unsigned char>>>::front()

void std::queue<unsigned_char,std::deque<unsigned_char,std::allocator<unsigned_char>>>::front(void)

{
  deque<unsigned_char,std::allocator<unsigned_char>>::front();
  return;
}



// __gnu_cxx::__normal_iterator<char*, std::__cxx11::basic_string<char, std::char_traits<char>,
// std::allocator<char>>>::__normal_iterator(char* const&)

void __thiscall
__gnu_cxx::
__normal_iterator<char*,std::__cxx11::basic_string<char,std::char_traits<char>,std::allocator<char>>>
::__normal_iterator(__normal_iterator_char__std____cxx11__basic_string_char_std__char_traits_char__std__allocator_char___
                    *this,char **param_1)

{
  *(char **)this = *param_1;
  return;
}



// __gnu_cxx::__normal_iterator<char*, std::__cxx11::basic_string<char, std::char_traits<char>,
// std::allocator<char>>>::base() const

__normal_iterator_char__std____cxx11__basic_string_char_std__char_traits_char__std__allocator_char___
* __thiscall
__gnu_cxx::
__normal_iterator<char*,std::__cxx11::basic_string<char,std::char_traits<char>,std::allocator<char>>>
::base(__normal_iterator_char__std____cxx11__basic_string_char_std__char_traits_char__std__allocator_char___
       *this)

{
  return this;
}



// std::deque<unsigned char, std::allocator<unsigned char>>::deque()

void __thiscall
std::deque<unsigned_char,std::allocator<unsigned_char>>::deque
          (deque_unsigned_char_std__allocator_unsigned_char__ *this)

{
  _Deque_base<unsigned_char,std::allocator<unsigned_char>>::_Deque_base
            ((_Deque_base_unsigned_char_std__allocator_unsigned_char__ *)this);
  return;
}



// std::_Deque_base<unsigned char, std::allocator<unsigned char>>::_Deque_impl::~_Deque_impl()

void __thiscall
std::_Deque_base<unsigned_char,std::allocator<unsigned_char>>::_Deque_impl::__Deque_impl
          (_Deque_impl *this)

{
  allocator<unsigned_char>::_allocator((allocator_unsigned_char_ *)this);
  return;
}



// std::_Deque_base<unsigned char, std::allocator<unsigned char>>::~_Deque_base()

void __thiscall
std::_Deque_base<unsigned_char,std::allocator<unsigned_char>>::__Deque_base
          (_Deque_base_unsigned_char_std__allocator_unsigned_char__ *this)

{
  if (*(long *)this != 0) {
    _M_destroy_nodes(this,*(uchar ***)(this + 0x28),(uchar **)(*(long *)(this + 0x48) + 8));
    _M_deallocate_map(this,*(uchar ***)this,*(ulong *)(this + 8));
  }
  _Deque_impl::__Deque_impl((_Deque_impl *)this);
  return;
}



// std::deque<unsigned char, std::allocator<unsigned char>>::begin()

_Deque_iterator_unsigned_char_unsigned_char__unsigned_char__ *
std::deque<unsigned_char,std::allocator<unsigned_char>>::begin(void)

{
  long in_RSI;
  _Deque_iterator_unsigned_char_unsigned_char__unsigned_char__ *in_RDI;
  
  _Deque_iterator<unsigned_char,unsigned_char&,unsigned_char*>::_Deque_iterator
            (in_RDI,(_Deque_iterator *)(in_RSI + 0x10));
  return in_RDI;
}



// std::deque<unsigned char, std::allocator<unsigned char>>::end()

_Deque_iterator_unsigned_char_unsigned_char__unsigned_char__ *
std::deque<unsigned_char,std::allocator<unsigned_char>>::end(void)

{
  long in_RSI;
  _Deque_iterator_unsigned_char_unsigned_char__unsigned_char__ *in_RDI;
  
  _Deque_iterator<unsigned_char,unsigned_char&,unsigned_char*>::_Deque_iterator
            (in_RDI,(_Deque_iterator *)(in_RSI + 0x30));
  return in_RDI;
}



// std::_Deque_base<unsigned char, std::allocator<unsigned char>>::_M_get_Tp_allocator()

_Deque_base_unsigned_char_std__allocator_unsigned_char__ * __thiscall
std::_Deque_base<unsigned_char,std::allocator<unsigned_char>>::_M_get_Tp_allocator
          (_Deque_base_unsigned_char_std__allocator_unsigned_char__ *this)

{
  return this;
}



// std::_Deque_iterator<unsigned char, unsigned char&, unsigned
// char*>::_Deque_iterator(std::_Deque_iterator<unsigned char, unsigned char&, unsigned char*>
// const&)

void __thiscall
std::_Deque_iterator<unsigned_char,unsigned_char&,unsigned_char*>::_Deque_iterator
          (_Deque_iterator_unsigned_char_unsigned_char__unsigned_char__ *this,
          _Deque_iterator *param_1)

{
  *(undefined8 *)this = *(undefined8 *)param_1;
  *(undefined8 *)(this + 8) = *(undefined8 *)(param_1 + 8);
  *(undefined8 *)(this + 0x10) = *(undefined8 *)(param_1 + 0x10);
  *(undefined8 *)(this + 0x18) = *(undefined8 *)(param_1 + 0x18);
  return;
}



// std::deque<unsigned char, std::allocator<unsigned
// char>>::_M_destroy_data(std::_Deque_iterator<unsigned char, unsigned char&, unsigned char*>,
// std::_Deque_iterator<unsigned char, unsigned char&, unsigned char*>, std::allocator<unsigned
// char> const&)

void std::deque<unsigned_char,std::allocator<unsigned_char>>::_M_destroy_data
               (_Deque_iterator param_1,_Deque_iterator param_2,allocator *param_3)

{
  long in_FS_OFFSET;
  
  if (*(long *)(in_FS_OFFSET + 0x28) != *(long *)(in_FS_OFFSET + 0x28)) {
                    // WARNING: Subroutine does not return
    __stack_chk_fail();
  }
  return;
}



// std::deque<unsigned char, std::allocator<unsigned char>>::push_back(unsigned char const&)

void __thiscall
std::deque<unsigned_char,std::allocator<unsigned_char>>::push_back
          (deque_unsigned_char_std__allocator_unsigned_char__ *this,uchar *param_1)

{
  if (*(long *)(this + 0x30) == *(long *)(this + 0x40) + -1) {
    _M_push_back_aux_unsigned_char_const__(this,param_1);
  }
  else {
    allocator_traits<std::allocator<unsigned_char>>::construct_unsigned_char_unsigned_char_const__
              ((allocator *)this,*(uchar **)(this + 0x30),param_1);
    *(long *)(this + 0x30) = *(long *)(this + 0x30) + 1;
  }
  return;
}



// std::deque<unsigned char, std::allocator<unsigned char>>::size() const

void __thiscall
std::deque<unsigned_char,std::allocator<unsigned_char>>::size
          (deque_unsigned_char_std__allocator_unsigned_char__ *this)

{
  operator_((_Deque_iterator *)(this + 0x30),(_Deque_iterator *)(this + 0x10));
  return;
}



// std::deque<unsigned char, std::allocator<unsigned char>>::pop_front()

void __thiscall
std::deque<unsigned_char,std::allocator<unsigned_char>>::pop_front
          (deque_unsigned_char_std__allocator_unsigned_char__ *this)

{
  if (*(long *)(this + 0x10) == *(long *)(this + 0x20) + -1) {
    _M_pop_front_aux(this);
  }
  else {
    allocator_traits<std::allocator<unsigned_char>>::destroy_unsigned_char_
              ((allocator *)this,*(uchar **)(this + 0x10));
    *(long *)(this + 0x10) = *(long *)(this + 0x10) + 1;
  }
  return;
}



// std::_Deque_base<unsigned char, std::allocator<unsigned char>>::_M_get_Tp_allocator() const

_Deque_base_unsigned_char_std__allocator_unsigned_char__ * __thiscall
std::_Deque_base<unsigned_char,std::allocator<unsigned_char>>::_M_get_Tp_allocator
          (_Deque_base_unsigned_char_std__allocator_unsigned_char__ *this)

{
  return this;
}



// WARNING: Unknown calling convention yet parameter storage is locked
// std::TEMPNAMEPLACEHOLDERVALUE(std::allocator<unsigned char> const&, std::allocator<unsigned char>
// const&)

undefined8 std::operator__(allocator *param_1,allocator *param_2)

{
  return 0;
}



// std::deque<unsigned char, std::allocator<unsigned char>>::get_allocator() const

deque_unsigned_char_std__allocator_unsigned_char__ * __thiscall
std::deque<unsigned_char,std::allocator<unsigned_char>>::get_allocator
          (deque_unsigned_char_std__allocator_unsigned_char__ *this)

{
  long lVar1;
  long in_FS_OFFSET;
  
  lVar1 = *(long *)(in_FS_OFFSET + 0x28);
  _Deque_base<unsigned_char,std::allocator<unsigned_char>>::get_allocator();
  if (lVar1 != *(long *)(in_FS_OFFSET + 0x28)) {
                    // WARNING: Subroutine does not return
    __stack_chk_fail();
  }
  return this;
}



// std::allocator<unsigned char>::~allocator()

void __thiscall std::allocator<unsigned_char>::_allocator(allocator_unsigned_char_ *this)

{
  __gnu_cxx::new_allocator<unsigned_char>::_new_allocator((new_allocator_unsigned_char_ *)this);
  return;
}



// void std::deque<unsigned char, std::allocator<unsigned char>>::_M_replace_map<std::deque<unsigned
// char, std::allocator<unsigned char>> const&, std::allocator<unsigned char>>(std::deque<unsigned
// char, std::allocator<unsigned char>> const&, std::allocator<unsigned char>&&)

void __thiscall
std::deque<unsigned_char,std::allocator<unsigned_char>>::
_M_replace_map_std__deque_unsigned_char_std__allocator_unsigned_char__const__std__allocator_unsigned_char__
          (deque_unsigned_char_std__allocator_unsigned_char__ *this,deque *param_1,
          allocator *param_2)

{
  allocator *paVar1;
  deque *pdVar2;
  long in_FS_OFFSET;
  uchar **local_80;
  deque_unsigned_char_std__allocator_unsigned_char__ local_78 [88];
  long local_20;
  
  local_20 = *(long *)(in_FS_OFFSET + 0x28);
  paVar1 = forward_std__allocator_unsigned_char__((type *)param_2);
  pdVar2 = forward_std__deque_unsigned_char_std__allocator_unsigned_char__const__((type *)param_1);
  deque(local_78,pdVar2,paVar1);
  clear(this);
  begin();
  _Deque_base<unsigned_char,std::allocator<unsigned_char>>::_M_deallocate_node
            ((_Deque_base_unsigned_char_std__allocator_unsigned_char__ *)this,*local_80);
  _Deque_base<unsigned_char,std::allocator<unsigned_char>>::_M_deallocate_map
            ((_Deque_base_unsigned_char_std__allocator_unsigned_char__ *)this,*(uchar ***)this,
             *(ulong *)(this + 8));
  *(undefined8 *)this = 0;
  *(undefined8 *)(this + 8) = 0;
  _Deque_base<unsigned_char,std::allocator<unsigned_char>>::_Deque_impl::_M_swap_data
            ((_Deque_impl *)this,(_Deque_impl *)local_78);
  _deque(local_78);
  if (local_20 != *(long *)(in_FS_OFFSET + 0x28)) {
                    // WARNING: Subroutine does not return
    __stack_chk_fail();
  }
  return;
}



// WARNING: Unknown calling convention yet parameter storage is locked
// void std::__alloc_on_copy<std::allocator<unsigned char>>(std::allocator<unsigned char>&,
// std::allocator<unsigned char> const&)

void std::__alloc_on_copy_std__allocator_unsigned_char__(allocator *param_1,allocator *param_2)

{
  long lVar1;
  long in_FS_OFFSET;
  
  lVar1 = *(long *)(in_FS_OFFSET + 0x28);
  __do_alloc_on_copy_std__allocator_unsigned_char__(param_1,param_2,(integral_constant)param_2);
  if (lVar1 != *(long *)(in_FS_OFFSET + 0x28)) {
                    // WARNING: Subroutine does not return
    __stack_chk_fail();
  }
  return;
}



// std::deque<unsigned char, std::allocator<unsigned
// char>>::_M_erase_at_end(std::_Deque_iterator<unsigned char, unsigned char&, unsigned char*>)

void __thiscall
std::deque<unsigned_char,std::allocator<unsigned_char>>::_M_erase_at_end
          (deque_unsigned_char_std__allocator_unsigned_char__ *this,_Deque_iterator param_1)

{
  undefined8 uVar1;
  undefined7 in_register_00000031;
  undefined8 *puVar2;
  long in_FS_OFFSET;
  allocator local_68 [32];
  _Deque_iterator_unsigned_char_unsigned_char__unsigned_char__ local_48 [40];
  long local_20;
  
  puVar2 = (undefined8 *)CONCAT71(in_register_00000031,param_1);
  local_20 = *(long *)(in_FS_OFFSET + 0x28);
  _Deque_base<unsigned_char,std::allocator<unsigned_char>>::_M_get_Tp_allocator
            ((_Deque_base_unsigned_char_std__allocator_unsigned_char__ *)this);
  end();
  _Deque_iterator<unsigned_char,unsigned_char&,unsigned_char*>::_Deque_iterator
            (local_48,(_Deque_iterator *)puVar2);
  _M_destroy_data(SUB81(this,0),(_Deque_iterator)0xb8,local_68);
  _Deque_base<unsigned_char,std::allocator<unsigned_char>>::_M_destroy_nodes
            ((_Deque_base_unsigned_char_std__allocator_unsigned_char__ *)this,
             (uchar **)(puVar2[3] + 8),(uchar **)(*(long *)(this + 0x48) + 8));
  uVar1 = puVar2[1];
  *(undefined8 *)(this + 0x30) = *puVar2;
  *(undefined8 *)(this + 0x38) = uVar1;
  uVar1 = puVar2[3];
  *(undefined8 *)(this + 0x40) = puVar2[2];
  *(undefined8 *)(this + 0x48) = uVar1;
  if (local_20 != *(long *)(in_FS_OFFSET + 0x28)) {
                    // WARNING: Subroutine does not return
    __stack_chk_fail();
  }
  return;
}



// std::deque<unsigned char, std::allocator<unsigned char>>::begin() const

_Deque_iterator_unsigned_char_unsigned_char_const__unsigned_char_const__ *
std::deque<unsigned_char,std::allocator<unsigned_char>>::begin(void)

{
  long in_RSI;
  _Deque_iterator_unsigned_char_unsigned_char_const__unsigned_char_const__ *in_RDI;
  
  _Deque_iterator<unsigned_char,unsigned_char_const&,unsigned_char_const*>::
  _Deque_iterator_std___Deque_iterator_unsigned_char_unsigned_char__unsigned_char___void_
            (in_RDI,(_Deque_iterator *)(in_RSI + 0x10));
  return in_RDI;
}



// std::deque<unsigned char, std::allocator<unsigned char>>::end() const

_Deque_iterator_unsigned_char_unsigned_char_const__unsigned_char_const__ *
std::deque<unsigned_char,std::allocator<unsigned_char>>::end(void)

{
  long in_RSI;
  _Deque_iterator_unsigned_char_unsigned_char_const__unsigned_char_const__ *in_RDI;
  
  _Deque_iterator<unsigned_char,unsigned_char_const&,unsigned_char_const*>::
  _Deque_iterator_std___Deque_iterator_unsigned_char_unsigned_char__unsigned_char___void_
            (in_RDI,(_Deque_iterator *)(in_RSI + 0x30));
  return in_RDI;
}



// std::_Deque_iterator<unsigned char, unsigned char const&, unsigned char
// const*>::_Deque_iterator(std::_Deque_iterator<unsigned char, unsigned char const&, unsigned char
// const*> const&)

void __thiscall
std::_Deque_iterator<unsigned_char,unsigned_char_const&,unsigned_char_const*>::_Deque_iterator
          (_Deque_iterator_unsigned_char_unsigned_char_const__unsigned_char_const__ *this,
          _Deque_iterator *param_1)

{
  *(undefined8 *)this = *(undefined8 *)param_1;
  *(undefined8 *)(this + 8) = *(undefined8 *)(param_1 + 8);
  *(undefined8 *)(this + 0x10) = *(undefined8 *)(param_1 + 0x10);
  *(undefined8 *)(this + 0x18) = *(undefined8 *)(param_1 + 0x18);
  return;
}



// WARNING: Unknown calling convention yet parameter storage is locked
// long const& std::min<long>(long const&, long const&)

long * std::min_long_(long *param_1,long *param_2)

{
  if (*param_2 < *param_1) {
    param_1 = param_2;
  }
  return param_1;
}



// WARNING: Unknown calling convention yet parameter storage is locked
// std::_Deque_iterator<unsigned char, unsigned char&, unsigned char*> std::copy<unsigned
// char>(std::_Deque_iterator<unsigned char, unsigned char const&, unsigned char const*>,
// std::_Deque_iterator<unsigned char, unsigned char const&, unsigned char const*>,
// std::_Deque_iterator<unsigned char, unsigned char&, unsigned char*>)

_Deque_iterator
std::copy_unsigned_char_(_Deque_iterator param_1,_Deque_iterator param_2,_Deque_iterator param_3)

{
  difference_type dVar1;
  undefined4 extraout_var;
  long *plVar2;
  uchar **in_RCX;
  undefined7 in_register_00000011;
  undefined7 in_register_00000031;
  uchar **this;
  undefined7 in_register_00000039;
  long in_FS_OFFSET;
  long local_30;
  uchar *local_28;
  uchar *local_20;
  long local_18;
  long local_10;
  
  this = (uchar **)CONCAT71(in_register_00000031,param_2);
  local_10 = *(long *)(in_FS_OFFSET + 0x28);
  dVar1 = operator_((_Deque_iterator *)CONCAT71(in_register_00000011,param_3),
                    (_Deque_iterator *)this);
  for (local_30 = CONCAT44(extraout_var,dVar1); 0 < local_30; local_30 = local_30 - local_18) {
    local_20 = in_RCX[2] + -(long)*in_RCX;
    local_28 = this[2] + -(long)*this;
    plVar2 = min_long_((long *)&local_28,(long *)&local_20);
    plVar2 = min_long_(&local_30,plVar2);
    local_18 = *plVar2;
    copy_unsigned_char__unsigned_char__(*this,*this + local_18,*in_RCX);
    _Deque_iterator<unsigned_char,unsigned_char_const&,unsigned_char_const*>::operator__
              ((_Deque_iterator_unsigned_char_unsigned_char_const__unsigned_char_const__ *)this,
               local_18);
    _Deque_iterator<unsigned_char,unsigned_char&,unsigned_char*>::operator__
              ((_Deque_iterator_unsigned_char_unsigned_char__unsigned_char__ *)in_RCX,local_18);
  }
  _Deque_iterator<unsigned_char,unsigned_char&,unsigned_char*>::_Deque_iterator
            ((_Deque_iterator_unsigned_char_unsigned_char__unsigned_char__ *)
             CONCAT71(in_register_00000039,param_1),(_Deque_iterator *)in_RCX);
  if (local_10 != *(long *)(in_FS_OFFSET + 0x28)) {
                    // WARNING: Subroutine does not return
    __stack_chk_fail();
  }
  return param_1;
}



// std::_Deque_iterator<unsigned char, unsigned char const&, unsigned char
// const*>::TEMPNAMEPLACEHOLDERVALUE(long) const

_Deque_iterator_unsigned_char_unsigned_char_const__unsigned_char_const__ * __thiscall
std::_Deque_iterator<unsigned_char,unsigned_char_const&,unsigned_char_const*>::operator_
          (_Deque_iterator_unsigned_char_unsigned_char_const__unsigned_char_const__ *this,
          long param_1)

{
  _Deque_iterator *p_Var1;
  long in_RDX;
  long in_FS_OFFSET;
  _Deque_iterator_unsigned_char_unsigned_char_const__unsigned_char_const__ local_38 [40];
  long local_10;
  
  local_10 = *(long *)(in_FS_OFFSET + 0x28);
  _Deque_iterator(local_38,(_Deque_iterator *)param_1);
  p_Var1 = (_Deque_iterator *)operator__(local_38,in_RDX);
  _Deque_iterator(this,p_Var1);
  if (local_10 != *(long *)(in_FS_OFFSET + 0x28)) {
                    // WARNING: Subroutine does not return
    __stack_chk_fail();
  }
  return this;
}



// void std::deque<unsigned char, std::allocator<unsigned
// char>>::_M_range_insert_aux<std::_Deque_iterator<unsigned char, unsigned char const&, unsigned
// char const*>>(std::_Deque_iterator<unsigned char, unsigned char&, unsigned char*>,
// std::_Deque_iterator<unsigned char, unsigned char const&, unsigned char const*>,
// std::_Deque_iterator<unsigned char, unsigned char const&, unsigned char const*>,
// std::forward_iterator_tag)

void std::deque<unsigned_char,std::allocator<unsigned_char>>::
     _M_range_insert_aux_std___Deque_iterator_unsigned_char_unsigned_char_const__unsigned_char_const___
               (_Deque_iterator param_1,_Deque_iterator param_2,_Deque_iterator param_3,
               forward_iterator_tag param_4)

{
  difference_type dVar1;
  undefined4 extraout_var;
  undefined4 in_register_0000000c;
  _Deque_iterator *p_Var2;
  undefined7 in_register_00000011;
  _Deque_iterator *p_Var3;
  undefined7 in_register_00000031;
  long *plVar4;
  undefined7 in_register_00000039;
  _Deque_base_unsigned_char_std__allocator_unsigned_char__ *this;
  long in_FS_OFFSET;
  undefined8 local_c8;
  undefined8 local_c0;
  undefined8 local_b8;
  undefined8 local_b0;
  _Deque_iterator_unsigned_char_unsigned_char_const__unsigned_char_const__ local_a8 [32];
  _Deque_iterator_unsigned_char_unsigned_char_const__unsigned_char_const__ local_88 [32];
  _Deque_iterator_unsigned_char_unsigned_char_const__unsigned_char_const__ local_68 [32];
  _Deque_iterator_unsigned_char_unsigned_char_const__unsigned_char_const__ local_48 [40];
  long local_20;
  
  this = (_Deque_base_unsigned_char_std__allocator_unsigned_char__ *)
         CONCAT71(in_register_00000039,param_1);
  plVar4 = (long *)CONCAT71(in_register_00000031,param_2);
  p_Var3 = (_Deque_iterator *)CONCAT71(in_register_00000011,param_3);
  p_Var2 = (_Deque_iterator *)CONCAT44(in_register_0000000c,param_4);
  local_20 = *(long *)(in_FS_OFFSET + 0x28);
  _Deque_iterator<unsigned_char,unsigned_char_const&,unsigned_char_const*>::_Deque_iterator
            (local_48,p_Var2);
  _Deque_iterator<unsigned_char,unsigned_char_const&,unsigned_char_const*>::_Deque_iterator
            (local_68,p_Var3);
  dVar1 = distance_std___Deque_iterator_unsigned_char_unsigned_char_const__unsigned_char_const___
                    ((_Deque_iterator)0x98,(_Deque_iterator)0xb8);
  if (*plVar4 == *(long *)(this + 0x10)) {
    _M_reserve_elements_at_front((ulong)&local_c8);
    _Deque_base<unsigned_char,std::allocator<unsigned_char>>::_M_get_Tp_allocator(this);
    _Deque_iterator<unsigned_char,unsigned_char&,unsigned_char*>::_Deque_iterator
              ((_Deque_iterator_unsigned_char_unsigned_char__unsigned_char__ *)local_68,
               (_Deque_iterator *)&local_c8);
    _Deque_iterator<unsigned_char,unsigned_char_const&,unsigned_char_const*>::_Deque_iterator
              (local_88,p_Var2);
    _Deque_iterator<unsigned_char,unsigned_char_const&,unsigned_char_const*>::_Deque_iterator
              (local_a8,p_Var3);
                    // try { // try from 00106063 to 00106067 has its CatchHandler @ 001061fb
    __uninitialized_copy_a_std___Deque_iterator_unsigned_char_unsigned_char_const__unsigned_char_const___std___Deque_iterator_unsigned_char_unsigned_char__unsigned_char___unsigned_char_
              ((_Deque_iterator)0xb8,(_Deque_iterator)0x58,(_Deque_iterator)0x78,
               (allocator *)local_68);
    *(undefined8 *)(this + 0x10) = local_c8;
    *(undefined8 *)(this + 0x18) = local_c0;
    *(undefined8 *)(this + 0x20) = local_b8;
    *(undefined8 *)(this + 0x28) = local_b0;
  }
  else {
    if (*plVar4 == *(long *)(this + 0x30)) {
      _M_reserve_elements_at_back((ulong)&local_c8);
      _Deque_base<unsigned_char,std::allocator<unsigned_char>>::_M_get_Tp_allocator(this);
      _Deque_iterator<unsigned_char,unsigned_char&,unsigned_char*>::_Deque_iterator
                ((_Deque_iterator_unsigned_char_unsigned_char__unsigned_char__ *)local_68,
                 (_Deque_iterator *)(this + 0x30));
      _Deque_iterator<unsigned_char,unsigned_char_const&,unsigned_char_const*>::_Deque_iterator
                (local_88,p_Var2);
      _Deque_iterator<unsigned_char,unsigned_char_const&,unsigned_char_const*>::_Deque_iterator
                (local_a8,p_Var3);
                    // try { // try from 00106152 to 00106156 has its CatchHandler @ 00106247
      __uninitialized_copy_a_std___Deque_iterator_unsigned_char_unsigned_char_const__unsigned_char_const___std___Deque_iterator_unsigned_char_unsigned_char__unsigned_char___unsigned_char_
                ((_Deque_iterator)0xb8,(_Deque_iterator)0x58,(_Deque_iterator)0x78,
                 (allocator *)local_68);
      *(undefined8 *)(this + 0x30) = local_c8;
      *(undefined8 *)(this + 0x38) = local_c0;
      *(undefined8 *)(this + 0x40) = local_b8;
      *(undefined8 *)(this + 0x48) = local_b0;
    }
    else {
      _Deque_iterator<unsigned_char,unsigned_char_const&,unsigned_char_const*>::_Deque_iterator
                (local_48,p_Var2);
      _Deque_iterator<unsigned_char,unsigned_char_const&,unsigned_char_const*>::_Deque_iterator
                (local_68,p_Var3);
      _Deque_iterator<unsigned_char,unsigned_char&,unsigned_char*>::_Deque_iterator
                ((_Deque_iterator_unsigned_char_unsigned_char__unsigned_char__ *)local_88,
                 (_Deque_iterator *)plVar4);
      _M_insert_aux_std___Deque_iterator_unsigned_char_unsigned_char_const__unsigned_char_const___
                ((deque_unsigned_char_std__allocator_unsigned_char__ *)this,(_Deque_iterator)0x78,
                 (_Deque_iterator)0x98,(_Deque_iterator)0xb8,CONCAT44(extraout_var,dVar1));
    }
  }
  if (local_20 != *(long *)(in_FS_OFFSET + 0x28)) {
                    // WARNING: Subroutine does not return
    __stack_chk_fail();
  }
  return;
}



// std::deque<unsigned char, std::allocator<unsigned char>>::empty() const

void __thiscall
std::deque<unsigned_char,std::allocator<unsigned_char>>::empty
          (deque_unsigned_char_std__allocator_unsigned_char__ *this)

{
  operator__((_Deque_iterator *)(this + 0x30),(_Deque_iterator *)(this + 0x10));
  return;
}



// std::deque<unsigned char, std::allocator<unsigned char>>::front()

void std::deque<unsigned_char,std::allocator<unsigned_char>>::front(void)

{
  long in_FS_OFFSET;
  _Deque_iterator_unsigned_char_unsigned_char__unsigned_char__ local_38 [40];
  long local_10;
  
  local_10 = *(long *)(in_FS_OFFSET + 0x28);
  begin();
  _Deque_iterator<unsigned_char,unsigned_char&,unsigned_char*>::operator_(local_38);
  if (local_10 != *(long *)(in_FS_OFFSET + 0x28)) {
                    // WARNING: Subroutine does not return
    __stack_chk_fail();
  }
  return;
}



// std::_Deque_base<unsigned char, std::allocator<unsigned char>>::_Deque_base()

void __thiscall
std::_Deque_base<unsigned_char,std::allocator<unsigned_char>>::_Deque_base
          (_Deque_base_unsigned_char_std__allocator_unsigned_char__ *this)

{
  _Deque_impl::_Deque_impl((_Deque_impl *)this);
                    // try { // try from 00106365 to 00106369 has its CatchHandler @ 0010636c
  _M_initialize_map(this,0);
  return;
}



// std::_Deque_base<unsigned char, std::allocator<unsigned char>>::_M_destroy_nodes(unsigned char**,
// unsigned char**)

void __thiscall
std::_Deque_base<unsigned_char,std::allocator<unsigned_char>>::_M_destroy_nodes
          (_Deque_base_unsigned_char_std__allocator_unsigned_char__ *this,uchar **param_1,
          uchar **param_2)

{
  uchar **local_10;
  
  for (local_10 = param_1; local_10 < param_2; local_10 = local_10 + 1) {
    _M_deallocate_node(this,*local_10);
  }
  return;
}



// std::_Deque_base<unsigned char, std::allocator<unsigned char>>::_M_deallocate_map(unsigned
// char**, unsigned long)

void __thiscall
std::_Deque_base<unsigned_char,std::allocator<unsigned_char>>::_M_deallocate_map
          (_Deque_base_unsigned_char_std__allocator_unsigned_char__ *this,uchar **param_1,
          ulong param_2)

{
  long in_FS_OFFSET;
  allocator local_11;
  long local_10;
  
  local_10 = *(long *)(in_FS_OFFSET + 0x28);
  _M_get_map_allocator();
  allocator_traits<std::allocator<unsigned_char*>>::deallocate(&local_11,param_1,param_2);
  allocator<unsigned_char*>::_allocator((allocator_unsigned_char__ *)&local_11);
  if (local_10 != *(long *)(in_FS_OFFSET + 0x28)) {
                    // WARNING: Subroutine does not return
    __stack_chk_fail();
  }
  return;
}



// void std::allocator_traits<std::allocator<unsigned char>>::construct<unsigned char, unsigned char
// const&>(std::allocator<unsigned char>&, unsigned char*, unsigned char const&)

void std::allocator_traits<std::allocator<unsigned_char>>::
     construct_unsigned_char_unsigned_char_const__(allocator *param_1,uchar *param_2,uchar *param_3)

{
  uchar *puVar1;
  
  puVar1 = forward_unsigned_char_const__((type *)param_3);
  __gnu_cxx::new_allocator<unsigned_char>::construct_unsigned_char_unsigned_char_const__
            ((new_allocator_unsigned_char_ *)param_1,param_2,puVar1);
  return;
}



// void std::deque<unsigned char, std::allocator<unsigned char>>::_M_push_back_aux<unsigned char
// const&>(unsigned char const&)

void __thiscall
std::deque<unsigned_char,std::allocator<unsigned_char>>::_M_push_back_aux_unsigned_char_const__
          (deque_unsigned_char_std__allocator_unsigned_char__ *this,uchar *param_1)

{
  long lVar1;
  long lVar2;
  undefined8 uVar3;
  uchar *puVar4;
  
  lVar1 = size(this);
  lVar2 = max_size(this);
  if (lVar1 == lVar2) {
    std::__throw_length_error("cannot create std::deque larger than max_size()");
  }
  _M_reserve_map_at_back(this,1);
  lVar1 = *(long *)(this + 0x48);
  uVar3 = _Deque_base<unsigned_char,std::allocator<unsigned_char>>::_M_allocate_node
                    ((_Deque_base_unsigned_char_std__allocator_unsigned_char__ *)this);
  *(undefined8 *)(lVar1 + 8) = uVar3;
  puVar4 = forward_unsigned_char_const__((type *)param_1);
  allocator_traits<std::allocator<unsigned_char>>::construct_unsigned_char_unsigned_char_const__
            ((allocator *)this,*(uchar **)(this + 0x30),puVar4);
  _Deque_iterator<unsigned_char,unsigned_char&,unsigned_char*>::_M_set_node
            ((_Deque_iterator_unsigned_char_unsigned_char__unsigned_char__ *)(this + 0x30),
             (uchar **)(*(long *)(this + 0x48) + 8));
  *(undefined8 *)(this + 0x30) = *(undefined8 *)(this + 0x38);
  return;
}



// WARNING: Unknown calling convention yet parameter storage is locked
// std::_Deque_iterator<unsigned char, unsigned char&, unsigned char*>::difference_type
// std::TEMPNAMEPLACEHOLDERVALUE(std::_Deque_iterator<unsigned char, unsigned char&, unsigned char*>
// const&, std::_Deque_iterator<unsigned char, unsigned char&, unsigned char*> const&)

difference_type std::operator_(_Deque_iterator *param_1,_Deque_iterator *param_2)

{
  int iVar1;
  
  iVar1 = _Deque_iterator<unsigned_char,unsigned_char&,unsigned_char*>::_S_buffer_size();
  return (difference_type)
         (((int)*(undefined8 *)(param_2 + 0x10) - (int)*(undefined8 *)param_2) +
         iVar1 * ((int)(*(long *)(param_1 + 0x18) - *(long *)(param_2 + 0x18) >> 3) + -1) +
         ((int)*(undefined8 *)param_1 - (int)*(undefined8 *)(param_1 + 8)));
}



// void std::allocator_traits<std::allocator<unsigned char>>::destroy<unsigned
// char>(std::allocator<unsigned char>&, unsigned char*)

void std::allocator_traits<std::allocator<unsigned_char>>::destroy_unsigned_char_
               (allocator *param_1,uchar *param_2)

{
  __gnu_cxx::new_allocator<unsigned_char>::destroy_unsigned_char_((uchar *)param_1);
  return;
}



// std::deque<unsigned char, std::allocator<unsigned char>>::_M_pop_front_aux()

void __thiscall
std::deque<unsigned_char,std::allocator<unsigned_char>>::_M_pop_front_aux
          (deque_unsigned_char_std__allocator_unsigned_char__ *this)

{
  uchar *puVar1;
  allocator *paVar2;
  
  puVar1 = *(uchar **)(this + 0x10);
  paVar2 = (allocator *)
           _Deque_base<unsigned_char,std::allocator<unsigned_char>>::_M_get_Tp_allocator
                     ((_Deque_base_unsigned_char_std__allocator_unsigned_char__ *)this);
  allocator_traits<std::allocator<unsigned_char>>::destroy_unsigned_char_(paVar2,puVar1);
  _Deque_base<unsigned_char,std::allocator<unsigned_char>>::_M_deallocate_node
            ((_Deque_base_unsigned_char_std__allocator_unsigned_char__ *)this,
             *(uchar **)(this + 0x18));
  _Deque_iterator<unsigned_char,unsigned_char&,unsigned_char*>::_M_set_node
            ((_Deque_iterator_unsigned_char_unsigned_char__unsigned_char__ *)(this + 0x10),
             (uchar **)(*(long *)(this + 0x28) + 8));
  *(undefined8 *)(this + 0x10) = *(undefined8 *)(this + 0x18);
  return;
}



// std::_Deque_base<unsigned char, std::allocator<unsigned char>>::get_allocator() const

allocator * std::_Deque_base<unsigned_char,std::allocator<unsigned_char>>::get_allocator(void)

{
  _Deque_base_unsigned_char_std__allocator_unsigned_char__ *in_RSI;
  allocator *in_RDI;
  
  _M_get_Tp_allocator(in_RSI);
  allocator<unsigned_char>::allocator(in_RDI);
  return in_RDI;
}



// std::allocator<unsigned char>::allocator(std::allocator<unsigned char> const&)

void std::allocator<unsigned_char>::allocator(allocator *param_1)

{
  __gnu_cxx::new_allocator<unsigned_char>::new_allocator((new_allocator *)param_1);
  return;
}



// __gnu_cxx::new_allocator<unsigned char>::~new_allocator()

void __thiscall
__gnu_cxx::new_allocator<unsigned_char>::_new_allocator(new_allocator_unsigned_char_ *this)

{
  return;
}



// WARNING: Unknown calling convention yet parameter storage is locked
// std::deque<unsigned char, std::allocator<unsigned char>> const& std::forward<std::deque<unsigned
// char, std::allocator<unsigned char>> const&>(std::remove_reference<std::deque<unsigned char,
// std::allocator<unsigned char>> const&>::type&)

deque * std::forward_std__deque_unsigned_char_std__allocator_unsigned_char__const__(type *param_1)

{
  return (deque *)param_1;
}



// WARNING: Unknown calling convention yet parameter storage is locked
// std::allocator<unsigned char>&& std::forward<std::allocator<unsigned
// char>>(std::remove_reference<std::allocator<unsigned char>>::type&)

allocator * std::forward_std__allocator_unsigned_char__(type *param_1)

{
  return (allocator *)param_1;
}



// std::deque<unsigned char, std::allocator<unsigned char>>::deque(std::deque<unsigned char,
// std::allocator<unsigned char>> const&, std::allocator<unsigned char> const&)

void __thiscall
std::deque<unsigned_char,std::allocator<unsigned_char>>::deque
          (deque_unsigned_char_std__allocator_unsigned_char__ *this,deque *param_1,
          allocator *param_2)

{
  ulong uVar1;
  long in_FS_OFFSET;
  _Deque_iterator_unsigned_char_unsigned_char__unsigned_char__ local_48 [40];
  long local_20;
  
  local_20 = *(long *)(in_FS_OFFSET + 0x28);
  uVar1 = size((deque_unsigned_char_std__allocator_unsigned_char__ *)param_1);
  _Deque_base<unsigned_char,std::allocator<unsigned_char>>::_Deque_base
            ((_Deque_base_unsigned_char_std__allocator_unsigned_char__ *)this,param_2,uVar1);
  _Deque_base<unsigned_char,std::allocator<unsigned_char>>::_M_get_Tp_allocator
            ((_Deque_base_unsigned_char_std__allocator_unsigned_char__ *)this);
  _Deque_iterator<unsigned_char,unsigned_char&,unsigned_char*>::_Deque_iterator
            (local_48,(_Deque_iterator *)(this + 0x10));
  end();
  begin();
                    // try { // try from 001067e0 to 001067e4 has its CatchHandler @ 001067e7
  __uninitialized_copy_a_std___Deque_iterator_unsigned_char_unsigned_char_const__unsigned_char_const___std___Deque_iterator_unsigned_char_unsigned_char__unsigned_char___unsigned_char_
            ((_Deque_iterator)0x58,(_Deque_iterator)0x78,(_Deque_iterator)0x98,(allocator *)local_48
            );
  if (local_20 != *(long *)(in_FS_OFFSET + 0x28)) {
                    // WARNING: Subroutine does not return
    __stack_chk_fail();
  }
  return;
}



// std::deque<unsigned char, std::allocator<unsigned char>>::clear()

void __thiscall
std::deque<unsigned_char,std::allocator<unsigned_char>>::clear
          (deque_unsigned_char_std__allocator_unsigned_char__ *this)

{
  long lVar1;
  long in_FS_OFFSET;
  
  lVar1 = *(long *)(in_FS_OFFSET + 0x28);
  begin();
  _M_erase_at_end(this,(_Deque_iterator)0xc8);
  if (lVar1 != *(long *)(in_FS_OFFSET + 0x28)) {
                    // WARNING: Subroutine does not return
    __stack_chk_fail();
  }
  return;
}



// std::_Deque_base<unsigned char, std::allocator<unsigned char>>::_M_deallocate_node(unsigned
// char*)

void __thiscall
std::_Deque_base<unsigned_char,std::allocator<unsigned_char>>::_M_deallocate_node
          (_Deque_base_unsigned_char_std__allocator_unsigned_char__ *this,uchar *param_1)

{
  ulong uVar1;
  
  uVar1 = __deque_buf_size(1);
  allocator_traits<std::allocator<unsigned_char>>::deallocate((allocator *)this,param_1,uVar1);
  return;
}



// std::_Deque_base<unsigned char, std::allocator<unsigned
// char>>::_Deque_impl::_M_swap_data(std::_Deque_base<unsigned char, std::allocator<unsigned
// char>>::_Deque_impl&)

void __thiscall
std::_Deque_base<unsigned_char,std::allocator<unsigned_char>>::_Deque_impl::_M_swap_data
          (_Deque_impl *this,_Deque_impl *param_1)

{
  swap_std___Deque_iterator_unsigned_char_unsigned_char__unsigned_char___
            ((_Deque_iterator *)(this + 0x10),(_Deque_iterator *)(param_1 + 0x10));
  swap_std___Deque_iterator_unsigned_char_unsigned_char__unsigned_char___
            ((_Deque_iterator *)(this + 0x30),(_Deque_iterator *)(param_1 + 0x30));
  swap_unsigned_char___((uchar ***)this,(uchar ***)param_1);
  swap_unsigned_long_((ulong *)(this + 8),(ulong *)(param_1 + 8));
  return;
}



// WARNING: Unknown calling convention yet parameter storage is locked
// void std::__do_alloc_on_copy<std::allocator<unsigned char>>(std::allocator<unsigned char>&,
// std::allocator<unsigned char> const&, std::integral_constant<bool, false>)

void std::__do_alloc_on_copy_std__allocator_unsigned_char__
               (allocator *param_1,allocator *param_2,integral_constant param_3)

{
  return;
}



// std::_Deque_iterator<unsigned char, unsigned char const&, unsigned char
// const*>::_Deque_iterator<std::_Deque_iterator<unsigned char, unsigned char&, unsigned char*>,
// void>(std::_Deque_iterator<unsigned char, unsigned char&, unsigned char*> const&)

void __thiscall
std::_Deque_iterator<unsigned_char,unsigned_char_const&,unsigned_char_const*>::
_Deque_iterator_std___Deque_iterator_unsigned_char_unsigned_char__unsigned_char___void_
          (_Deque_iterator_unsigned_char_unsigned_char_const__unsigned_char_const__ *this,
          _Deque_iterator *param_1)

{
  *(undefined8 *)this = *(undefined8 *)param_1;
  *(undefined8 *)(this + 8) = *(undefined8 *)(param_1 + 8);
  *(undefined8 *)(this + 0x10) = *(undefined8 *)(param_1 + 0x10);
  *(undefined8 *)(this + 0x18) = *(undefined8 *)(param_1 + 0x18);
  return;
}



// WARNING: Unknown calling convention yet parameter storage is locked
// std::_Deque_iterator<unsigned char, unsigned char const&, unsigned char const*>::difference_type
// std::TEMPNAMEPLACEHOLDERVALUE(std::_Deque_iterator<unsigned char, unsigned char const&, unsigned
// char const*> const&, std::_Deque_iterator<unsigned char, unsigned char const&, unsigned char
// const*> const&)

difference_type std::operator_(_Deque_iterator *param_1,_Deque_iterator *param_2)

{
  int iVar1;
  
  iVar1 = _Deque_iterator<unsigned_char,unsigned_char_const&,unsigned_char_const*>::_S_buffer_size()
  ;
  return (difference_type)
         (((int)*(undefined8 *)(param_2 + 0x10) - (int)*(undefined8 *)param_2) +
         iVar1 * ((int)(*(long *)(param_1 + 0x18) - *(long *)(param_2 + 0x18) >> 3) + -1) +
         ((int)*(undefined8 *)param_1 - (int)*(undefined8 *)(param_1 + 8)));
}



// WARNING: Unknown calling convention yet parameter storage is locked
// unsigned char* std::copy<unsigned char*, unsigned char*>(unsigned char*, unsigned char*, unsigned
// char*)

uchar * std::copy_unsigned_char__unsigned_char__(uchar *param_1,uchar *param_2,uchar *param_3)

{
  uchar *puVar1;
  uchar *puVar2;
  
  puVar1 = __miter_base_unsigned_char__(param_2);
  puVar2 = __miter_base_unsigned_char__(param_1);
  puVar1 = __copy_move_a2_false_unsigned_char__unsigned_char__(puVar2,puVar1,param_3);
  return puVar1;
}



// std::_Deque_iterator<unsigned char, unsigned char const&, unsigned char
// const*>::TEMPNAMEPLACEHOLDERVALUE(long)

_Deque_iterator_unsigned_char_unsigned_char_const__unsigned_char_const__ * __thiscall
std::_Deque_iterator<unsigned_char,unsigned_char_const&,unsigned_char_const*>::operator__
          (_Deque_iterator_unsigned_char_unsigned_char_const__unsigned_char_const__ *this,
          long param_1)

{
  bool bVar1;
  ulong uVar2;
  long lVar3;
  ulong uVar4;
  long lVar5;
  
  uVar2 = param_1 + (*(long *)this - *(long *)(this + 8));
  if (-1 < (long)uVar2) {
    lVar3 = _S_buffer_size();
    if ((long)uVar2 < lVar3) {
      bVar1 = true;
      goto LAB_00106aae;
    }
  }
  bVar1 = false;
LAB_00106aae:
  if (bVar1) {
    *(long *)this = *(long *)this + param_1;
  }
  else {
    if ((long)uVar2 < 1) {
      uVar4 = _S_buffer_size();
      uVar4 = ~(~uVar2 / uVar4);
    }
    else {
      lVar3 = _S_buffer_size();
      uVar4 = (long)uVar2 / lVar3;
    }
    _M_set_node(this,(uchar **)(uVar4 * 8 + *(long *)(this + 0x18)));
    lVar3 = *(long *)(this + 8);
    lVar5 = _S_buffer_size();
    *(ulong *)this = lVar3 + (uVar2 - lVar5 * uVar4);
  }
  return this;
}



// std::_Deque_iterator<unsigned char, unsigned char&, unsigned
// char*>::TEMPNAMEPLACEHOLDERVALUE(long)

_Deque_iterator_unsigned_char_unsigned_char__unsigned_char__ * __thiscall
std::_Deque_iterator<unsigned_char,unsigned_char&,unsigned_char*>::operator__
          (_Deque_iterator_unsigned_char_unsigned_char__unsigned_char__ *this,long param_1)

{
  bool bVar1;
  ulong uVar2;
  long lVar3;
  ulong uVar4;
  long lVar5;
  
  uVar2 = param_1 + (*(long *)this - *(long *)(this + 8));
  if (-1 < (long)uVar2) {
    lVar3 = _S_buffer_size();
    if ((long)uVar2 < lVar3) {
      bVar1 = true;
      goto LAB_00106bae;
    }
  }
  bVar1 = false;
LAB_00106bae:
  if (bVar1) {
    *(long *)this = *(long *)this + param_1;
  }
  else {
    if ((long)uVar2 < 1) {
      uVar4 = _S_buffer_size();
      uVar4 = ~(~uVar2 / uVar4);
    }
    else {
      lVar3 = _S_buffer_size();
      uVar4 = (long)uVar2 / lVar3;
    }
    _M_set_node(this,(uchar **)(uVar4 * 8 + *(long *)(this + 0x18)));
    lVar3 = *(long *)(this + 8);
    lVar5 = _S_buffer_size();
    *(ulong *)this = lVar3 + (uVar2 - lVar5 * uVar4);
  }
  return this;
}



// WARNING: Unknown calling convention yet parameter storage is locked
// std::iterator_traits<std::_Deque_iterator<unsigned char, unsigned char const&, unsigned char
// const*>>::difference_type std::distance<std::_Deque_iterator<unsigned char, unsigned char const&,
// unsigned char const*>>(std::_Deque_iterator<unsigned char, unsigned char const&, unsigned char
// const*>, std::_Deque_iterator<unsigned char, unsigned char const&, unsigned char const*>)

difference_type
std::distance_std___Deque_iterator_unsigned_char_unsigned_char_const__unsigned_char_const___
          (_Deque_iterator param_1,_Deque_iterator param_2)

{
  difference_type dVar1;
  undefined7 in_register_00000031;
  undefined7 in_register_00000039;
  long in_FS_OFFSET;
  _Deque_iterator_unsigned_char_unsigned_char_const__unsigned_char_const__ local_58 [32];
  _Deque_iterator_unsigned_char_unsigned_char_const__unsigned_char_const__ local_38 [40];
  long local_10;
  
  local_10 = *(long *)(in_FS_OFFSET + 0x28);
  __iterator_category_std___Deque_iterator_unsigned_char_unsigned_char_const__unsigned_char_const___
            ((_Deque_iterator *)CONCAT71(in_register_00000039,param_1));
  _Deque_iterator<unsigned_char,unsigned_char_const&,unsigned_char_const*>::_Deque_iterator
            (local_38,(_Deque_iterator *)CONCAT71(in_register_00000031,param_2));
  _Deque_iterator<unsigned_char,unsigned_char_const&,unsigned_char_const*>::_Deque_iterator
            (local_58,(_Deque_iterator *)CONCAT71(in_register_00000039,param_1));
  dVar1 = __distance_std___Deque_iterator_unsigned_char_unsigned_char_const__unsigned_char_const___
                    ((_Deque_iterator)0xa8,SUB81(local_38,0),(random_access_iterator_tag)local_38);
  if (local_10 != *(long *)(in_FS_OFFSET + 0x28)) {
                    // WARNING: Subroutine does not return
    __stack_chk_fail();
  }
  return dVar1;
}



// std::deque<unsigned char, std::allocator<unsigned char>>::_M_reserve_elements_at_front(unsigned
// long)

ulong std::deque<unsigned_char,std::allocator<unsigned_char>>::_M_reserve_elements_at_front
                (ulong param_1)

{
  long lVar1;
  ulong in_RDX;
  deque_unsigned_char_std__allocator_unsigned_char__ *in_RSI;
  long in_FS_OFFSET;
  
  lVar1 = *(long *)(in_FS_OFFSET + 0x28);
  if ((ulong)(*(long *)(in_RSI + 0x10) - *(long *)(in_RSI + 0x18)) < in_RDX) {
    _M_new_elements_at_front(in_RSI,in_RDX - (*(long *)(in_RSI + 0x10) - *(long *)(in_RSI + 0x18)));
  }
  _Deque_iterator<unsigned_char,unsigned_char&,unsigned_char*>::operator_
            ((_Deque_iterator_unsigned_char_unsigned_char__unsigned_char__ *)param_1,
             (long)(in_RSI + 0x10));
  if (lVar1 != *(long *)(in_FS_OFFSET + 0x28)) {
                    // WARNING: Subroutine does not return
    __stack_chk_fail();
  }
  return param_1;
}



// WARNING: Unknown calling convention yet parameter storage is locked
// std::_Deque_iterator<unsigned char, unsigned char&, unsigned char*>
// std::__uninitialized_copy_a<std::_Deque_iterator<unsigned char, unsigned char const&, unsigned
// char const*>, std::_Deque_iterator<unsigned char, unsigned char&, unsigned char*>, unsigned
// char>(std::_Deque_iterator<unsigned char, unsigned char const&, unsigned char const*>,
// std::_Deque_iterator<unsigned char, unsigned char const&, unsigned char const*>,
// std::_Deque_iterator<unsigned char, unsigned char&, unsigned char*>, std::allocator<unsigned
// char>&)

_Deque_iterator
std::
__uninitialized_copy_a_std___Deque_iterator_unsigned_char_unsigned_char_const__unsigned_char_const___std___Deque_iterator_unsigned_char_unsigned_char__unsigned_char___unsigned_char_
          (_Deque_iterator param_1,_Deque_iterator param_2,_Deque_iterator param_3,
          allocator *param_4)

{
  undefined7 in_register_00000011;
  undefined7 in_register_00000031;
  long in_FS_OFFSET;
  _Deque_iterator_unsigned_char_unsigned_char_const__unsigned_char_const__ local_78 [32];
  _Deque_iterator_unsigned_char_unsigned_char_const__unsigned_char_const__ local_58 [32];
  _Deque_iterator_unsigned_char_unsigned_char__unsigned_char__ local_38 [40];
  long local_10;
  
  local_10 = *(long *)(in_FS_OFFSET + 0x28);
  _Deque_iterator<unsigned_char,unsigned_char&,unsigned_char*>::_Deque_iterator
            (local_38,(_Deque_iterator *)param_4);
  _Deque_iterator<unsigned_char,unsigned_char_const&,unsigned_char_const*>::_Deque_iterator
            (local_58,(_Deque_iterator *)CONCAT71(in_register_00000011,param_3));
  _Deque_iterator<unsigned_char,unsigned_char_const&,unsigned_char_const*>::_Deque_iterator
            (local_78,(_Deque_iterator *)CONCAT71(in_register_00000031,param_2));
  uninitialized_copy_std___Deque_iterator_unsigned_char_unsigned_char_const__unsigned_char_const___std___Deque_iterator_unsigned_char_unsigned_char__unsigned_char___
            (param_1,(_Deque_iterator)0x88,(_Deque_iterator)0xa8);
  if (local_10 != *(long *)(in_FS_OFFSET + 0x28)) {
                    // WARNING: Subroutine does not return
    __stack_chk_fail();
  }
  return param_1;
}



// std::deque<unsigned char, std::allocator<unsigned char>>::_M_reserve_elements_at_back(unsigned
// long)

ulong std::deque<unsigned_char,std::allocator<unsigned_char>>::_M_reserve_elements_at_back
                (ulong param_1)

{
  long lVar1;
  ulong uVar2;
  ulong in_RDX;
  deque_unsigned_char_std__allocator_unsigned_char__ *in_RSI;
  long in_FS_OFFSET;
  
  lVar1 = *(long *)(in_FS_OFFSET + 0x28);
  uVar2 = (*(long *)(in_RSI + 0x40) - *(long *)(in_RSI + 0x30)) - 1;
  if (uVar2 < in_RDX) {
    _M_new_elements_at_back(in_RSI,in_RDX - uVar2);
  }
  _Deque_iterator<unsigned_char,unsigned_char&,unsigned_char*>::operator_
            ((_Deque_iterator_unsigned_char_unsigned_char__unsigned_char__ *)param_1,
             (long)(in_RSI + 0x30));
  if (lVar1 != *(long *)(in_FS_OFFSET + 0x28)) {
                    // WARNING: Subroutine does not return
    __stack_chk_fail();
  }
  return param_1;
}



// void std::deque<unsigned char, std::allocator<unsigned
// char>>::_M_insert_aux<std::_Deque_iterator<unsigned char, unsigned char const&, unsigned char
// const*>>(std::_Deque_iterator<unsigned char, unsigned char&, unsigned char*>,
// std::_Deque_iterator<unsigned char, unsigned char const&, unsigned char const*>,
// std::_Deque_iterator<unsigned char, unsigned char const&, unsigned char const*>, unsigned long)

void __thiscall
std::deque<unsigned_char,std::allocator<unsigned_char>>::
_M_insert_aux_std___Deque_iterator_unsigned_char_unsigned_char_const__unsigned_char_const___
          (deque_unsigned_char_std__allocator_unsigned_char__ *this,_Deque_iterator param_1,
          _Deque_iterator param_2,_Deque_iterator param_3,ulong param_4)

{
  difference_type dVar1;
  undefined4 extraout_var;
  ulong uVar3;
  undefined7 in_register_00000009;
  _Deque_iterator *p_Var4;
  undefined7 in_register_00000011;
  _Deque_iterator *p_Var5;
  undefined7 in_register_00000031;
  undefined8 *puVar6;
  long in_FS_OFFSET;
  undefined8 local_148;
  undefined8 local_140;
  undefined8 local_138;
  undefined8 local_130;
  _Deque_iterator_unsigned_char_unsigned_char__unsigned_char__ local_128 [32];
  _Deque_iterator_unsigned_char_unsigned_char_const__unsigned_char_const__ local_108 [32];
  _Deque_iterator_unsigned_char_unsigned_char__unsigned_char__ local_e8 [32];
  _Deque_iterator_unsigned_char_unsigned_char__unsigned_char__ local_c8 [32];
  _Deque_iterator_unsigned_char_unsigned_char__unsigned_char__ local_a8 [32];
  _Deque_iterator_unsigned_char_unsigned_char__unsigned_char__ local_88 [32];
  _Deque_iterator_unsigned_char_unsigned_char__unsigned_char__ local_68 [32];
  undefined8 local_48;
  undefined8 local_40;
  undefined8 local_38;
  undefined8 local_30;
  long local_20;
  ulong uVar2;
  
  puVar6 = (undefined8 *)CONCAT71(in_register_00000031,param_1);
  p_Var5 = (_Deque_iterator *)CONCAT71(in_register_00000011,param_2);
  p_Var4 = (_Deque_iterator *)CONCAT71(in_register_00000009,param_3);
  local_20 = *(long *)(in_FS_OFFSET + 0x28);
  dVar1 = operator_((_Deque_iterator *)puVar6,(_Deque_iterator *)(this + 0x10));
  uVar2 = CONCAT44(extraout_var,dVar1);
  uVar3 = size(this);
  if (uVar2 < uVar3 >> 1) {
    _M_reserve_elements_at_front((ulong)&local_148);
    _Deque_iterator<unsigned_char,unsigned_char&,unsigned_char*>::_Deque_iterator
              (local_128,(_Deque_iterator *)(this + 0x10));
    _Deque_iterator<unsigned_char,unsigned_char&,unsigned_char*>::operator_
              ((_Deque_iterator_unsigned_char_unsigned_char__unsigned_char__ *)&local_48,
               (long)(this + 0x10));
    *puVar6 = local_48;
    puVar6[1] = local_40;
    puVar6[2] = local_38;
    puVar6[3] = local_30;
    if ((long)uVar2 < (long)param_4) {
      _Deque_iterator<unsigned_char,unsigned_char_const&,unsigned_char_const*>::_Deque_iterator
                (local_108,p_Var5);
      advance_std___Deque_iterator_unsigned_char_unsigned_char_const__unsigned_char_const___long_
                ((_Deque_iterator *)local_108,param_4 - uVar2);
      _Deque_base<unsigned_char,std::allocator<unsigned_char>>::_M_get_Tp_allocator
                ((_Deque_base_unsigned_char_std__allocator_unsigned_char__ *)this);
      _Deque_iterator<unsigned_char,unsigned_char&,unsigned_char*>::_Deque_iterator
                ((_Deque_iterator_unsigned_char_unsigned_char__unsigned_char__ *)&local_48,
                 (_Deque_iterator *)&local_148);
      _Deque_iterator<unsigned_char,unsigned_char_const&,unsigned_char_const*>::_Deque_iterator
                ((_Deque_iterator_unsigned_char_unsigned_char_const__unsigned_char_const__ *)
                 local_88,(_Deque_iterator *)local_108);
      _Deque_iterator<unsigned_char,unsigned_char_const&,unsigned_char_const*>::_Deque_iterator
                ((_Deque_iterator_unsigned_char_unsigned_char_const__unsigned_char_const__ *)
                 local_a8,p_Var5);
      _Deque_iterator<unsigned_char,unsigned_char&,unsigned_char*>::_Deque_iterator
                (local_c8,(_Deque_iterator *)puVar6);
      _Deque_iterator<unsigned_char,unsigned_char&,unsigned_char*>::_Deque_iterator
                (local_e8,(_Deque_iterator *)(this + 0x10));
      __uninitialized_move_copy_std___Deque_iterator_unsigned_char_unsigned_char__unsigned_char___std___Deque_iterator_unsigned_char_unsigned_char_const__unsigned_char_const___std___Deque_iterator_unsigned_char_unsigned_char__unsigned_char___std__allocator_unsigned_char__
                ((_Deque_iterator)0x98,(_Deque_iterator)0x18,(_Deque_iterator)0x38,
                 (_Deque_iterator)0x58,(_Deque_iterator)0x78,(allocator *)&local_48);
      *(undefined8 *)(this + 0x10) = local_148;
      *(undefined8 *)(this + 0x18) = local_140;
      *(undefined8 *)(this + 0x20) = local_138;
      *(undefined8 *)(this + 0x28) = local_130;
      _Deque_iterator<unsigned_char,unsigned_char&,unsigned_char*>::_Deque_iterator
                (local_68,(_Deque_iterator *)local_128);
      _Deque_iterator<unsigned_char,unsigned_char_const&,unsigned_char_const*>::_Deque_iterator
                ((_Deque_iterator_unsigned_char_unsigned_char_const__unsigned_char_const__ *)
                 local_88,p_Var4);
      _Deque_iterator<unsigned_char,unsigned_char_const&,unsigned_char_const*>::_Deque_iterator
                ((_Deque_iterator_unsigned_char_unsigned_char_const__unsigned_char_const__ *)
                 local_a8,(_Deque_iterator *)local_108);
      copy_unsigned_char_((_Deque_iterator)0xb8,(_Deque_iterator)0x58,(_Deque_iterator)0x78);
    }
    else {
      _Deque_iterator<unsigned_char,unsigned_char&,unsigned_char*>::operator_
                (local_e8,(long)(this + 0x10));
      _Deque_base<unsigned_char,std::allocator<unsigned_char>>::_M_get_Tp_allocator
                ((_Deque_base_unsigned_char_std__allocator_unsigned_char__ *)this);
      _Deque_iterator<unsigned_char,unsigned_char&,unsigned_char*>::_Deque_iterator
                ((_Deque_iterator_unsigned_char_unsigned_char__unsigned_char__ *)&local_48,
                 (_Deque_iterator *)&local_148);
      _Deque_iterator<unsigned_char,unsigned_char&,unsigned_char*>::_Deque_iterator
                (local_68,(_Deque_iterator *)local_e8);
      _Deque_iterator<unsigned_char,unsigned_char&,unsigned_char*>::_Deque_iterator
                (local_88,(_Deque_iterator *)(this + 0x10));
                    // try { // try from 00107086 to 0010731a has its CatchHandler @ 001076f3
      __uninitialized_move_a_std___Deque_iterator_unsigned_char_unsigned_char__unsigned_char___std___Deque_iterator_unsigned_char_unsigned_char__unsigned_char___std__allocator_unsigned_char__
                ((_Deque_iterator)0x38,(_Deque_iterator)0x78,(_Deque_iterator)0x98,
                 (allocator *)&local_48);
      *(undefined8 *)(this + 0x10) = local_148;
      *(undefined8 *)(this + 0x18) = local_140;
      *(undefined8 *)(this + 0x20) = local_138;
      *(undefined8 *)(this + 0x28) = local_130;
      _Deque_iterator<unsigned_char,unsigned_char&,unsigned_char*>::_Deque_iterator
                ((_Deque_iterator_unsigned_char_unsigned_char__unsigned_char__ *)&local_48,
                 (_Deque_iterator *)local_128);
      _Deque_iterator<unsigned_char,unsigned_char&,unsigned_char*>::_Deque_iterator
                (local_68,(_Deque_iterator *)puVar6);
      _Deque_iterator<unsigned_char,unsigned_char&,unsigned_char*>::_Deque_iterator
                (local_88,(_Deque_iterator *)local_e8);
      move_unsigned_char_((_Deque_iterator)0x58,(_Deque_iterator)0x78,(_Deque_iterator)0x98);
      _Deque_iterator<unsigned_char,unsigned_char&,unsigned_char*>::operator_(local_a8,(long)puVar6)
      ;
      _Deque_iterator<unsigned_char,unsigned_char_const&,unsigned_char_const*>::_Deque_iterator
                ((_Deque_iterator_unsigned_char_unsigned_char_const__unsigned_char_const__ *)
                 &local_48,p_Var4);
      _Deque_iterator<unsigned_char,unsigned_char_const&,unsigned_char_const*>::_Deque_iterator
                ((_Deque_iterator_unsigned_char_unsigned_char_const__unsigned_char_const__ *)
                 local_68,p_Var5);
      copy_unsigned_char_((_Deque_iterator)0x78,(_Deque_iterator)0x98,(_Deque_iterator)0xb8);
    }
  }
  else {
    _M_reserve_elements_at_back((ulong)&local_148);
    _Deque_iterator<unsigned_char,unsigned_char&,unsigned_char*>::_Deque_iterator
              (local_128,(_Deque_iterator *)(this + 0x30));
    _Deque_iterator<unsigned_char,unsigned_char&,unsigned_char*>::operator_
              ((_Deque_iterator_unsigned_char_unsigned_char__unsigned_char__ *)&local_48,
               (long)(this + 0x30));
    *puVar6 = local_48;
    puVar6[1] = local_40;
    puVar6[2] = local_38;
    puVar6[3] = local_30;
    if ((long)param_4 < (long)(uVar3 - uVar2)) {
      _Deque_iterator<unsigned_char,unsigned_char&,unsigned_char*>::operator_
                (local_e8,(long)(this + 0x30));
      _Deque_base<unsigned_char,std::allocator<unsigned_char>>::_M_get_Tp_allocator
                ((_Deque_base_unsigned_char_std__allocator_unsigned_char__ *)this);
      _Deque_iterator<unsigned_char,unsigned_char&,unsigned_char*>::_Deque_iterator
                ((_Deque_iterator_unsigned_char_unsigned_char__unsigned_char__ *)&local_48,
                 (_Deque_iterator *)(this + 0x30));
      _Deque_iterator<unsigned_char,unsigned_char&,unsigned_char*>::_Deque_iterator
                (local_68,(_Deque_iterator *)(this + 0x30));
      _Deque_iterator<unsigned_char,unsigned_char&,unsigned_char*>::_Deque_iterator
                (local_88,(_Deque_iterator *)local_e8);
                    // try { // try from 00107466 to 001076ed has its CatchHandler @ 0010773f
      __uninitialized_move_a_std___Deque_iterator_unsigned_char_unsigned_char__unsigned_char___std___Deque_iterator_unsigned_char_unsigned_char__unsigned_char___std__allocator_unsigned_char__
                ((_Deque_iterator)0x38,(_Deque_iterator)0x78,(_Deque_iterator)0x98,
                 (allocator *)&local_48);
      *(undefined8 *)(this + 0x30) = local_148;
      *(undefined8 *)(this + 0x38) = local_140;
      *(undefined8 *)(this + 0x40) = local_138;
      *(undefined8 *)(this + 0x48) = local_130;
      _Deque_iterator<unsigned_char,unsigned_char&,unsigned_char*>::_Deque_iterator
                ((_Deque_iterator_unsigned_char_unsigned_char__unsigned_char__ *)&local_48,
                 (_Deque_iterator *)local_128);
      _Deque_iterator<unsigned_char,unsigned_char&,unsigned_char*>::_Deque_iterator
                (local_68,(_Deque_iterator *)local_e8);
      _Deque_iterator<unsigned_char,unsigned_char&,unsigned_char*>::_Deque_iterator
                (local_88,(_Deque_iterator *)puVar6);
      move_backward_unsigned_char_
                ((_Deque_iterator)0x58,(_Deque_iterator)0x78,(_Deque_iterator)0x98);
      _Deque_iterator<unsigned_char,unsigned_char&,unsigned_char*>::_Deque_iterator
                ((_Deque_iterator_unsigned_char_unsigned_char__unsigned_char__ *)&local_48,
                 (_Deque_iterator *)puVar6);
      _Deque_iterator<unsigned_char,unsigned_char_const&,unsigned_char_const*>::_Deque_iterator
                ((_Deque_iterator_unsigned_char_unsigned_char_const__unsigned_char_const__ *)
                 local_68,p_Var4);
      _Deque_iterator<unsigned_char,unsigned_char_const&,unsigned_char_const*>::_Deque_iterator
                ((_Deque_iterator_unsigned_char_unsigned_char_const__unsigned_char_const__ *)
                 local_a8,p_Var5);
      copy_unsigned_char_((_Deque_iterator)0x78,(_Deque_iterator)0x58,(_Deque_iterator)0x98);
    }
    else {
      _Deque_iterator<unsigned_char,unsigned_char_const&,unsigned_char_const*>::_Deque_iterator
                (local_108,p_Var5);
      advance_std___Deque_iterator_unsigned_char_unsigned_char_const__unsigned_char_const___long_
                ((_Deque_iterator *)local_108,uVar3 - uVar2);
      _Deque_base<unsigned_char,std::allocator<unsigned_char>>::_M_get_Tp_allocator
                ((_Deque_base_unsigned_char_std__allocator_unsigned_char__ *)this);
      _Deque_iterator<unsigned_char,unsigned_char&,unsigned_char*>::_Deque_iterator
                ((_Deque_iterator_unsigned_char_unsigned_char__unsigned_char__ *)&local_48,
                 (_Deque_iterator *)(this + 0x30));
      _Deque_iterator<unsigned_char,unsigned_char&,unsigned_char*>::_Deque_iterator
                (local_88,(_Deque_iterator *)(this + 0x30));
      _Deque_iterator<unsigned_char,unsigned_char&,unsigned_char*>::_Deque_iterator
                (local_a8,(_Deque_iterator *)puVar6);
      _Deque_iterator<unsigned_char,unsigned_char_const&,unsigned_char_const*>::_Deque_iterator
                ((_Deque_iterator_unsigned_char_unsigned_char_const__unsigned_char_const__ *)
                 local_c8,p_Var4);
      _Deque_iterator<unsigned_char,unsigned_char_const&,unsigned_char_const*>::_Deque_iterator
                ((_Deque_iterator_unsigned_char_unsigned_char_const__unsigned_char_const__ *)
                 local_e8,(_Deque_iterator *)local_108);
      __uninitialized_copy_move_std___Deque_iterator_unsigned_char_unsigned_char_const__unsigned_char_const___std___Deque_iterator_unsigned_char_unsigned_char__unsigned_char___std___Deque_iterator_unsigned_char_unsigned_char__unsigned_char___std__allocator_unsigned_char__
                ((_Deque_iterator)0x98,(_Deque_iterator)0x18,(_Deque_iterator)0x38,
                 (_Deque_iterator)0x58,(_Deque_iterator)0x78,(allocator *)&local_48);
      *(undefined8 *)(this + 0x30) = local_148;
      *(undefined8 *)(this + 0x38) = local_140;
      *(undefined8 *)(this + 0x40) = local_138;
      *(undefined8 *)(this + 0x48) = local_130;
      _Deque_iterator<unsigned_char,unsigned_char&,unsigned_char*>::_Deque_iterator
                (local_68,(_Deque_iterator *)puVar6);
      _Deque_iterator<unsigned_char,unsigned_char_const&,unsigned_char_const*>::_Deque_iterator
                ((_Deque_iterator_unsigned_char_unsigned_char_const__unsigned_char_const__ *)
                 local_88,(_Deque_iterator *)local_108);
      _Deque_iterator<unsigned_char,unsigned_char_const&,unsigned_char_const*>::_Deque_iterator
                ((_Deque_iterator_unsigned_char_unsigned_char_const__unsigned_char_const__ *)
                 local_a8,p_Var5);
      copy_unsigned_char_((_Deque_iterator)0xb8,(_Deque_iterator)0x58,(_Deque_iterator)0x78);
    }
  }
  if (local_20 != *(long *)(in_FS_OFFSET + 0x28)) {
                    // WARNING: Subroutine does not return
    __stack_chk_fail();
  }
  return;
}



// WARNING: Unknown calling convention yet parameter storage is locked
// bool std::TEMPNAMEPLACEHOLDERVALUE(std::_Deque_iterator<unsigned char, unsigned char&, unsigned
// char*> const&, std::_Deque_iterator<unsigned char, unsigned char&, unsigned char*> const&)

bool std::operator__(_Deque_iterator *param_1,_Deque_iterator *param_2)

{
  return *(long *)param_1 == *(long *)param_2;
}



// std::_Deque_iterator<unsigned char, unsigned char&, unsigned char*>::TEMPNAMEPLACEHOLDERVALUE()
// const

undefined8 __thiscall
std::_Deque_iterator<unsigned_char,unsigned_char&,unsigned_char*>::operator_
          (_Deque_iterator_unsigned_char_unsigned_char__unsigned_char__ *this)

{
  return *(undefined8 *)this;
}



// std::_Deque_base<unsigned char, std::allocator<unsigned char>>::_Deque_impl::_Deque_impl()

void __thiscall
std::_Deque_base<unsigned_char,std::allocator<unsigned_char>>::_Deque_impl::_Deque_impl
          (_Deque_impl *this)

{
  allocator<unsigned_char>::allocator();
  *(undefined8 *)this = 0;
  *(undefined8 *)(this + 8) = 0;
  _Deque_iterator<unsigned_char,unsigned_char&,unsigned_char*>::_Deque_iterator
            ((_Deque_iterator_unsigned_char_unsigned_char__unsigned_char__ *)(this + 0x10));
  _Deque_iterator<unsigned_char,unsigned_char&,unsigned_char*>::_Deque_iterator
            ((_Deque_iterator_unsigned_char_unsigned_char__unsigned_char__ *)(this + 0x30));
  return;
}



// std::_Deque_base<unsigned char, std::allocator<unsigned char>>::_M_initialize_map(unsigned long)

void __thiscall
std::_Deque_base<unsigned_char,std::allocator<unsigned_char>>::_M_initialize_map
          (_Deque_base_unsigned_char_std__allocator_unsigned_char__ *this,ulong param_1)

{
  long lVar1;
  ulong uVar2;
  ulong *puVar3;
  undefined8 uVar4;
  long in_FS_OFFSET;
  ulong local_48;
  ulong local_40;
  long local_38;
  uchar **local_30;
  uchar **local_28;
  long local_20;
  
  local_20 = *(long *)(in_FS_OFFSET + 0x28);
  uVar2 = __deque_buf_size(1);
  local_38 = param_1 / uVar2 + 1;
  local_40 = param_1 / uVar2 + 3;
  local_48 = 8;
  puVar3 = max_unsigned_long_(&local_48,&local_40);
  *(ulong *)(this + 8) = *puVar3;
  uVar4 = _M_allocate_map(this,*(ulong *)(this + 8));
  *(undefined8 *)this = uVar4;
  local_30 = (uchar **)(((ulong)(*(long *)(this + 8) - local_38) >> 1) * 8 + *(long *)this);
  local_28 = local_30 + local_38;
                    // try { // try from 0010791f to 00107923 has its CatchHandler @ 001079ab
  _M_create_nodes(this,local_30,local_28);
  _Deque_iterator<unsigned_char,unsigned_char&,unsigned_char*>::_M_set_node
            ((_Deque_iterator_unsigned_char_unsigned_char__unsigned_char__ *)(this + 0x10),local_30)
  ;
  _Deque_iterator<unsigned_char,unsigned_char&,unsigned_char*>::_M_set_node
            ((_Deque_iterator_unsigned_char_unsigned_char__unsigned_char__ *)(this + 0x30),
             local_28 + -1);
  *(undefined8 *)(this + 0x10) = *(undefined8 *)(this + 0x18);
  lVar1 = *(long *)(this + 0x38);
  uVar2 = __deque_buf_size(1);
  *(ulong *)(this + 0x30) = lVar1 + param_1 % uVar2;
  if (local_20 != *(long *)(in_FS_OFFSET + 0x28)) {
                    // WARNING: Subroutine does not return
    __stack_chk_fail();
  }
  return;
}



// std::_Deque_base<unsigned char, std::allocator<unsigned char>>::_M_get_map_allocator() const

allocator *
std::_Deque_base<unsigned_char,std::allocator<unsigned_char>>::_M_get_map_allocator(void)

{
  _Deque_base_unsigned_char_std__allocator_unsigned_char__ *in_RSI;
  allocator *in_RDI;
  
  _M_get_Tp_allocator(in_RSI);
  allocator<unsigned_char*>::allocator_unsigned_char_(in_RDI);
  return in_RDI;
}



// std::allocator<unsigned char*>::~allocator()

void __thiscall std::allocator<unsigned_char*>::_allocator(allocator_unsigned_char__ *this)

{
  __gnu_cxx::new_allocator<unsigned_char*>::_new_allocator((new_allocator_unsigned_char__ *)this);
  return;
}



// std::allocator_traits<std::allocator<unsigned char*>>::deallocate(std::allocator<unsigned
// char*>&, unsigned char**, unsigned long)

void std::allocator_traits<std::allocator<unsigned_char*>>::deallocate
               (allocator *param_1,uchar **param_2,ulong param_3)

{
  __gnu_cxx::new_allocator<unsigned_char*>::deallocate((uchar **)param_1,(ulong)param_2);
  return;
}



// std::deque<unsigned char, std::allocator<unsigned char>>::_S_buffer_size()

void std::deque<unsigned_char,std::allocator<unsigned_char>>::_S_buffer_size(void)

{
  __deque_buf_size(1);
  return;
}



// WARNING: Unknown calling convention yet parameter storage is locked
// unsigned char const& std::forward<unsigned char const&>(std::remove_reference<unsigned char
// const&>::type&)

uchar * std::forward_unsigned_char_const__(type *param_1)

{
  return (uchar *)param_1;
}



// void __gnu_cxx::new_allocator<unsigned char>::construct<unsigned char, unsigned char
// const&>(unsigned char*, unsigned char const&)

void __thiscall
__gnu_cxx::new_allocator<unsigned_char>::construct_unsigned_char_unsigned_char_const__
          (new_allocator_unsigned_char_ *this,uchar *param_1,uchar *param_2)

{
  uchar uVar1;
  uchar *puVar2;
  
  puVar2 = std::forward_unsigned_char_const__((type *)param_2);
  uVar1 = *puVar2;
  puVar2 = (uchar *)operator_new(1,param_1);
  *puVar2 = uVar1;
  return;
}



// std::deque<unsigned char, std::allocator<unsigned char>>::max_size() const

void __thiscall
std::deque<unsigned_char,std::allocator<unsigned_char>>::max_size
          (deque_unsigned_char_std__allocator_unsigned_char__ *this)

{
  allocator *paVar1;
  
  paVar1 = (allocator *)
           _Deque_base<unsigned_char,std::allocator<unsigned_char>>::_M_get_Tp_allocator
                     ((_Deque_base_unsigned_char_std__allocator_unsigned_char__ *)this);
  _S_max_size(paVar1);
  return;
}



// std::deque<unsigned char, std::allocator<unsigned char>>::_M_reserve_map_at_back(unsigned long)

void __thiscall
std::deque<unsigned_char,std::allocator<unsigned_char>>::_M_reserve_map_at_back
          (deque_unsigned_char_std__allocator_unsigned_char__ *this,ulong param_1)

{
  if ((ulong)(*(long *)(this + 8) - (*(long *)(this + 0x48) - *(long *)this >> 3)) < param_1 + 1) {
    _M_reallocate_map(this,param_1,false);
  }
  return;
}



// std::_Deque_base<unsigned char, std::allocator<unsigned char>>::_M_allocate_node()

void __thiscall
std::_Deque_base<unsigned_char,std::allocator<unsigned_char>>::_M_allocate_node
          (_Deque_base_unsigned_char_std__allocator_unsigned_char__ *this)

{
  ulong uVar1;
  
  uVar1 = __deque_buf_size(1);
  allocator_traits<std::allocator<unsigned_char>>::allocate((allocator *)this,uVar1);
  return;
}



// std::_Deque_iterator<unsigned char, unsigned char&, unsigned char*>::_M_set_node(unsigned char**)

void __thiscall
std::_Deque_iterator<unsigned_char,unsigned_char&,unsigned_char*>::_M_set_node
          (_Deque_iterator_unsigned_char_unsigned_char__unsigned_char__ *this,uchar **param_1)

{
  long lVar1;
  long lVar2;
  
  *(uchar ***)(this + 0x18) = param_1;
  *(uchar **)(this + 8) = *param_1;
  lVar1 = *(long *)(this + 8);
  lVar2 = _S_buffer_size();
  *(long *)(this + 0x10) = lVar1 + lVar2;
  return;
}



// std::_Deque_iterator<unsigned char, unsigned char&, unsigned char*>::_S_buffer_size()

void std::_Deque_iterator<unsigned_char,unsigned_char&,unsigned_char*>::_S_buffer_size(void)

{
  __deque_buf_size(1);
  return;
}



// void __gnu_cxx::new_allocator<unsigned char>::destroy<unsigned char>(unsigned char*)

void __gnu_cxx::new_allocator<unsigned_char>::destroy_unsigned_char_(uchar *param_1)

{
  return;
}



// __gnu_cxx::new_allocator<unsigned char>::new_allocator(__gnu_cxx::new_allocator<unsigned char>
// const&)

void __gnu_cxx::new_allocator<unsigned_char>::new_allocator(new_allocator *param_1)

{
  return;
}



// std::_Deque_base<unsigned char, std::allocator<unsigned
// char>>::_Deque_base(std::allocator<unsigned char> const&, unsigned long)

void __thiscall
std::_Deque_base<unsigned_char,std::allocator<unsigned_char>>::_Deque_base
          (_Deque_base_unsigned_char_std__allocator_unsigned_char__ *this,allocator *param_1,
          ulong param_2)

{
  _Deque_impl::_Deque_impl((_Deque_impl *)this,param_1);
                    // try { // try from 00107c88 to 00107c8c has its CatchHandler @ 00107c8f
  _M_initialize_map(this,param_2);
  return;
}



// std::allocator_traits<std::allocator<unsigned char>>::deallocate(std::allocator<unsigned char>&,
// unsigned char*, unsigned long)

void std::allocator_traits<std::allocator<unsigned_char>>::deallocate
               (allocator *param_1,uchar *param_2,ulong param_3)

{
  __gnu_cxx::new_allocator<unsigned_char>::deallocate((uchar *)param_1,(ulong)param_2);
  return;
}



// WARNING: Unknown calling convention yet parameter storage is locked
// std::enable_if<std::__and_<std::__not_<std::__is_tuple_like<std::_Deque_iterator<unsigned char,
// unsigned char&, unsigned char*>>>, std::is_move_constructible<std::_Deque_iterator<unsigned char,
// unsigned char&, unsigned char*>>, std::is_move_assignable<std::_Deque_iterator<unsigned char,
// unsigned char&, unsigned char*>>>::value, void>::type std::swap<std::_Deque_iterator<unsigned
// char, unsigned char&, unsigned char*>>(std::_Deque_iterator<unsigned char, unsigned char&,
// unsigned char*>&, std::_Deque_iterator<unsigned char, unsigned char&, unsigned char*>&)

type std::swap_std___Deque_iterator_unsigned_char_unsigned_char__unsigned_char___
               (_Deque_iterator *param_1,_Deque_iterator *param_2)

{
  undefined8 uVar1;
  type *ptVar2;
  undefined8 *puVar3;
  long in_FS_OFFSET;
  _Deque_iterator_unsigned_char_unsigned_char__unsigned_char__ local_38 [40];
  long local_10;
  
  local_10 = *(long *)(in_FS_OFFSET + 0x28);
  ptVar2 = move_std___Deque_iterator_unsigned_char_unsigned_char__unsigned_char____(param_1);
  _Deque_iterator<unsigned_char,unsigned_char&,unsigned_char*>::_Deque_iterator
            (local_38,(_Deque_iterator *)ptVar2);
  puVar3 = (undefined8 *)
           move_std___Deque_iterator_unsigned_char_unsigned_char__unsigned_char____(param_2);
  uVar1 = puVar3[1];
  *(undefined8 *)param_1 = *puVar3;
  *(undefined8 *)(param_1 + 8) = uVar1;
  uVar1 = puVar3[3];
  *(undefined8 *)(param_1 + 0x10) = puVar3[2];
  *(undefined8 *)(param_1 + 0x18) = uVar1;
  puVar3 = (undefined8 *)
           move_std___Deque_iterator_unsigned_char_unsigned_char__unsigned_char____
                     ((_Deque_iterator *)local_38);
  uVar1 = puVar3[1];
  *(undefined8 *)param_2 = *puVar3;
  *(undefined8 *)(param_2 + 8) = uVar1;
  uVar1 = puVar3[3];
  *(undefined8 *)(param_2 + 0x10) = puVar3[2];
  *(undefined8 *)(param_2 + 0x18) = uVar1;
  if (local_10 != *(long *)(in_FS_OFFSET + 0x28)) {
                    // WARNING: Subroutine does not return
    __stack_chk_fail();
  }
  return 0;
}



// WARNING: Unknown calling convention yet parameter storage is locked
// std::remove_reference<unsigned char**&>::type&& std::move<unsigned char**&>(unsigned char**&)

type * std::move_unsigned_char____(uchar ***param_1)

{
  return (type *)param_1;
}



// WARNING: Unknown calling convention yet parameter storage is locked
// std::enable_if<std::__and_<std::__not_<std::__is_tuple_like<unsigned char**>>,
// std::is_move_constructible<unsigned char**>, std::is_move_assignable<unsigned char**>>::value,
// void>::type std::swap<unsigned char**>(unsigned char**&, unsigned char**&)

type std::swap_unsigned_char___(uchar ***param_1,uchar ***param_2)

{
  uchar **ppuVar1;
  long in_FS_OFFSET;
  uchar **local_18;
  long local_10;
  
  local_10 = *(long *)(in_FS_OFFSET + 0x28);
  local_18 = (uchar **)move_unsigned_char____(param_1);
  local_18 = (uchar **)*local_18;
  ppuVar1 = (uchar **)move_unsigned_char____(param_2);
  *param_1 = (uchar **)*ppuVar1;
  ppuVar1 = (uchar **)move_unsigned_char____(&local_18);
  *param_2 = (uchar **)*ppuVar1;
  if (local_10 != *(long *)(in_FS_OFFSET + 0x28)) {
                    // WARNING: Subroutine does not return
    __stack_chk_fail();
  }
  return 0;
}



// WARNING: Unknown calling convention yet parameter storage is locked
// std::remove_reference<unsigned long&>::type&& std::move<unsigned long&>(unsigned long&)

type * std::move_unsigned_long__(ulong *param_1)

{
  return (type *)param_1;
}



// WARNING: Unknown calling convention yet parameter storage is locked
// std::enable_if<std::__and_<std::__not_<std::__is_tuple_like<unsigned long>>,
// std::is_move_constructible<unsigned long>, std::is_move_assignable<unsigned long>>::value,
// void>::type std::swap<unsigned long>(unsigned long&, unsigned long&)

type std::swap_unsigned_long_(ulong *param_1,ulong *param_2)

{
  ulong *puVar1;
  long in_FS_OFFSET;
  ulong local_18;
  long local_10;
  
  local_10 = *(long *)(in_FS_OFFSET + 0x28);
  puVar1 = (ulong *)move_unsigned_long__(param_1);
  local_18 = *puVar1;
  puVar1 = (ulong *)move_unsigned_long__(param_2);
  *param_1 = *puVar1;
  puVar1 = (ulong *)move_unsigned_long__(&local_18);
  *param_2 = *puVar1;
  if (local_10 != *(long *)(in_FS_OFFSET + 0x28)) {
                    // WARNING: Subroutine does not return
    __stack_chk_fail();
  }
  return 0;
}



// std::_Deque_iterator<unsigned char, unsigned char const&, unsigned char const*>::_S_buffer_size()

void std::_Deque_iterator<unsigned_char,unsigned_char_const&,unsigned_char_const*>::_S_buffer_size
               (void)

{
  __deque_buf_size(1);
  return;
}



// WARNING: Unknown calling convention yet parameter storage is locked
// unsigned char* std::__miter_base<unsigned char*>(unsigned char*)

uchar * std::__miter_base_unsigned_char__(uchar *param_1)

{
  return param_1;
}



// WARNING: Unknown calling convention yet parameter storage is locked
// unsigned char* std::__copy_move_a2<false, unsigned char*, unsigned char*>(unsigned char*,
// unsigned char*, unsigned char*)

uchar * std::__copy_move_a2_false_unsigned_char__unsigned_char__
                  (uchar *param_1,uchar *param_2,uchar *param_3)

{
  uchar *puVar1;
  uchar *puVar2;
  uchar *puVar3;
  uchar *local_30;
  uchar *local_28;
  uchar *local_20;
  
  local_30 = param_3;
  local_28 = param_2;
  local_20 = param_1;
  puVar1 = __niter_base_unsigned_char__(param_3);
  puVar2 = __niter_base_unsigned_char__(local_28);
  puVar3 = __niter_base_unsigned_char__(local_20);
  puVar1 = __copy_move_a_false_unsigned_char__unsigned_char__(puVar3,puVar2,puVar1);
  puVar1 = __niter_wrap_unsigned_char__(&local_30,puVar1);
  return puVar1;
}



// std::_Deque_iterator<unsigned char, unsigned char const&, unsigned char
// const*>::_M_set_node(unsigned char**)

void __thiscall
std::_Deque_iterator<unsigned_char,unsigned_char_const&,unsigned_char_const*>::_M_set_node
          (_Deque_iterator_unsigned_char_unsigned_char_const__unsigned_char_const__ *this,
          uchar **param_1)

{
  long lVar1;
  long lVar2;
  
  *(uchar ***)(this + 0x18) = param_1;
  *(uchar **)(this + 8) = *param_1;
  lVar1 = *(long *)(this + 8);
  lVar2 = _S_buffer_size();
  *(long *)(this + 0x10) = lVar1 + lVar2;
  return;
}



// WARNING: Unknown calling convention yet parameter storage is locked
// std::iterator_traits<std::_Deque_iterator<unsigned char, unsigned char const&, unsigned char
// const*>>::iterator_category std::__iterator_category<std::_Deque_iterator<unsigned char, unsigned
// char const&, unsigned char const*>>(std::_Deque_iterator<unsigned char, unsigned char const&,
// unsigned char const*> const&)

iterator_category
std::
__iterator_category_std___Deque_iterator_unsigned_char_unsigned_char_const__unsigned_char_const___
          (_Deque_iterator *param_1)

{
  iterator_category in_EAX;
  
  return in_EAX;
}



// WARNING: Unknown calling convention yet parameter storage is locked
// std::iterator_traits<std::_Deque_iterator<unsigned char, unsigned char const&, unsigned char
// const*>>::difference_type std::__distance<std::_Deque_iterator<unsigned char, unsigned char
// const&, unsigned char const*>>(std::_Deque_iterator<unsigned char, unsigned char const&, unsigned
// char const*>, std::_Deque_iterator<unsigned char, unsigned char const&, unsigned char const*>,
// std::random_access_iterator_tag)

difference_type
std::__distance_std___Deque_iterator_unsigned_char_unsigned_char_const__unsigned_char_const___
          (_Deque_iterator param_1,_Deque_iterator param_2,random_access_iterator_tag param_3)

{
  difference_type dVar1;
  undefined7 in_register_00000031;
  undefined7 in_register_00000039;
  
  dVar1 = operator_((_Deque_iterator *)CONCAT71(in_register_00000031,param_2),
                    (_Deque_iterator *)CONCAT71(in_register_00000039,param_1));
  return (difference_type)dVar1;
}



// std::deque<unsigned char, std::allocator<unsigned char>>::_M_new_elements_at_front(unsigned long)

void __thiscall
std::deque<unsigned_char,std::allocator<unsigned_char>>::_M_new_elements_at_front
          (deque_unsigned_char_std__allocator_unsigned_char__ *this,ulong param_1)

{
  long lVar1;
  long lVar2;
  ulong uVar3;
  undefined8 uVar4;
  ulong local_30;
  
  lVar1 = max_size(this);
  lVar2 = size(this);
  if ((ulong)(lVar1 - lVar2) < param_1) {
    std::__throw_length_error("deque::_M_new_elements_at_front");
  }
  lVar1 = _S_buffer_size();
  uVar3 = _S_buffer_size();
  uVar3 = ((lVar1 + param_1) - 1) / uVar3;
  _M_reserve_map_at_front(this,uVar3);
  for (local_30 = 1; local_30 <= uVar3; local_30 = local_30 + 1) {
    lVar1 = *(long *)(this + 0x28);
                    // try { // try from 0010808d to 00108091 has its CatchHandler @ 0010809c
    uVar4 = _Deque_base<unsigned_char,std::allocator<unsigned_char>>::_M_allocate_node
                      ((_Deque_base_unsigned_char_std__allocator_unsigned_char__ *)this);
    *(undefined8 *)(lVar1 + local_30 * -8) = uVar4;
  }
  return;
}



// std::_Deque_iterator<unsigned char, unsigned char&, unsigned
// char*>::TEMPNAMEPLACEHOLDERVALUE(long) const

_Deque_iterator_unsigned_char_unsigned_char__unsigned_char__ * __thiscall
std::_Deque_iterator<unsigned_char,unsigned_char&,unsigned_char*>::operator_
          (_Deque_iterator_unsigned_char_unsigned_char__unsigned_char__ *this,long param_1)

{
  _Deque_iterator *p_Var1;
  long in_RDX;
  long in_FS_OFFSET;
  _Deque_iterator_unsigned_char_unsigned_char__unsigned_char__ local_38 [40];
  long local_10;
  
  local_10 = *(long *)(in_FS_OFFSET + 0x28);
  _Deque_iterator(local_38,(_Deque_iterator *)param_1);
  p_Var1 = (_Deque_iterator *)operator__(local_38,in_RDX);
  _Deque_iterator(this,p_Var1);
  if (local_10 != *(long *)(in_FS_OFFSET + 0x28)) {
                    // WARNING: Subroutine does not return
    __stack_chk_fail();
  }
  return this;
}



// WARNING: Unknown calling convention yet parameter storage is locked
// std::_Deque_iterator<unsigned char, unsigned char&, unsigned char*>
// std::uninitialized_copy<std::_Deque_iterator<unsigned char, unsigned char const&, unsigned char
// const*>, std::_Deque_iterator<unsigned char, unsigned char&, unsigned
// char*>>(std::_Deque_iterator<unsigned char, unsigned char const&, unsigned char const*>,
// std::_Deque_iterator<unsigned char, unsigned char const&, unsigned char const*>,
// std::_Deque_iterator<unsigned char, unsigned char&, unsigned char*>)

_Deque_iterator
std::
uninitialized_copy_std___Deque_iterator_unsigned_char_unsigned_char_const__unsigned_char_const___std___Deque_iterator_unsigned_char_unsigned_char__unsigned_char___
          (_Deque_iterator param_1,_Deque_iterator param_2,_Deque_iterator param_3)

{
  _Deque_iterator *in_RCX;
  undefined7 in_register_00000011;
  undefined7 in_register_00000031;
  undefined7 in_register_00000039;
  long in_FS_OFFSET;
  _Deque_iterator_unsigned_char_unsigned_char_const__unsigned_char_const__ local_78 [32];
  _Deque_iterator_unsigned_char_unsigned_char_const__unsigned_char_const__ local_58 [32];
  _Deque_iterator_unsigned_char_unsigned_char__unsigned_char__ local_38 [40];
  long local_10;
  
  local_10 = *(long *)(in_FS_OFFSET + 0x28);
  _Deque_iterator<unsigned_char,unsigned_char&,unsigned_char*>::_Deque_iterator(local_38,in_RCX);
  _Deque_iterator<unsigned_char,unsigned_char_const&,unsigned_char_const*>::_Deque_iterator
            (local_58,(_Deque_iterator *)CONCAT71(in_register_00000011,param_3));
  _Deque_iterator<unsigned_char,unsigned_char_const&,unsigned_char_const*>::_Deque_iterator
            (local_78,(_Deque_iterator *)CONCAT71(in_register_00000031,param_2));
  __uninitialized_copy<true>::
  __uninit_copy_std___Deque_iterator_unsigned_char_unsigned_char_const__unsigned_char_const___std___Deque_iterator_unsigned_char_unsigned_char__unsigned_char___
            ((__uninitialized_copy_true_ *)CONCAT71(in_register_00000039,param_1),
             (_Deque_iterator)0x88,(_Deque_iterator)0xa8,(_Deque_iterator)0xc8);
  if (local_10 != *(long *)(in_FS_OFFSET + 0x28)) {
                    // WARNING: Subroutine does not return
    __stack_chk_fail();
  }
  return param_1;
}



// std::deque<unsigned char, std::allocator<unsigned char>>::_M_new_elements_at_back(unsigned long)

void __thiscall
std::deque<unsigned_char,std::allocator<unsigned_char>>::_M_new_elements_at_back
          (deque_unsigned_char_std__allocator_unsigned_char__ *this,ulong param_1)

{
  long lVar1;
  long lVar2;
  ulong uVar3;
  undefined8 uVar4;
  ulong local_30;
  
  lVar1 = max_size(this);
  lVar2 = size(this);
  if ((ulong)(lVar1 - lVar2) < param_1) {
    std::__throw_length_error("deque::_M_new_elements_at_back");
  }
  lVar1 = _S_buffer_size();
  uVar3 = _S_buffer_size();
  uVar3 = ((lVar1 + param_1) - 1) / uVar3;
  _M_reserve_map_at_back(this,uVar3);
  for (local_30 = 1; local_30 <= uVar3; local_30 = local_30 + 1) {
    lVar1 = *(long *)(this + 0x48);
                    // try { // try from 001082f8 to 001082fc has its CatchHandler @ 00108307
    uVar4 = _Deque_base<unsigned_char,std::allocator<unsigned_char>>::_M_allocate_node
                      ((_Deque_base_unsigned_char_std__allocator_unsigned_char__ *)this);
    *(undefined8 *)(lVar1 + local_30 * 8) = uVar4;
  }
  return;
}



// std::_Deque_iterator<unsigned char, unsigned char&, unsigned
// char*>::TEMPNAMEPLACEHOLDERVALUE(long) const

_Deque_iterator_unsigned_char_unsigned_char__unsigned_char__ * __thiscall
std::_Deque_iterator<unsigned_char,unsigned_char&,unsigned_char*>::operator_
          (_Deque_iterator_unsigned_char_unsigned_char__unsigned_char__ *this,long param_1)

{
  _Deque_iterator *p_Var1;
  long in_RDX;
  long in_FS_OFFSET;
  _Deque_iterator_unsigned_char_unsigned_char__unsigned_char__ local_38 [40];
  long local_10;
  
  local_10 = *(long *)(in_FS_OFFSET + 0x28);
  _Deque_iterator(local_38,(_Deque_iterator *)param_1);
  p_Var1 = (_Deque_iterator *)operator__(local_38,in_RDX);
  _Deque_iterator(this,p_Var1);
  if (local_10 != *(long *)(in_FS_OFFSET + 0x28)) {
                    // WARNING: Subroutine does not return
    __stack_chk_fail();
  }
  return this;
}



// WARNING: Unknown calling convention yet parameter storage is locked
// std::_Deque_iterator<unsigned char, unsigned char&, unsigned char*>
// std::__uninitialized_move_a<std::_Deque_iterator<unsigned char, unsigned char&, unsigned char*>,
// std::_Deque_iterator<unsigned char, unsigned char&, unsigned char*>, std::allocator<unsigned
// char>>(std::_Deque_iterator<unsigned char, unsigned char&, unsigned char*>,
// std::_Deque_iterator<unsigned char, unsigned char&, unsigned char*>,
// std::_Deque_iterator<unsigned char, unsigned char&, unsigned char*>, std::allocator<unsigned
// char>&)

_Deque_iterator
std::
__uninitialized_move_a_std___Deque_iterator_unsigned_char_unsigned_char__unsigned_char___std___Deque_iterator_unsigned_char_unsigned_char__unsigned_char___std__allocator_unsigned_char__
          (_Deque_iterator param_1,_Deque_iterator param_2,_Deque_iterator param_3,
          allocator *param_4)

{
  undefined7 in_register_00000011;
  undefined7 in_register_00000031;
  undefined7 in_register_00000039;
  long in_FS_OFFSET;
  _Deque_iterator_unsigned_char_unsigned_char__unsigned_char__ local_b8 [64];
  _Deque_iterator_unsigned_char_unsigned_char__unsigned_char__ local_78 [64];
  _Deque_iterator_unsigned_char_unsigned_char__unsigned_char__ local_38 [40];
  long local_10;
  
  local_10 = *(long *)(in_FS_OFFSET + 0x28);
  _Deque_iterator<unsigned_char,unsigned_char&,unsigned_char*>::_Deque_iterator
            (local_38,(_Deque_iterator *)param_4);
  _Deque_iterator<unsigned_char,unsigned_char&,unsigned_char*>::_Deque_iterator
            (local_78,(_Deque_iterator *)CONCAT71(in_register_00000011,param_3));
                    // try { // try from 0010846a to 0010846e has its CatchHandler @ 001084d1
  make_move_iterator_std___Deque_iterator_unsigned_char_unsigned_char__unsigned_char___
            ((_Deque_iterator)0xa8);
  _Deque_iterator<unsigned_char,unsigned_char&,unsigned_char*>::_Deque_iterator
            (local_b8,(_Deque_iterator *)CONCAT71(in_register_00000031,param_2));
                    // try { // try from 0010849c to 001084c8 has its CatchHandler @ 001084cb
  make_move_iterator_std___Deque_iterator_unsigned_char_unsigned_char__unsigned_char___
            ((_Deque_iterator)0x68);
  __uninitialized_copy_a_std__move_iterator_std___Deque_iterator_unsigned_char_unsigned_char__unsigned_char____std___Deque_iterator_unsigned_char_unsigned_char__unsigned_char___unsigned_char_
            ((move_iterator)CONCAT71(in_register_00000039,param_1),(int)register0x00000020 - 0x98,
             (_Deque_iterator)0xa8,(allocator *)local_38);
  if (local_10 != *(long *)(in_FS_OFFSET + 0x28)) {
                    // WARNING: Subroutine does not return
    __stack_chk_fail();
  }
  return param_1;
}



// WARNING: Unknown calling convention yet parameter storage is locked
// std::_Deque_iterator<unsigned char, unsigned char&, unsigned char*> std::move<unsigned
// char>(std::_Deque_iterator<unsigned char, unsigned char&, unsigned char*>,
// std::_Deque_iterator<unsigned char, unsigned char&, unsigned char*>,
// std::_Deque_iterator<unsigned char, unsigned char&, unsigned char*>)

_Deque_iterator
std::move_unsigned_char_(_Deque_iterator param_1,_Deque_iterator param_2,_Deque_iterator param_3)

{
  _Deque_iterator *in_RCX;
  undefined7 in_register_00000011;
  undefined7 in_register_00000031;
  long in_FS_OFFSET;
  _Deque_iterator_unsigned_char_unsigned_char_const__unsigned_char_const__ local_78 [32];
  _Deque_iterator_unsigned_char_unsigned_char_const__unsigned_char_const__ local_58 [32];
  _Deque_iterator_unsigned_char_unsigned_char__unsigned_char__ local_38 [40];
  long local_10;
  
  local_10 = *(long *)(in_FS_OFFSET + 0x28);
  _Deque_iterator<unsigned_char,unsigned_char&,unsigned_char*>::_Deque_iterator(local_38,in_RCX);
  _Deque_iterator<unsigned_char,unsigned_char_const&,unsigned_char_const*>::
  _Deque_iterator_std___Deque_iterator_unsigned_char_unsigned_char__unsigned_char___void_
            (local_58,(_Deque_iterator *)CONCAT71(in_register_00000011,param_3));
  _Deque_iterator<unsigned_char,unsigned_char_const&,unsigned_char_const*>::
  _Deque_iterator_std___Deque_iterator_unsigned_char_unsigned_char__unsigned_char___void_
            (local_78,(_Deque_iterator *)CONCAT71(in_register_00000031,param_2));
  move_unsigned_char_(param_1,(_Deque_iterator)0x88,(_Deque_iterator)0xa8);
  if (local_10 != *(long *)(in_FS_OFFSET + 0x28)) {
                    // WARNING: Subroutine does not return
    __stack_chk_fail();
  }
  return param_1;
}



// WARNING: Unknown calling convention yet parameter storage is locked
// void std::advance<std::_Deque_iterator<unsigned char, unsigned char const&, unsigned char
// const*>, long>(std::_Deque_iterator<unsigned char, unsigned char const&, unsigned char const*>&,
// long)

void std::
     advance_std___Deque_iterator_unsigned_char_unsigned_char_const__unsigned_char_const___long_
               (_Deque_iterator *param_1,long param_2)

{
  long lVar1;
  long in_FS_OFFSET;
  
  lVar1 = *(long *)(in_FS_OFFSET + 0x28);
  __iterator_category_std___Deque_iterator_unsigned_char_unsigned_char_const__unsigned_char_const___
            (param_1);
  __advance_std___Deque_iterator_unsigned_char_unsigned_char_const__unsigned_char_const___long_
            (param_1,param_2,(random_access_iterator_tag)param_2);
  if (lVar1 != *(long *)(in_FS_OFFSET + 0x28)) {
                    // WARNING: Subroutine does not return
    __stack_chk_fail();
  }
  return;
}



// WARNING: Unknown calling convention yet parameter storage is locked
// std::_Deque_iterator<unsigned char, unsigned char&, unsigned char*>
// std::__uninitialized_move_copy<std::_Deque_iterator<unsigned char, unsigned char&, unsigned
// char*>, std::_Deque_iterator<unsigned char, unsigned char const&, unsigned char const*>,
// std::_Deque_iterator<unsigned char, unsigned char&, unsigned char*>, std::allocator<unsigned
// char>>(std::_Deque_iterator<unsigned char, unsigned char&, unsigned char*>,
// std::_Deque_iterator<unsigned char, unsigned char&, unsigned char*>,
// std::_Deque_iterator<unsigned char, unsigned char const&, unsigned char const*>,
// std::_Deque_iterator<unsigned char, unsigned char const&, unsigned char const*>,
// std::_Deque_iterator<unsigned char, unsigned char&, unsigned char*>, std::allocator<unsigned
// char>&)

_Deque_iterator
std::
__uninitialized_move_copy_std___Deque_iterator_unsigned_char_unsigned_char__unsigned_char___std___Deque_iterator_unsigned_char_unsigned_char_const__unsigned_char_const___std___Deque_iterator_unsigned_char_unsigned_char__unsigned_char___std__allocator_unsigned_char__
          (_Deque_iterator param_1,_Deque_iterator param_2,_Deque_iterator param_3,
          _Deque_iterator param_4,_Deque_iterator param_5,allocator *param_6)

{
  undefined7 in_register_00000009;
  undefined7 in_register_00000011;
  undefined7 in_register_00000031;
  undefined7 in_register_00000081;
  long in_FS_OFFSET;
  _Deque_iterator local_a8 [32];
  _Deque_iterator_unsigned_char_unsigned_char__unsigned_char__ local_88 [32];
  _Deque_iterator_unsigned_char_unsigned_char__unsigned_char__ local_68 [32];
  _Deque_iterator_unsigned_char_unsigned_char__unsigned_char__ local_48 [40];
  long local_20;
  
  local_20 = *(long *)(in_FS_OFFSET + 0x28);
  _Deque_iterator<unsigned_char,unsigned_char&,unsigned_char*>::_Deque_iterator
            (local_48,(_Deque_iterator *)param_6);
  _Deque_iterator<unsigned_char,unsigned_char&,unsigned_char*>::_Deque_iterator
            (local_68,(_Deque_iterator *)CONCAT71(in_register_00000011,param_3));
  _Deque_iterator<unsigned_char,unsigned_char&,unsigned_char*>::_Deque_iterator
            (local_88,(_Deque_iterator *)CONCAT71(in_register_00000031,param_2));
  __uninitialized_move_a_std___Deque_iterator_unsigned_char_unsigned_char__unsigned_char___std___Deque_iterator_unsigned_char_unsigned_char__unsigned_char___std__allocator_unsigned_char__
            ((_Deque_iterator)0x58,(_Deque_iterator)0x78,(_Deque_iterator)0x98,(allocator *)local_48
            );
  _Deque_iterator<unsigned_char,unsigned_char&,unsigned_char*>::_Deque_iterator(local_48,local_a8);
  _Deque_iterator<unsigned_char,unsigned_char_const&,unsigned_char_const*>::_Deque_iterator
            ((_Deque_iterator_unsigned_char_unsigned_char_const__unsigned_char_const__ *)local_68,
             (_Deque_iterator *)CONCAT71(in_register_00000081,param_5));
  _Deque_iterator<unsigned_char,unsigned_char_const&,unsigned_char_const*>::_Deque_iterator
            ((_Deque_iterator_unsigned_char_unsigned_char_const__unsigned_char_const__ *)local_88,
             (_Deque_iterator *)CONCAT71(in_register_00000009,param_4));
                    // try { // try from 0010871d to 00108721 has its CatchHandler @ 00108724
  __uninitialized_copy_a_std___Deque_iterator_unsigned_char_unsigned_char_const__unsigned_char_const___std___Deque_iterator_unsigned_char_unsigned_char__unsigned_char___unsigned_char_
            (param_1,(_Deque_iterator)0x78,(_Deque_iterator)0x98,(allocator *)local_48);
  if (local_20 != *(long *)(in_FS_OFFSET + 0x28)) {
                    // WARNING: Subroutine does not return
    __stack_chk_fail();
  }
  return param_1;
}



// WARNING: Unknown calling convention yet parameter storage is locked
// std::_Deque_iterator<unsigned char, unsigned char&, unsigned char*> std::move_backward<unsigned
// char>(std::_Deque_iterator<unsigned char, unsigned char&, unsigned char*>,
// std::_Deque_iterator<unsigned char, unsigned char&, unsigned char*>,
// std::_Deque_iterator<unsigned char, unsigned char&, unsigned char*>)

_Deque_iterator
std::move_backward_unsigned_char_
          (_Deque_iterator param_1,_Deque_iterator param_2,_Deque_iterator param_3)

{
  _Deque_iterator *in_RCX;
  undefined7 in_register_00000011;
  undefined7 in_register_00000031;
  long in_FS_OFFSET;
  _Deque_iterator_unsigned_char_unsigned_char_const__unsigned_char_const__ local_78 [32];
  _Deque_iterator_unsigned_char_unsigned_char_const__unsigned_char_const__ local_58 [32];
  _Deque_iterator_unsigned_char_unsigned_char__unsigned_char__ local_38 [40];
  long local_10;
  
  local_10 = *(long *)(in_FS_OFFSET + 0x28);
  _Deque_iterator<unsigned_char,unsigned_char&,unsigned_char*>::_Deque_iterator(local_38,in_RCX);
  _Deque_iterator<unsigned_char,unsigned_char_const&,unsigned_char_const*>::
  _Deque_iterator_std___Deque_iterator_unsigned_char_unsigned_char__unsigned_char___void_
            (local_58,(_Deque_iterator *)CONCAT71(in_register_00000011,param_3));
  _Deque_iterator<unsigned_char,unsigned_char_const&,unsigned_char_const*>::
  _Deque_iterator_std___Deque_iterator_unsigned_char_unsigned_char__unsigned_char___void_
            (local_78,(_Deque_iterator *)CONCAT71(in_register_00000031,param_2));
  move_backward_unsigned_char_(param_1,(_Deque_iterator)0x88,(_Deque_iterator)0xa8);
  if (local_10 != *(long *)(in_FS_OFFSET + 0x28)) {
                    // WARNING: Subroutine does not return
    __stack_chk_fail();
  }
  return param_1;
}



// WARNING: Unknown calling convention yet parameter storage is locked
// std::_Deque_iterator<unsigned char, unsigned char&, unsigned char*>
// std::__uninitialized_copy_move<std::_Deque_iterator<unsigned char, unsigned char const&, unsigned
// char const*>, std::_Deque_iterator<unsigned char, unsigned char&, unsigned char*>,
// std::_Deque_iterator<unsigned char, unsigned char&, unsigned char*>, std::allocator<unsigned
// char>>(std::_Deque_iterator<unsigned char, unsigned char const&, unsigned char const*>,
// std::_Deque_iterator<unsigned char, unsigned char const&, unsigned char const*>,
// std::_Deque_iterator<unsigned char, unsigned char&, unsigned char*>,
// std::_Deque_iterator<unsigned char, unsigned char&, unsigned char*>,
// std::_Deque_iterator<unsigned char, unsigned char&, unsigned char*>, std::allocator<unsigned
// char>&)

_Deque_iterator
std::
__uninitialized_copy_move_std___Deque_iterator_unsigned_char_unsigned_char_const__unsigned_char_const___std___Deque_iterator_unsigned_char_unsigned_char__unsigned_char___std___Deque_iterator_unsigned_char_unsigned_char__unsigned_char___std__allocator_unsigned_char__
          (_Deque_iterator param_1,_Deque_iterator param_2,_Deque_iterator param_3,
          _Deque_iterator param_4,_Deque_iterator param_5,allocator *param_6)

{
  undefined7 in_register_00000009;
  undefined7 in_register_00000011;
  undefined7 in_register_00000031;
  undefined7 in_register_00000081;
  long in_FS_OFFSET;
  _Deque_iterator local_a8 [32];
  _Deque_iterator_unsigned_char_unsigned_char_const__unsigned_char_const__ local_88 [32];
  _Deque_iterator_unsigned_char_unsigned_char_const__unsigned_char_const__ local_68 [32];
  _Deque_iterator_unsigned_char_unsigned_char__unsigned_char__ local_48 [40];
  long local_20;
  
  local_20 = *(long *)(in_FS_OFFSET + 0x28);
  _Deque_iterator<unsigned_char,unsigned_char&,unsigned_char*>::_Deque_iterator
            (local_48,(_Deque_iterator *)param_6);
  _Deque_iterator<unsigned_char,unsigned_char_const&,unsigned_char_const*>::_Deque_iterator
            (local_68,(_Deque_iterator *)CONCAT71(in_register_00000011,param_3));
  _Deque_iterator<unsigned_char,unsigned_char_const&,unsigned_char_const*>::_Deque_iterator
            (local_88,(_Deque_iterator *)CONCAT71(in_register_00000031,param_2));
  __uninitialized_copy_a_std___Deque_iterator_unsigned_char_unsigned_char_const__unsigned_char_const___std___Deque_iterator_unsigned_char_unsigned_char__unsigned_char___unsigned_char_
            ((_Deque_iterator)0x58,(_Deque_iterator)0x78,(_Deque_iterator)0x98,(allocator *)local_48
            );
  _Deque_iterator<unsigned_char,unsigned_char&,unsigned_char*>::_Deque_iterator(local_48,local_a8);
  _Deque_iterator<unsigned_char,unsigned_char&,unsigned_char*>::_Deque_iterator
            ((_Deque_iterator_unsigned_char_unsigned_char__unsigned_char__ *)local_68,
             (_Deque_iterator *)CONCAT71(in_register_00000081,param_5));
  _Deque_iterator<unsigned_char,unsigned_char&,unsigned_char*>::_Deque_iterator
            ((_Deque_iterator_unsigned_char_unsigned_char__unsigned_char__ *)local_88,
             (_Deque_iterator *)CONCAT71(in_register_00000009,param_4));
                    // try { // try from 00108979 to 0010897d has its CatchHandler @ 00108980
  __uninitialized_move_a_std___Deque_iterator_unsigned_char_unsigned_char__unsigned_char___std___Deque_iterator_unsigned_char_unsigned_char__unsigned_char___std__allocator_unsigned_char__
            (param_1,(_Deque_iterator)0x78,(_Deque_iterator)0x98,(allocator *)local_48);
  if (local_20 != *(long *)(in_FS_OFFSET + 0x28)) {
                    // WARNING: Subroutine does not return
    __stack_chk_fail();
  }
  return param_1;
}



// std::allocator<unsigned char>::allocator()

void std::allocator<unsigned_char>::allocator(void)

{
  __gnu_cxx::new_allocator<unsigned_char>::new_allocator();
  return;
}



// std::_Deque_iterator<unsigned char, unsigned char&, unsigned char*>::_Deque_iterator()

void __thiscall
std::_Deque_iterator<unsigned_char,unsigned_char&,unsigned_char*>::_Deque_iterator
          (_Deque_iterator_unsigned_char_unsigned_char__unsigned_char__ *this)

{
  *(undefined8 *)this = 0;
  *(undefined8 *)(this + 8) = 0;
  *(undefined8 *)(this + 0x10) = 0;
  *(undefined8 *)(this + 0x18) = 0;
  return;
}



// WARNING: Unknown calling convention yet parameter storage is locked
// unsigned long const& std::max<unsigned long>(unsigned long const&, unsigned long const&)

ulong * std::max_unsigned_long_(ulong *param_1,ulong *param_2)

{
  if (*param_1 < *param_2) {
    param_1 = param_2;
  }
  return param_1;
}



// std::_Deque_base<unsigned char, std::allocator<unsigned char>>::_M_allocate_map(unsigned long)

undefined8 __thiscall
std::_Deque_base<unsigned_char,std::allocator<unsigned_char>>::_M_allocate_map
          (_Deque_base_unsigned_char_std__allocator_unsigned_char__ *this,ulong param_1)

{
  undefined8 uVar1;
  long in_FS_OFFSET;
  allocator local_21;
  long local_20;
  
  local_20 = *(long *)(in_FS_OFFSET + 0x28);
  _M_get_map_allocator();
                    // try { // try from 00108ae7 to 00108aeb has its CatchHandler @ 00108b10
  uVar1 = allocator_traits<std::allocator<unsigned_char*>>::allocate(&local_21,param_1);
  allocator<unsigned_char*>::_allocator((allocator_unsigned_char__ *)&local_21);
  if (local_20 != *(long *)(in_FS_OFFSET + 0x28)) {
                    // WARNING: Subroutine does not return
    __stack_chk_fail();
  }
  return uVar1;
}



// std::_Deque_base<unsigned char, std::allocator<unsigned char>>::_M_create_nodes(unsigned char**,
// unsigned char**)

void __thiscall
std::_Deque_base<unsigned_char,std::allocator<unsigned_char>>::_M_create_nodes
          (_Deque_base_unsigned_char_std__allocator_unsigned_char__ *this,uchar **param_1,
          uchar **param_2)

{
  uchar *puVar1;
  uchar **local_20;
  
  for (local_20 = param_1; local_20 < param_2; local_20 = local_20 + 1) {
                    // try { // try from 00108b6c to 00108b70 has its CatchHandler @ 00108b7f
    puVar1 = (uchar *)_M_allocate_node(this);
    *local_20 = puVar1;
  }
  return;
}



// std::allocator<unsigned char*>::allocator<unsigned char>(std::allocator<unsigned char> const&)

void std::allocator<unsigned_char*>::allocator_unsigned_char_(allocator *param_1)

{
  __gnu_cxx::new_allocator<unsigned_char*>::new_allocator();
  return;
}



// __gnu_cxx::new_allocator<unsigned char*>::~new_allocator()

void __thiscall
__gnu_cxx::new_allocator<unsigned_char*>::_new_allocator(new_allocator_unsigned_char__ *this)

{
  return;
}



// __gnu_cxx::new_allocator<unsigned char*>::deallocate(unsigned char**, unsigned long)

void __gnu_cxx::new_allocator<unsigned_char*>::deallocate(uchar **param_1,ulong param_2)

{
  operator_delete((void *)param_2);
  return;
}



// std::deque<unsigned char, std::allocator<unsigned char>>::_S_max_size(std::allocator<unsigned
// char> const&)

ulong std::deque<unsigned_char,std::allocator<unsigned_char>>::_S_max_size(allocator *param_1)

{
  ulong *puVar1;
  long in_FS_OFFSET;
  ulong local_20;
  ulong local_18;
  long local_10;
  
  local_10 = *(long *)(in_FS_OFFSET + 0x28);
  local_20 = 0x7fffffffffffffff;
  local_18 = allocator_traits<std::allocator<unsigned_char>>::max_size(param_1);
  puVar1 = min_unsigned_long_(&local_20,&local_18);
  if (local_10 != *(long *)(in_FS_OFFSET + 0x28)) {
                    // WARNING: Subroutine does not return
    __stack_chk_fail();
  }
  return *puVar1;
}



// std::deque<unsigned char, std::allocator<unsigned char>>::_M_reallocate_map(unsigned long, bool)

void __thiscall
std::deque<unsigned_char,std::allocator<unsigned_char>>::_M_reallocate_map
          (deque_unsigned_char_std__allocator_unsigned_char__ *this,ulong param_1,bool param_2)

{
  long lVar1;
  ulong *puVar2;
  ulong local_58;
  uchar **local_50;
  uchar **local_40;
  long local_38;
  long local_30;
  uchar *local_28;
  uchar *local_20;
  
  local_38 = (*(long *)(this + 0x48) - *(long *)(this + 0x28) >> 3) + 1;
  local_30 = local_38 + param_1;
  local_58 = param_1;
  local_50 = (uchar **)this;
  if ((ulong)(local_30 * 2) < *(ulong *)(this + 8)) {
    if (param_2 == false) {
      lVar1 = 0;
    }
    else {
      lVar1 = param_1 << 3;
    }
    local_40 = (uchar **)
               (lVar1 + ((ulong)(*(long *)(this + 8) - local_30) >> 1) * 8 + *(long *)this);
    if (local_40 < *(uchar ***)(this + 0x28)) {
      copy_unsigned_char___unsigned_char___
                (*(uchar ***)(this + 0x28),(uchar **)(*(long *)(this + 0x48) + 8),local_40);
    }
    else {
      copy_backward_unsigned_char___unsigned_char___
                (*(uchar ***)(this + 0x28),(uchar **)(*(long *)(this + 0x48) + 8),
                 local_40 + local_38);
    }
  }
  else {
    lVar1 = *(long *)(this + 8);
    puVar2 = max_unsigned_long_((ulong *)(this + 8),&local_58);
    local_28 = (uchar *)(*puVar2 + lVar1 + 2);
    local_20 = (uchar *)_Deque_base<unsigned_char,std::allocator<unsigned_char>>::_M_allocate_map
                                  ((_Deque_base_unsigned_char_std__allocator_unsigned_char__ *)
                                   local_50,(ulong)local_28);
    if (param_2 == false) {
      lVar1 = 0;
    }
    else {
      lVar1 = local_58 << 3;
    }
    local_40 = (uchar **)(local_20 + ((ulong)(local_28 + -local_30) >> 1) * 8 + lVar1);
    copy_unsigned_char___unsigned_char___
              ((uchar **)local_50[5],(uchar **)(local_50[9] + 8),local_40);
    _Deque_base<unsigned_char,std::allocator<unsigned_char>>::_M_deallocate_map
              ((_Deque_base_unsigned_char_std__allocator_unsigned_char__ *)local_50,
               (uchar **)*local_50,(ulong)local_50[1]);
    *local_50 = local_20;
    local_50[1] = local_28;
  }
  _Deque_iterator<unsigned_char,unsigned_char&,unsigned_char*>::_M_set_node
            ((_Deque_iterator_unsigned_char_unsigned_char__unsigned_char__ *)(local_50 + 2),local_40
            );
  _Deque_iterator<unsigned_char,unsigned_char&,unsigned_char*>::_M_set_node
            ((_Deque_iterator_unsigned_char_unsigned_char__unsigned_char__ *)(local_50 + 6),
             local_40 + local_38 + -1);
  return;
}



// std::allocator_traits<std::allocator<unsigned char>>::allocate(std::allocator<unsigned char>&,
// unsigned long)

void std::allocator_traits<std::allocator<unsigned_char>>::allocate
               (allocator *param_1,ulong param_2)

{
  __gnu_cxx::new_allocator<unsigned_char>::allocate((ulong)param_1,(void *)param_2);
  return;
}



// std::_Deque_base<unsigned char, std::allocator<unsigned
// char>>::_Deque_impl::_Deque_impl(std::allocator<unsigned char> const&)

void __thiscall
std::_Deque_base<unsigned_char,std::allocator<unsigned_char>>::_Deque_impl::_Deque_impl
          (_Deque_impl *this,allocator *param_1)

{
  allocator<unsigned_char>::allocator((allocator *)this);
  *(undefined8 *)this = 0;
  *(undefined8 *)(this + 8) = 0;
  _Deque_iterator<unsigned_char,unsigned_char&,unsigned_char*>::_Deque_iterator
            ((_Deque_iterator_unsigned_char_unsigned_char__unsigned_char__ *)(this + 0x10));
  _Deque_iterator<unsigned_char,unsigned_char&,unsigned_char*>::_Deque_iterator
            ((_Deque_iterator_unsigned_char_unsigned_char__unsigned_char__ *)(this + 0x30));
  return;
}



// __gnu_cxx::new_allocator<unsigned char>::deallocate(unsigned char*, unsigned long)

void __gnu_cxx::new_allocator<unsigned_char>::deallocate(uchar *param_1,ulong param_2)

{
  operator_delete((void *)param_2);
  return;
}



// WARNING: Unknown calling convention yet parameter storage is locked
// std::remove_reference<std::_Deque_iterator<unsigned char, unsigned char&, unsigned
// char*>&>::type&& std::move<std::_Deque_iterator<unsigned char, unsigned char&, unsigned
// char*>&>(std::_Deque_iterator<unsigned char, unsigned char&, unsigned char*>&)

type * std::move_std___Deque_iterator_unsigned_char_unsigned_char__unsigned_char____
                 (_Deque_iterator *param_1)

{
  return (type *)param_1;
}



// WARNING: Unknown calling convention yet parameter storage is locked
// unsigned char* std::__niter_base<unsigned char*>(unsigned char*)

uchar * std::__niter_base_unsigned_char__(uchar *param_1)

{
  return param_1;
}



// WARNING: Unknown calling convention yet parameter storage is locked
// unsigned char* std::__copy_move_a<false, unsigned char*, unsigned char*>(unsigned char*, unsigned
// char*, unsigned char*)

uchar * std::__copy_move_a_false_unsigned_char__unsigned_char__
                  (uchar *param_1,uchar *param_2,uchar *param_3)

{
  uchar *puVar1;
  
  puVar1 = __copy_move<false,true,std::random_access_iterator_tag>::__copy_m_unsigned_char_
                     (param_1,param_2,param_3);
  return puVar1;
}



// WARNING: Unknown calling convention yet parameter storage is locked
// unsigned char* std::__niter_wrap<unsigned char*>(unsigned char* const&, unsigned char*)

uchar * std::__niter_wrap_unsigned_char__(uchar **param_1,uchar *param_2)

{
  return param_2;
}



// std::deque<unsigned char, std::allocator<unsigned char>>::_M_reserve_map_at_front(unsigned long)

void __thiscall
std::deque<unsigned_char,std::allocator<unsigned_char>>::_M_reserve_map_at_front
          (deque_unsigned_char_std__allocator_unsigned_char__ *this,ulong param_1)

{
  if ((ulong)(*(long *)(this + 0x28) - *(long *)this >> 3) < param_1) {
    _M_reallocate_map(this,param_1,true);
  }
  return;
}



// std::_Deque_iterator<unsigned char, unsigned char&, unsigned
// char*>::TEMPNAMEPLACEHOLDERVALUE(long)

void __thiscall
std::_Deque_iterator<unsigned_char,unsigned_char&,unsigned_char*>::operator__
          (_Deque_iterator_unsigned_char_unsigned_char__unsigned_char__ *this,long param_1)

{
  operator__(this,-param_1);
  return;
}



// std::_Deque_iterator<unsigned char, unsigned char&, unsigned char*>
// std::__uninitialized_copy<true>::__uninit_copy<std::_Deque_iterator<unsigned char, unsigned char
// const&, unsigned char const*>, std::_Deque_iterator<unsigned char, unsigned char&, unsigned
// char*>>(std::_Deque_iterator<unsigned char, unsigned char const&, unsigned char const*>,
// std::_Deque_iterator<unsigned char, unsigned char const&, unsigned char const*>,
// std::_Deque_iterator<unsigned char, unsigned char&, unsigned char*>)

_Deque_iterator __thiscall
std::__uninitialized_copy<true>::
__uninit_copy_std___Deque_iterator_unsigned_char_unsigned_char_const__unsigned_char_const___std___Deque_iterator_unsigned_char_unsigned_char__unsigned_char___
          (__uninitialized_copy_true_ *this,_Deque_iterator param_1,_Deque_iterator param_2,
          _Deque_iterator param_3)

{
  undefined7 in_register_00000009;
  undefined7 in_register_00000011;
  undefined7 in_register_00000031;
  long in_FS_OFFSET;
  _Deque_iterator_unsigned_char_unsigned_char_const__unsigned_char_const__ local_78 [32];
  _Deque_iterator_unsigned_char_unsigned_char_const__unsigned_char_const__ local_58 [32];
  _Deque_iterator_unsigned_char_unsigned_char__unsigned_char__ local_38 [40];
  long local_10;
  
  local_10 = *(long *)(in_FS_OFFSET + 0x28);
  _Deque_iterator<unsigned_char,unsigned_char&,unsigned_char*>::_Deque_iterator
            (local_38,(_Deque_iterator *)CONCAT71(in_register_00000009,param_3));
  _Deque_iterator<unsigned_char,unsigned_char_const&,unsigned_char_const*>::_Deque_iterator
            (local_58,(_Deque_iterator *)CONCAT71(in_register_00000011,param_2));
  _Deque_iterator<unsigned_char,unsigned_char_const&,unsigned_char_const*>::_Deque_iterator
            (local_78,(_Deque_iterator *)CONCAT71(in_register_00000031,param_1));
  copy_std___Deque_iterator_unsigned_char_unsigned_char_const__unsigned_char_const___std___Deque_iterator_unsigned_char_unsigned_char__unsigned_char___
            (SUB81(this,0),(_Deque_iterator)0x88,(_Deque_iterator)0xa8);
  if (local_10 != *(long *)(in_FS_OFFSET + 0x28)) {
                    // WARNING: Subroutine does not return
    __stack_chk_fail();
  }
  return SUB81(this,0);
}



// WARNING: Unknown calling convention yet parameter storage is locked
// std::move_iterator<std::_Deque_iterator<unsigned char, unsigned char&, unsigned char*>>
// std::make_move_iterator<std::_Deque_iterator<unsigned char, unsigned char&, unsigned
// char*>>(std::_Deque_iterator<unsigned char, unsigned char&, unsigned char*>)

move_iterator
std::make_move_iterator_std___Deque_iterator_unsigned_char_unsigned_char__unsigned_char___
          (_Deque_iterator param_1)

{
  _Deque_iterator *in_RSI;
  undefined7 in_register_00000039;
  long in_FS_OFFSET;
  _Deque_iterator_unsigned_char_unsigned_char__unsigned_char__ local_38 [40];
  long local_10;
  
  local_10 = *(long *)(in_FS_OFFSET + 0x28);
  _Deque_iterator<unsigned_char,unsigned_char&,unsigned_char*>::_Deque_iterator(local_38,in_RSI);
  move_iterator<std::_Deque_iterator<unsigned_char,unsigned_char&,unsigned_char*>>::move_iterator
            ((move_iterator_std___Deque_iterator_unsigned_char_unsigned_char__unsigned_char___ *)
             CONCAT71(in_register_00000039,param_1),(_Deque_iterator)0xc8);
  if (local_10 != *(long *)(in_FS_OFFSET + 0x28)) {
                    // WARNING: Subroutine does not return
    __stack_chk_fail();
  }
  return (move_iterator)
         (move_iterator_std___Deque_iterator_unsigned_char_unsigned_char__unsigned_char___ *)
         CONCAT71(in_register_00000039,param_1);
}



// std::move_iterator<std::_Deque_iterator<unsigned char, unsigned char&, unsigned
// char*>>::move_iterator(std::move_iterator<std::_Deque_iterator<unsigned char, unsigned char&,
// unsigned char*>> const&)

void __thiscall
std::move_iterator<std::_Deque_iterator<unsigned_char,unsigned_char&,unsigned_char*>>::move_iterator
          (move_iterator_std___Deque_iterator_unsigned_char_unsigned_char__unsigned_char___ *this,
          move_iterator *param_1)

{
  _Deque_iterator<unsigned_char,unsigned_char&,unsigned_char*>::_Deque_iterator
            ((_Deque_iterator_unsigned_char_unsigned_char__unsigned_char__ *)this,
             (_Deque_iterator *)param_1);
  return;
}



// WARNING: Unknown calling convention yet parameter storage is locked
// std::_Deque_iterator<unsigned char, unsigned char&, unsigned char*>
// std::__uninitialized_copy_a<std::move_iterator<std::_Deque_iterator<unsigned char, unsigned
// char&, unsigned char*>>, std::_Deque_iterator<unsigned char, unsigned char&, unsigned char*>,
// unsigned char>(std::move_iterator<std::_Deque_iterator<unsigned char, unsigned char&, unsigned
// char*>>, std::move_iterator<std::_Deque_iterator<unsigned char, unsigned char&, unsigned char*>>,
// std::_Deque_iterator<unsigned char, unsigned char&, unsigned char*>, std::allocator<unsigned
// char>&)

_Deque_iterator
std::
__uninitialized_copy_a_std__move_iterator_std___Deque_iterator_unsigned_char_unsigned_char__unsigned_char____std___Deque_iterator_unsigned_char_unsigned_char__unsigned_char___unsigned_char_
          (move_iterator param_1,move_iterator param_2,_Deque_iterator param_3,allocator *param_4)

{
  undefined7 in_register_00000011;
  undefined4 in_register_00000034;
  long in_FS_OFFSET;
  move_iterator_std___Deque_iterator_unsigned_char_unsigned_char__unsigned_char___ local_78 [32];
  move_iterator_std___Deque_iterator_unsigned_char_unsigned_char__unsigned_char___ local_58 [32];
  _Deque_iterator_unsigned_char_unsigned_char__unsigned_char__ local_38 [40];
  long local_10;
  
  local_10 = *(long *)(in_FS_OFFSET + 0x28);
  _Deque_iterator<unsigned_char,unsigned_char&,unsigned_char*>::_Deque_iterator
            (local_38,(_Deque_iterator *)param_4);
  move_iterator<std::_Deque_iterator<unsigned_char,unsigned_char&,unsigned_char*>>::move_iterator
            (local_58,(move_iterator *)CONCAT71(in_register_00000011,param_3));
  move_iterator<std::_Deque_iterator<unsigned_char,unsigned_char&,unsigned_char*>>::move_iterator
            (local_78,(move_iterator *)CONCAT44(in_register_00000034,param_2));
  uninitialized_copy_std__move_iterator_std___Deque_iterator_unsigned_char_unsigned_char__unsigned_char____std___Deque_iterator_unsigned_char_unsigned_char__unsigned_char___
            (param_1,(int)register0x00000020 - 0x78,(_Deque_iterator)0xa8);
  if (local_10 != *(long *)(in_FS_OFFSET + 0x28)) {
                    // WARNING: Subroutine does not return
    __stack_chk_fail();
  }
  return SUB41(param_1,0);
}



// WARNING: Unknown calling convention yet parameter storage is locked
// std::_Deque_iterator<unsigned char, unsigned char&, unsigned char*> std::move<unsigned
// char>(std::_Deque_iterator<unsigned char, unsigned char const&, unsigned char const*>,
// std::_Deque_iterator<unsigned char, unsigned char const&, unsigned char const*>,
// std::_Deque_iterator<unsigned char, unsigned char&, unsigned char*>)

_Deque_iterator
std::move_unsigned_char_(_Deque_iterator param_1,_Deque_iterator param_2,_Deque_iterator param_3)

{
  difference_type dVar1;
  undefined4 extraout_var;
  long *plVar2;
  uchar **in_RCX;
  undefined7 in_register_00000011;
  undefined7 in_register_00000031;
  uchar **this;
  undefined7 in_register_00000039;
  long in_FS_OFFSET;
  long local_30;
  uchar *local_28;
  uchar *local_20;
  long local_18;
  long local_10;
  
  this = (uchar **)CONCAT71(in_register_00000031,param_2);
  local_10 = *(long *)(in_FS_OFFSET + 0x28);
  dVar1 = operator_((_Deque_iterator *)CONCAT71(in_register_00000011,param_3),
                    (_Deque_iterator *)this);
  for (local_30 = CONCAT44(extraout_var,dVar1); 0 < local_30; local_30 = local_30 - local_18) {
    local_20 = in_RCX[2] + -(long)*in_RCX;
    local_28 = this[2] + -(long)*this;
    plVar2 = min_long_((long *)&local_28,(long *)&local_20);
    plVar2 = min_long_(&local_30,plVar2);
    local_18 = *plVar2;
    move_unsigned_char__unsigned_char__(*this,*this + local_18,*in_RCX);
    _Deque_iterator<unsigned_char,unsigned_char_const&,unsigned_char_const*>::operator__
              ((_Deque_iterator_unsigned_char_unsigned_char_const__unsigned_char_const__ *)this,
               local_18);
    _Deque_iterator<unsigned_char,unsigned_char&,unsigned_char*>::operator__
              ((_Deque_iterator_unsigned_char_unsigned_char__unsigned_char__ *)in_RCX,local_18);
  }
  _Deque_iterator<unsigned_char,unsigned_char&,unsigned_char*>::_Deque_iterator
            ((_Deque_iterator_unsigned_char_unsigned_char__unsigned_char__ *)
             CONCAT71(in_register_00000039,param_1),(_Deque_iterator *)in_RCX);
  if (local_10 != *(long *)(in_FS_OFFSET + 0x28)) {
                    // WARNING: Subroutine does not return
    __stack_chk_fail();
  }
  return param_1;
}



// WARNING: Unknown calling convention yet parameter storage is locked
// void std::__advance<std::_Deque_iterator<unsigned char, unsigned char const&, unsigned char
// const*>, long>(std::_Deque_iterator<unsigned char, unsigned char const&, unsigned char const*>&,
// long, std::random_access_iterator_tag)

void std::
     __advance_std___Deque_iterator_unsigned_char_unsigned_char_const__unsigned_char_const___long_
               (_Deque_iterator *param_1,long param_2,random_access_iterator_tag param_3)

{
  _Deque_iterator<unsigned_char,unsigned_char_const&,unsigned_char_const*>::operator__
            ((_Deque_iterator_unsigned_char_unsigned_char_const__unsigned_char_const__ *)param_1,
             param_2);
  return;
}



// WARNING: Unknown calling convention yet parameter storage is locked
// void std::_Destroy<std::_Deque_iterator<unsigned char, unsigned char&, unsigned char*>, unsigned
// char>(std::_Deque_iterator<unsigned char, unsigned char&, unsigned char*>,
// std::_Deque_iterator<unsigned char, unsigned char&, unsigned char*>, std::allocator<unsigned
// char>&)

void std::_Destroy_std___Deque_iterator_unsigned_char_unsigned_char__unsigned_char___unsigned_char_
               (_Deque_iterator param_1,_Deque_iterator param_2,allocator *param_3)

{
  undefined7 in_register_00000031;
  undefined7 in_register_00000039;
  long in_FS_OFFSET;
  _Deque_iterator_unsigned_char_unsigned_char__unsigned_char__ local_58 [32];
  _Deque_iterator_unsigned_char_unsigned_char__unsigned_char__ local_38 [40];
  long local_10;
  
  local_10 = *(long *)(in_FS_OFFSET + 0x28);
  _Deque_iterator<unsigned_char,unsigned_char&,unsigned_char*>::_Deque_iterator
            (local_38,(_Deque_iterator *)CONCAT71(in_register_00000031,param_2));
  _Deque_iterator<unsigned_char,unsigned_char&,unsigned_char*>::_Deque_iterator
            (local_58,(_Deque_iterator *)CONCAT71(in_register_00000039,param_1));
  _Destroy_std___Deque_iterator_unsigned_char_unsigned_char__unsigned_char___
            ((_Deque_iterator)0xa8,(_Deque_iterator)0xc8);
  if (local_10 != *(long *)(in_FS_OFFSET + 0x28)) {
                    // WARNING: Subroutine does not return
    __stack_chk_fail();
  }
  return;
}



// WARNING: Unknown calling convention yet parameter storage is locked
// std::_Deque_iterator<unsigned char, unsigned char&, unsigned char*> std::move_backward<unsigned
// char>(std::_Deque_iterator<unsigned char, unsigned char const&, unsigned char const*>,
// std::_Deque_iterator<unsigned char, unsigned char const&, unsigned char const*>,
// std::_Deque_iterator<unsigned char, unsigned char&, unsigned char*>)

_Deque_iterator
std::move_backward_unsigned_char_
          (_Deque_iterator param_1,_Deque_iterator param_2,_Deque_iterator param_3)

{
  difference_type dVar1;
  undefined4 extraout_var;
  long *plVar2;
  uchar **in_RCX;
  undefined7 in_register_00000011;
  uchar **this;
  undefined7 in_register_00000031;
  undefined7 in_register_00000039;
  long in_FS_OFFSET;
  long local_40;
  uchar *local_38;
  uchar *local_30;
  uchar *local_28;
  uchar *local_20;
  long local_18;
  long local_10;
  
  this = (uchar **)CONCAT71(in_register_00000011,param_3);
  local_10 = *(long *)(in_FS_OFFSET + 0x28);
  dVar1 = operator_((_Deque_iterator *)this,
                    (_Deque_iterator *)CONCAT71(in_register_00000031,param_2));
  for (local_40 = CONCAT44(extraout_var,dVar1); 0 < local_40; local_40 = local_40 - local_18) {
    local_38 = *this + -(long)this[1];
    local_28 = *this;
    local_30 = *in_RCX + -(long)in_RCX[1];
    local_20 = *in_RCX;
    if (local_38 == (uchar *)0x0) {
      local_38 = (uchar *)_Deque_iterator<unsigned_char,unsigned_char&,unsigned_char*>::
                          _S_buffer_size();
      local_28 = local_38 + *(long *)(this[3] + -8);
    }
    if (local_30 == (uchar *)0x0) {
      local_30 = (uchar *)_Deque_iterator<unsigned_char,unsigned_char&,unsigned_char*>::
                          _S_buffer_size();
      local_20 = local_30 + *(long *)(in_RCX[3] + -8);
    }
    plVar2 = min_long_((long *)&local_38,(long *)&local_30);
    plVar2 = min_long_(&local_40,plVar2);
    local_18 = *plVar2;
    move_backward_unsigned_char__unsigned_char__(local_28 + -local_18,local_28,local_20);
    _Deque_iterator<unsigned_char,unsigned_char_const&,unsigned_char_const*>::operator__
              ((_Deque_iterator_unsigned_char_unsigned_char_const__unsigned_char_const__ *)this,
               local_18);
    _Deque_iterator<unsigned_char,unsigned_char&,unsigned_char*>::operator__
              ((_Deque_iterator_unsigned_char_unsigned_char__unsigned_char__ *)in_RCX,local_18);
  }
  _Deque_iterator<unsigned_char,unsigned_char&,unsigned_char*>::_Deque_iterator
            ((_Deque_iterator_unsigned_char_unsigned_char__unsigned_char__ *)
             CONCAT71(in_register_00000039,param_1),(_Deque_iterator *)in_RCX);
  if (local_10 != *(long *)(in_FS_OFFSET + 0x28)) {
                    // WARNING: Subroutine does not return
    __stack_chk_fail();
  }
  return param_1;
}



// __gnu_cxx::new_allocator<unsigned char>::new_allocator()

void __gnu_cxx::new_allocator<unsigned_char>::new_allocator(void)

{
  return;
}



// std::allocator_traits<std::allocator<unsigned char*>>::allocate(std::allocator<unsigned char*>&,
// unsigned long)

void std::allocator_traits<std::allocator<unsigned_char*>>::allocate
               (allocator *param_1,ulong param_2)

{
  __gnu_cxx::new_allocator<unsigned_char*>::allocate((ulong)param_1,(void *)param_2);
  return;
}



// __gnu_cxx::new_allocator<unsigned char*>::new_allocator()

void __gnu_cxx::new_allocator<unsigned_char*>::new_allocator(void)

{
  return;
}



// std::allocator_traits<std::allocator<unsigned char>>::max_size(std::allocator<unsigned char>
// const&)

void std::allocator_traits<std::allocator<unsigned_char>>::max_size(allocator *param_1)

{
  __gnu_cxx::new_allocator<unsigned_char>::max_size();
  return;
}



// WARNING: Unknown calling convention yet parameter storage is locked
// unsigned long const& std::min<unsigned long>(unsigned long const&, unsigned long const&)

ulong * std::min_unsigned_long_(ulong *param_1,ulong *param_2)

{
  if (*param_2 < *param_1) {
    param_1 = param_2;
  }
  return param_1;
}



// WARNING: Unknown calling convention yet parameter storage is locked
// unsigned char** std::copy<unsigned char**, unsigned char**>(unsigned char**, unsigned char**,
// unsigned char**)

uchar ** std::copy_unsigned_char___unsigned_char___(uchar **param_1,uchar **param_2,uchar **param_3)

{
  uchar **ppuVar1;
  uchar **ppuVar2;
  
  ppuVar1 = __miter_base_unsigned_char___(param_2);
  ppuVar2 = __miter_base_unsigned_char___(param_1);
  ppuVar1 = __copy_move_a2_false_unsigned_char___unsigned_char___(ppuVar2,ppuVar1,param_3);
  return ppuVar1;
}



// WARNING: Unknown calling convention yet parameter storage is locked
// unsigned char** std::copy_backward<unsigned char**, unsigned char**>(unsigned char**, unsigned
// char**, unsigned char**)

uchar ** std::copy_backward_unsigned_char___unsigned_char___
                   (uchar **param_1,uchar **param_2,uchar **param_3)

{
  uchar **ppuVar1;
  uchar **ppuVar2;
  
  ppuVar1 = __miter_base_unsigned_char___(param_2);
  ppuVar2 = __miter_base_unsigned_char___(param_1);
  ppuVar1 = __copy_move_backward_a2_false_unsigned_char___unsigned_char___(ppuVar2,ppuVar1,param_3);
  return ppuVar1;
}



// __gnu_cxx::new_allocator<unsigned char>::allocate(unsigned long, void const*)

void __gnu_cxx::new_allocator<unsigned_char>::allocate(ulong param_1,void *param_2)

{
  void *pvVar1;
  
  pvVar1 = (void *)max_size();
  if (pvVar1 < param_2) {
    std::__throw_bad_alloc();
  }
  operator_new((ulong)param_2);
  return;
}



// unsigned char* std::__copy_move<false, true, std::random_access_iterator_tag>::__copy_m<unsigned
// char>(unsigned char const*, unsigned char const*, unsigned char*)

uchar * std::__copy_move<false,true,std::random_access_iterator_tag>::__copy_m_unsigned_char_
                  (uchar *param_1,uchar *param_2,uchar *param_3)

{
  uchar *__n;
  
  __n = param_2 + -(long)param_1;
  if (__n != (uchar *)0x0) {
    memmove(param_3,param_1,(size_t)__n);
  }
  return param_3 + (long)__n;
}



// WARNING: Unknown calling convention yet parameter storage is locked
// std::_Deque_iterator<unsigned char, unsigned char&, unsigned char*>
// std::copy<std::_Deque_iterator<unsigned char, unsigned char const&, unsigned char const*>,
// std::_Deque_iterator<unsigned char, unsigned char&, unsigned
// char*>>(std::_Deque_iterator<unsigned char, unsigned char const&, unsigned char const*>,
// std::_Deque_iterator<unsigned char, unsigned char const&, unsigned char const*>,
// std::_Deque_iterator<unsigned char, unsigned char&, unsigned char*>)

_Deque_iterator
std::
copy_std___Deque_iterator_unsigned_char_unsigned_char_const__unsigned_char_const___std___Deque_iterator_unsigned_char_unsigned_char__unsigned_char___
          (_Deque_iterator param_1,_Deque_iterator param_2,_Deque_iterator param_3)

{
  _Deque_iterator *in_RCX;
  undefined7 in_register_00000011;
  undefined7 in_register_00000031;
  long in_FS_OFFSET;
  _Deque_iterator_unsigned_char_unsigned_char_const__unsigned_char_const__ local_b8 [64];
  _Deque_iterator_unsigned_char_unsigned_char_const__unsigned_char_const__ local_78 [64];
  _Deque_iterator_unsigned_char_unsigned_char__unsigned_char__ local_38 [40];
  long local_10;
  
  local_10 = *(long *)(in_FS_OFFSET + 0x28);
  _Deque_iterator<unsigned_char,unsigned_char&,unsigned_char*>::_Deque_iterator(local_38,in_RCX);
  _Deque_iterator<unsigned_char,unsigned_char_const&,unsigned_char_const*>::_Deque_iterator
            (local_78,(_Deque_iterator *)CONCAT71(in_register_00000011,param_3));
  __miter_base_std___Deque_iterator_unsigned_char_unsigned_char_const__unsigned_char_const___
            ((_Deque_iterator)0xa8);
  _Deque_iterator<unsigned_char,unsigned_char_const&,unsigned_char_const*>::_Deque_iterator
            (local_b8,(_Deque_iterator *)CONCAT71(in_register_00000031,param_2));
  __miter_base_std___Deque_iterator_unsigned_char_unsigned_char_const__unsigned_char_const___
            ((_Deque_iterator)0x68);
  __copy_move_a2_false_std___Deque_iterator_unsigned_char_unsigned_char_const__unsigned_char_const___std___Deque_iterator_unsigned_char_unsigned_char__unsigned_char___
            (param_1,(_Deque_iterator)0x68,(_Deque_iterator)0xa8);
  if (local_10 != *(long *)(in_FS_OFFSET + 0x28)) {
                    // WARNING: Subroutine does not return
    __stack_chk_fail();
  }
  return param_1;
}



// std::move_iterator<std::_Deque_iterator<unsigned char, unsigned char&, unsigned
// char*>>::move_iterator(std::_Deque_iterator<unsigned char, unsigned char&, unsigned char*>)

void __thiscall
std::move_iterator<std::_Deque_iterator<unsigned_char,unsigned_char&,unsigned_char*>>::move_iterator
          (move_iterator_std___Deque_iterator_unsigned_char_unsigned_char__unsigned_char___ *this,
          _Deque_iterator param_1)

{
  undefined7 in_register_00000031;
  
  _Deque_iterator<unsigned_char,unsigned_char&,unsigned_char*>::_Deque_iterator
            ((_Deque_iterator_unsigned_char_unsigned_char__unsigned_char__ *)this,
             (_Deque_iterator *)CONCAT71(in_register_00000031,param_1));
  return;
}



// WARNING: Unknown calling convention yet parameter storage is locked
// std::_Deque_iterator<unsigned char, unsigned char&, unsigned char*>
// std::uninitialized_copy<std::move_iterator<std::_Deque_iterator<unsigned char, unsigned char&,
// unsigned char*>>, std::_Deque_iterator<unsigned char, unsigned char&, unsigned
// char*>>(std::move_iterator<std::_Deque_iterator<unsigned char, unsigned char&, unsigned char*>>,
// std::move_iterator<std::_Deque_iterator<unsigned char, unsigned char&, unsigned char*>>,
// std::_Deque_iterator<unsigned char, unsigned char&, unsigned char*>)

_Deque_iterator
std::
uninitialized_copy_std__move_iterator_std___Deque_iterator_unsigned_char_unsigned_char__unsigned_char____std___Deque_iterator_unsigned_char_unsigned_char__unsigned_char___
          (move_iterator param_1,move_iterator param_2,_Deque_iterator param_3)

{
  _Deque_iterator *in_RCX;
  undefined7 in_register_00000011;
  undefined4 in_register_00000034;
  undefined4 in_register_0000003c;
  long in_FS_OFFSET;
  move_iterator_std___Deque_iterator_unsigned_char_unsigned_char__unsigned_char___ local_78 [32];
  move_iterator_std___Deque_iterator_unsigned_char_unsigned_char__unsigned_char___ local_58 [32];
  _Deque_iterator_unsigned_char_unsigned_char__unsigned_char__ local_38 [40];
  long local_10;
  
  local_10 = *(long *)(in_FS_OFFSET + 0x28);
  _Deque_iterator<unsigned_char,unsigned_char&,unsigned_char*>::_Deque_iterator(local_38,in_RCX);
  move_iterator<std::_Deque_iterator<unsigned_char,unsigned_char&,unsigned_char*>>::move_iterator
            (local_58,(move_iterator *)CONCAT71(in_register_00000011,param_3));
  move_iterator<std::_Deque_iterator<unsigned_char,unsigned_char&,unsigned_char*>>::move_iterator
            (local_78,(move_iterator *)CONCAT44(in_register_00000034,param_2));
  __uninitialized_copy<true>::
  __uninit_copy_std__move_iterator_std___Deque_iterator_unsigned_char_unsigned_char__unsigned_char____std___Deque_iterator_unsigned_char_unsigned_char__unsigned_char___
            ((__uninitialized_copy_true_ *)CONCAT44(in_register_0000003c,param_1),
             (int)register0x00000020 - 0x78,(int)register0x00000020 - 0x58,(_Deque_iterator)0xc8);
  if (local_10 != *(long *)(in_FS_OFFSET + 0x28)) {
                    // WARNING: Subroutine does not return
    __stack_chk_fail();
  }
  return SUB41(param_1,0);
}



// WARNING: Unknown calling convention yet parameter storage is locked
// unsigned char* std::move<unsigned char*, unsigned char*>(unsigned char*, unsigned char*, unsigned
// char*)

uchar * std::move_unsigned_char__unsigned_char__(uchar *param_1,uchar *param_2,uchar *param_3)

{
  uchar *puVar1;
  uchar *puVar2;
  
  puVar1 = __miter_base_unsigned_char__(param_2);
  puVar2 = __miter_base_unsigned_char__(param_1);
  puVar1 = __copy_move_a2_true_unsigned_char__unsigned_char__(puVar2,puVar1,param_3);
  return puVar1;
}



// std::_Deque_iterator<unsigned char, unsigned char const&, unsigned char
// const*>::TEMPNAMEPLACEHOLDERVALUE()

_Deque_iterator_unsigned_char_unsigned_char_const__unsigned_char_const__ * __thiscall
std::_Deque_iterator<unsigned_char,unsigned_char_const&,unsigned_char_const*>::operator__
          (_Deque_iterator_unsigned_char_unsigned_char_const__unsigned_char_const__ *this)

{
  *(long *)this = *(long *)this + 1;
  if (*(long *)this == *(long *)(this + 0x10)) {
    _M_set_node(this,(uchar **)(*(long *)(this + 0x18) + 8));
    *(undefined8 *)this = *(undefined8 *)(this + 8);
  }
  return this;
}



// WARNING: Unknown calling convention yet parameter storage is locked
// void std::_Destroy<std::_Deque_iterator<unsigned char, unsigned char&, unsigned
// char*>>(std::_Deque_iterator<unsigned char, unsigned char&, unsigned char*>,
// std::_Deque_iterator<unsigned char, unsigned char&, unsigned char*>)

void std::_Destroy_std___Deque_iterator_unsigned_char_unsigned_char__unsigned_char___
               (_Deque_iterator param_1,_Deque_iterator param_2)

{
  undefined7 in_register_00000031;
  undefined7 in_register_00000039;
  long in_FS_OFFSET;
  _Deque_iterator_unsigned_char_unsigned_char__unsigned_char__ local_58 [32];
  _Deque_iterator_unsigned_char_unsigned_char__unsigned_char__ local_38 [40];
  long local_10;
  
  local_10 = *(long *)(in_FS_OFFSET + 0x28);
  _Deque_iterator<unsigned_char,unsigned_char&,unsigned_char*>::_Deque_iterator
            (local_38,(_Deque_iterator *)CONCAT71(in_register_00000031,param_2));
  _Deque_iterator<unsigned_char,unsigned_char&,unsigned_char*>::_Deque_iterator
            (local_58,(_Deque_iterator *)CONCAT71(in_register_00000039,param_1));
  _Destroy_aux<true>::__destroy_std___Deque_iterator_unsigned_char_unsigned_char__unsigned_char___
            ((_Deque_iterator)0xa8,(_Deque_iterator)0xc8);
  if (local_10 != *(long *)(in_FS_OFFSET + 0x28)) {
                    // WARNING: Subroutine does not return
    __stack_chk_fail();
  }
  return;
}



// WARNING: Unknown calling convention yet parameter storage is locked
// unsigned char* std::move_backward<unsigned char*, unsigned char*>(unsigned char*, unsigned char*,
// unsigned char*)

uchar * std::move_backward_unsigned_char__unsigned_char__
                  (uchar *param_1,uchar *param_2,uchar *param_3)

{
  uchar *puVar1;
  uchar *puVar2;
  
  puVar1 = __miter_base_unsigned_char__(param_2);
  puVar2 = __miter_base_unsigned_char__(param_1);
  puVar1 = __copy_move_backward_a2_true_unsigned_char__unsigned_char__(puVar2,puVar1,param_3);
  return puVar1;
}



// std::_Deque_iterator<unsigned char, unsigned char const&, unsigned char
// const*>::TEMPNAMEPLACEHOLDERVALUE(long)

void __thiscall
std::_Deque_iterator<unsigned_char,unsigned_char_const&,unsigned_char_const*>::operator__
          (_Deque_iterator_unsigned_char_unsigned_char_const__unsigned_char_const__ *this,
          long param_1)

{
  operator__(this,-param_1);
  return;
}



// __gnu_cxx::new_allocator<unsigned char*>::allocate(unsigned long, void const*)

void __gnu_cxx::new_allocator<unsigned_char*>::allocate(ulong param_1,void *param_2)

{
  void *pvVar1;
  
  pvVar1 = (void *)max_size();
  if (pvVar1 < param_2) {
    std::__throw_bad_alloc();
  }
  operator_new((long)param_2 << 3);
  return;
}



// __gnu_cxx::new_allocator<unsigned char>::max_size() const

undefined8 __gnu_cxx::new_allocator<unsigned_char>::max_size(void)

{
  return 0x7fffffffffffffff;
}



// WARNING: Unknown calling convention yet parameter storage is locked
// unsigned char** std::__miter_base<unsigned char**>(unsigned char**)

uchar ** std::__miter_base_unsigned_char___(uchar **param_1)

{
  return param_1;
}



// WARNING: Unknown calling convention yet parameter storage is locked
// unsigned char** std::__copy_move_a2<false, unsigned char**, unsigned char**>(unsigned char**,
// unsigned char**, unsigned char**)

uchar ** std::__copy_move_a2_false_unsigned_char___unsigned_char___
                   (uchar **param_1,uchar **param_2,uchar **param_3)

{
  uchar **ppuVar1;
  uchar **ppuVar2;
  uchar **ppuVar3;
  uchar **local_30;
  uchar **local_28;
  uchar **local_20;
  
  local_30 = param_3;
  local_28 = param_2;
  local_20 = param_1;
  ppuVar1 = __niter_base_unsigned_char___(param_3);
  ppuVar2 = __niter_base_unsigned_char___(local_28);
  ppuVar3 = __niter_base_unsigned_char___(local_20);
  ppuVar1 = __copy_move_a_false_unsigned_char___unsigned_char___(ppuVar3,ppuVar2,ppuVar1);
  ppuVar1 = __niter_wrap_unsigned_char___(&local_30,ppuVar1);
  return ppuVar1;
}



// WARNING: Unknown calling convention yet parameter storage is locked
// unsigned char** std::__copy_move_backward_a2<false, unsigned char**, unsigned char**>(unsigned
// char**, unsigned char**, unsigned char**)

uchar ** std::__copy_move_backward_a2_false_unsigned_char___unsigned_char___
                   (uchar **param_1,uchar **param_2,uchar **param_3)

{
  uchar **ppuVar1;
  uchar **ppuVar2;
  uchar **ppuVar3;
  uchar **local_30;
  uchar **local_28;
  uchar **local_20;
  
  local_30 = param_3;
  local_28 = param_2;
  local_20 = param_1;
  ppuVar1 = __niter_base_unsigned_char___(param_3);
  ppuVar2 = __niter_base_unsigned_char___(local_28);
  ppuVar3 = __niter_base_unsigned_char___(local_20);
  ppuVar1 = __copy_move_backward_a_false_unsigned_char___unsigned_char___(ppuVar3,ppuVar2,ppuVar1);
  ppuVar1 = __niter_wrap_unsigned_char___(&local_30,ppuVar1);
  return ppuVar1;
}



// WARNING: Unknown calling convention yet parameter storage is locked
// std::_Deque_iterator<unsigned char, unsigned char const&, unsigned char const*>
// std::__miter_base<std::_Deque_iterator<unsigned char, unsigned char const&, unsigned char
// const*>>(std::_Deque_iterator<unsigned char, unsigned char const&, unsigned char const*>)

_Deque_iterator
std::__miter_base_std___Deque_iterator_unsigned_char_unsigned_char_const__unsigned_char_const___
          (_Deque_iterator param_1)

{
  _Deque_iterator *in_RSI;
  undefined7 in_register_00000039;
  
  _Deque_iterator<unsigned_char,unsigned_char_const&,unsigned_char_const*>::_Deque_iterator
            ((_Deque_iterator_unsigned_char_unsigned_char_const__unsigned_char_const__ *)
             CONCAT71(in_register_00000039,param_1),in_RSI);
  return param_1;
}



// WARNING: Unknown calling convention yet parameter storage is locked
// std::_Deque_iterator<unsigned char, unsigned char&, unsigned char*> std::__copy_move_a2<false,
// std::_Deque_iterator<unsigned char, unsigned char const&, unsigned char const*>,
// std::_Deque_iterator<unsigned char, unsigned char&, unsigned
// char*>>(std::_Deque_iterator<unsigned char, unsigned char const&, unsigned char const*>,
// std::_Deque_iterator<unsigned char, unsigned char const&, unsigned char const*>,
// std::_Deque_iterator<unsigned char, unsigned char&, unsigned char*>)

_Deque_iterator
std::
__copy_move_a2_false_std___Deque_iterator_unsigned_char_unsigned_char_const__unsigned_char_const___std___Deque_iterator_unsigned_char_unsigned_char__unsigned_char___
          (_Deque_iterator param_1,_Deque_iterator param_2,_Deque_iterator param_3)

{
  _Deque_iterator *in_RCX;
  undefined7 in_register_00000011;
  undefined7 in_register_00000031;
  undefined7 in_register_00000039;
  long in_FS_OFFSET;
  _Deque_iterator_unsigned_char_unsigned_char_const__unsigned_char_const__ local_f8 [64];
  _Deque_iterator_unsigned_char_unsigned_char_const__unsigned_char_const__ local_b8 [64];
  _Deque_iterator_unsigned_char_unsigned_char__unsigned_char__ local_78 [104];
  long local_10;
  
  local_10 = *(long *)(in_FS_OFFSET + 0x28);
  _Deque_iterator<unsigned_char,unsigned_char&,unsigned_char*>::_Deque_iterator(local_78,in_RCX);
  __niter_base_std___Deque_iterator_unsigned_char_unsigned_char__unsigned_char___
            ((_Deque_iterator)0xa8);
  _Deque_iterator<unsigned_char,unsigned_char_const&,unsigned_char_const*>::_Deque_iterator
            (local_b8,(_Deque_iterator *)CONCAT71(in_register_00000011,param_3));
  __niter_base_std___Deque_iterator_unsigned_char_unsigned_char_const__unsigned_char_const___
            ((_Deque_iterator)0x68);
  _Deque_iterator<unsigned_char,unsigned_char_const&,unsigned_char_const*>::_Deque_iterator
            (local_f8,(_Deque_iterator *)CONCAT71(in_register_00000031,param_2));
  __niter_base_std___Deque_iterator_unsigned_char_unsigned_char_const__unsigned_char_const___
            ((_Deque_iterator)0x28);
  __copy_move_a_false_std___Deque_iterator_unsigned_char_unsigned_char_const__unsigned_char_const___std___Deque_iterator_unsigned_char_unsigned_char__unsigned_char___
            ((_Deque_iterator)0xc8,(_Deque_iterator)0x28,(_Deque_iterator)0x68);
  __niter_wrap_std___Deque_iterator_unsigned_char_unsigned_char__unsigned_char___
            ((_Deque_iterator *)CONCAT71(in_register_00000039,param_1),SUB81(in_RCX,0));
  if (local_10 != *(long *)(in_FS_OFFSET + 0x28)) {
                    // WARNING: Subroutine does not return
    __stack_chk_fail();
  }
  return param_1;
}



// std::_Deque_iterator<unsigned char, unsigned char&, unsigned char*>
// std::__uninitialized_copy<true>::__uninit_copy<std::move_iterator<std::_Deque_iterator<unsigned
// char, unsigned char&, unsigned char*>>, std::_Deque_iterator<unsigned char, unsigned char&,
// unsigned char*>>(std::move_iterator<std::_Deque_iterator<unsigned char, unsigned char&, unsigned
// char*>>, std::move_iterator<std::_Deque_iterator<unsigned char, unsigned char&, unsigned char*>>,
// std::_Deque_iterator<unsigned char, unsigned char&, unsigned char*>)

_Deque_iterator __thiscall
std::__uninitialized_copy<true>::
__uninit_copy_std__move_iterator_std___Deque_iterator_unsigned_char_unsigned_char__unsigned_char____std___Deque_iterator_unsigned_char_unsigned_char__unsigned_char___
          (__uninitialized_copy_true_ *this,move_iterator param_1,move_iterator param_2,
          _Deque_iterator param_3)

{
  undefined7 in_register_00000009;
  undefined4 in_register_00000014;
  undefined4 in_register_00000034;
  long in_FS_OFFSET;
  move_iterator_std___Deque_iterator_unsigned_char_unsigned_char__unsigned_char___ local_78 [32];
  move_iterator_std___Deque_iterator_unsigned_char_unsigned_char__unsigned_char___ local_58 [32];
  _Deque_iterator_unsigned_char_unsigned_char__unsigned_char__ local_38 [40];
  long local_10;
  
  local_10 = *(long *)(in_FS_OFFSET + 0x28);
  _Deque_iterator<unsigned_char,unsigned_char&,unsigned_char*>::_Deque_iterator
            (local_38,(_Deque_iterator *)CONCAT71(in_register_00000009,param_3));
  move_iterator<std::_Deque_iterator<unsigned_char,unsigned_char&,unsigned_char*>>::move_iterator
            (local_58,(move_iterator *)CONCAT44(in_register_00000014,param_2));
  move_iterator<std::_Deque_iterator<unsigned_char,unsigned_char&,unsigned_char*>>::move_iterator
            (local_78,(move_iterator *)CONCAT44(in_register_00000034,param_1));
  copy_std__move_iterator_std___Deque_iterator_unsigned_char_unsigned_char__unsigned_char____std___Deque_iterator_unsigned_char_unsigned_char__unsigned_char___
            ((move_iterator)this,(int)register0x00000020 - 0x78,(_Deque_iterator)0xa8);
  if (local_10 != *(long *)(in_FS_OFFSET + 0x28)) {
                    // WARNING: Subroutine does not return
    __stack_chk_fail();
  }
  return SUB81(this,0);
}



// WARNING: Unknown calling convention yet parameter storage is locked
// unsigned char* std::__copy_move_a2<true, unsigned char*, unsigned char*>(unsigned char*, unsigned
// char*, unsigned char*)

uchar * std::__copy_move_a2_true_unsigned_char__unsigned_char__
                  (uchar *param_1,uchar *param_2,uchar *param_3)

{
  uchar *puVar1;
  uchar *puVar2;
  uchar *puVar3;
  uchar *local_30;
  uchar *local_28;
  uchar *local_20;
  
  local_30 = param_3;
  local_28 = param_2;
  local_20 = param_1;
  puVar1 = __niter_base_unsigned_char__(param_3);
  puVar2 = __niter_base_unsigned_char__(local_28);
  puVar3 = __niter_base_unsigned_char__(local_20);
  puVar1 = __copy_move_a_true_unsigned_char__unsigned_char__(puVar3,puVar2,puVar1);
  puVar1 = __niter_wrap_unsigned_char__(&local_30,puVar1);
  return puVar1;
}



// void std::_Destroy_aux<true>::__destroy<std::_Deque_iterator<unsigned char, unsigned char&,
// unsigned char*>>(std::_Deque_iterator<unsigned char, unsigned char&, unsigned char*>,
// std::_Deque_iterator<unsigned char, unsigned char&, unsigned char*>)

void std::_Destroy_aux<true>::
     __destroy_std___Deque_iterator_unsigned_char_unsigned_char__unsigned_char___
               (_Deque_iterator param_1,_Deque_iterator param_2)

{
  return;
}



// WARNING: Unknown calling convention yet parameter storage is locked
// unsigned char* std::__copy_move_backward_a2<true, unsigned char*, unsigned char*>(unsigned char*,
// unsigned char*, unsigned char*)

uchar * std::__copy_move_backward_a2_true_unsigned_char__unsigned_char__
                  (uchar *param_1,uchar *param_2,uchar *param_3)

{
  uchar *puVar1;
  uchar *puVar2;
  uchar *puVar3;
  uchar *local_30;
  uchar *local_28;
  uchar *local_20;
  
  local_30 = param_3;
  local_28 = param_2;
  local_20 = param_1;
  puVar1 = __niter_base_unsigned_char__(param_3);
  puVar2 = __niter_base_unsigned_char__(local_28);
  puVar3 = __niter_base_unsigned_char__(local_20);
  puVar1 = __copy_move_backward_a_true_unsigned_char__unsigned_char__(puVar3,puVar2,puVar1);
  puVar1 = __niter_wrap_unsigned_char__(&local_30,puVar1);
  return puVar1;
}



// __gnu_cxx::new_allocator<unsigned char*>::max_size() const

undefined8 __gnu_cxx::new_allocator<unsigned_char*>::max_size(void)

{
  return 0xfffffffffffffff;
}



// WARNING: Unknown calling convention yet parameter storage is locked
// unsigned char** std::__niter_base<unsigned char**>(unsigned char**)

uchar ** std::__niter_base_unsigned_char___(uchar **param_1)

{
  return param_1;
}



// WARNING: Unknown calling convention yet parameter storage is locked
// unsigned char** std::__copy_move_a<false, unsigned char**, unsigned char**>(unsigned char**,
// unsigned char**, unsigned char**)

uchar ** std::__copy_move_a_false_unsigned_char___unsigned_char___
                   (uchar **param_1,uchar **param_2,uchar **param_3)

{
  uchar **ppuVar1;
  
  ppuVar1 = __copy_move<false,true,std::random_access_iterator_tag>::__copy_m_unsigned_char__
                      (param_1,param_2,param_3);
  return ppuVar1;
}



// WARNING: Unknown calling convention yet parameter storage is locked
// unsigned char** std::__niter_wrap<unsigned char**>(unsigned char** const&, unsigned char**)

uchar ** std::__niter_wrap_unsigned_char___(uchar ***param_1,uchar **param_2)

{
  return param_2;
}



// WARNING: Unknown calling convention yet parameter storage is locked
// unsigned char** std::__copy_move_backward_a<false, unsigned char**, unsigned char**>(unsigned
// char**, unsigned char**, unsigned char**)

uchar ** std::__copy_move_backward_a_false_unsigned_char___unsigned_char___
                   (uchar **param_1,uchar **param_2,uchar **param_3)

{
  uchar **ppuVar1;
  
  ppuVar1 = __copy_move_backward<false,true,std::random_access_iterator_tag>::
            __copy_move_b_unsigned_char__(param_1,param_2,param_3);
  return ppuVar1;
}



// WARNING: Unknown calling convention yet parameter storage is locked
// std::_Deque_iterator<unsigned char, unsigned char const&, unsigned char const*>
// std::__niter_base<std::_Deque_iterator<unsigned char, unsigned char const&, unsigned char
// const*>>(std::_Deque_iterator<unsigned char, unsigned char const&, unsigned char const*>)

_Deque_iterator
std::__niter_base_std___Deque_iterator_unsigned_char_unsigned_char_const__unsigned_char_const___
          (_Deque_iterator param_1)

{
  _Deque_iterator *in_RSI;
  undefined7 in_register_00000039;
  
  _Deque_iterator<unsigned_char,unsigned_char_const&,unsigned_char_const*>::_Deque_iterator
            ((_Deque_iterator_unsigned_char_unsigned_char_const__unsigned_char_const__ *)
             CONCAT71(in_register_00000039,param_1),in_RSI);
  return param_1;
}



// WARNING: Unknown calling convention yet parameter storage is locked
// std::_Deque_iterator<unsigned char, unsigned char&, unsigned char*>
// std::__niter_base<std::_Deque_iterator<unsigned char, unsigned char&, unsigned
// char*>>(std::_Deque_iterator<unsigned char, unsigned char&, unsigned char*>)

_Deque_iterator
std::__niter_base_std___Deque_iterator_unsigned_char_unsigned_char__unsigned_char___
          (_Deque_iterator param_1)

{
  _Deque_iterator *in_RSI;
  undefined7 in_register_00000039;
  
  _Deque_iterator<unsigned_char,unsigned_char&,unsigned_char*>::_Deque_iterator
            ((_Deque_iterator_unsigned_char_unsigned_char__unsigned_char__ *)
             CONCAT71(in_register_00000039,param_1),in_RSI);
  return param_1;
}



// WARNING: Unknown calling convention yet parameter storage is locked
// std::_Deque_iterator<unsigned char, unsigned char&, unsigned char*> std::__copy_move_a<false,
// std::_Deque_iterator<unsigned char, unsigned char const&, unsigned char const*>,
// std::_Deque_iterator<unsigned char, unsigned char&, unsigned
// char*>>(std::_Deque_iterator<unsigned char, unsigned char const&, unsigned char const*>,
// std::_Deque_iterator<unsigned char, unsigned char const&, unsigned char const*>,
// std::_Deque_iterator<unsigned char, unsigned char&, unsigned char*>)

_Deque_iterator
std::
__copy_move_a_false_std___Deque_iterator_unsigned_char_unsigned_char_const__unsigned_char_const___std___Deque_iterator_unsigned_char_unsigned_char__unsigned_char___
          (_Deque_iterator param_1,_Deque_iterator param_2,_Deque_iterator param_3)

{
  _Deque_iterator *in_RCX;
  undefined7 in_register_00000011;
  undefined7 in_register_00000031;
  undefined7 in_register_00000039;
  long in_FS_OFFSET;
  _Deque_iterator_unsigned_char_unsigned_char_const__unsigned_char_const__ local_78 [32];
  _Deque_iterator_unsigned_char_unsigned_char_const__unsigned_char_const__ local_58 [32];
  _Deque_iterator_unsigned_char_unsigned_char__unsigned_char__ local_38 [40];
  long local_10;
  
  local_10 = *(long *)(in_FS_OFFSET + 0x28);
  _Deque_iterator<unsigned_char,unsigned_char&,unsigned_char*>::_Deque_iterator(local_38,in_RCX);
  _Deque_iterator<unsigned_char,unsigned_char_const&,unsigned_char_const*>::_Deque_iterator
            (local_58,(_Deque_iterator *)CONCAT71(in_register_00000011,param_3));
  _Deque_iterator<unsigned_char,unsigned_char_const&,unsigned_char_const*>::_Deque_iterator
            (local_78,(_Deque_iterator *)CONCAT71(in_register_00000031,param_2));
  __copy_move<false,false,std::random_access_iterator_tag>::
  __copy_m_std___Deque_iterator_unsigned_char_unsigned_char_const__unsigned_char_const___std___Deque_iterator_unsigned_char_unsigned_char__unsigned_char___
            ((__copy_move_false_false_std__random_access_iterator_tag_ *)
             CONCAT71(in_register_00000039,param_1),(_Deque_iterator)0x88,(_Deque_iterator)0xa8,
             (_Deque_iterator)0xc8);
  if (local_10 != *(long *)(in_FS_OFFSET + 0x28)) {
                    // WARNING: Subroutine does not return
    __stack_chk_fail();
  }
  return param_1;
}



// WARNING: Unknown calling convention yet parameter storage is locked
// std::_Deque_iterator<unsigned char, unsigned char&, unsigned char*>
// std::__niter_wrap<std::_Deque_iterator<unsigned char, unsigned char&, unsigned
// char*>>(std::_Deque_iterator<unsigned char, unsigned char&, unsigned char*> const&,
// std::_Deque_iterator<unsigned char, unsigned char&, unsigned char*>)

_Deque_iterator
std::__niter_wrap_std___Deque_iterator_unsigned_char_unsigned_char__unsigned_char___
          (_Deque_iterator *param_1,_Deque_iterator param_2)

{
  _Deque_iterator *in_RDX;
  
  _Deque_iterator<unsigned_char,unsigned_char&,unsigned_char*>::_Deque_iterator
            ((_Deque_iterator_unsigned_char_unsigned_char__unsigned_char__ *)param_1,in_RDX);
  return SUB81(param_1,0);
}



// WARNING: Unknown calling convention yet parameter storage is locked
// std::_Deque_iterator<unsigned char, unsigned char&, unsigned char*>
// std::copy<std::move_iterator<std::_Deque_iterator<unsigned char, unsigned char&, unsigned
// char*>>, std::_Deque_iterator<unsigned char, unsigned char&, unsigned
// char*>>(std::move_iterator<std::_Deque_iterator<unsigned char, unsigned char&, unsigned char*>>,
// std::move_iterator<std::_Deque_iterator<unsigned char, unsigned char&, unsigned char*>>,
// std::_Deque_iterator<unsigned char, unsigned char&, unsigned char*>)

_Deque_iterator
std::
copy_std__move_iterator_std___Deque_iterator_unsigned_char_unsigned_char__unsigned_char____std___Deque_iterator_unsigned_char_unsigned_char__unsigned_char___
          (move_iterator param_1,move_iterator param_2,_Deque_iterator param_3)

{
  _Deque_iterator *in_RCX;
  undefined7 in_register_00000011;
  undefined4 in_register_00000034;
  long in_FS_OFFSET;
  move_iterator_std___Deque_iterator_unsigned_char_unsigned_char__unsigned_char___ local_b8 [64];
  move_iterator_std___Deque_iterator_unsigned_char_unsigned_char__unsigned_char___ local_78 [64];
  _Deque_iterator_unsigned_char_unsigned_char__unsigned_char__ local_38 [40];
  long local_10;
  
  local_10 = *(long *)(in_FS_OFFSET + 0x28);
  _Deque_iterator<unsigned_char,unsigned_char&,unsigned_char*>::_Deque_iterator(local_38,in_RCX);
  move_iterator<std::_Deque_iterator<unsigned_char,unsigned_char&,unsigned_char*>>::move_iterator
            (local_78,(move_iterator *)CONCAT71(in_register_00000011,param_3));
                    // try { // try from 0010a179 to 0010a17d has its CatchHandler @ 0010a1d6
  __miter_base_std___Deque_iterator_unsigned_char_unsigned_char__unsigned_char___
            ((int)register0x00000020 - 0x58);
  move_iterator<std::_Deque_iterator<unsigned_char,unsigned_char&,unsigned_char*>>::move_iterator
            (local_b8,(move_iterator *)CONCAT44(in_register_00000034,param_2));
                    // try { // try from 0010a1ab to 0010a1cd has its CatchHandler @ 0010a1d0
  __miter_base_std___Deque_iterator_unsigned_char_unsigned_char__unsigned_char___
            ((int)register0x00000020 - 0x98);
  __copy_move_a2_true_std___Deque_iterator_unsigned_char_unsigned_char__unsigned_char___std___Deque_iterator_unsigned_char_unsigned_char__unsigned_char___
            (SUB41(param_1,0),(_Deque_iterator)0x68,(_Deque_iterator)0xa8);
  if (local_10 != *(long *)(in_FS_OFFSET + 0x28)) {
                    // WARNING: Subroutine does not return
    __stack_chk_fail();
  }
  return SUB41(param_1,0);
}



// WARNING: Unknown calling convention yet parameter storage is locked
// unsigned char* std::__copy_move_a<true, unsigned char*, unsigned char*>(unsigned char*, unsigned
// char*, unsigned char*)

uchar * std::__copy_move_a_true_unsigned_char__unsigned_char__
                  (uchar *param_1,uchar *param_2,uchar *param_3)

{
  uchar *puVar1;
  
  puVar1 = __copy_move<true,true,std::random_access_iterator_tag>::__copy_m_unsigned_char_
                     (param_1,param_2,param_3);
  return puVar1;
}



// WARNING: Unknown calling convention yet parameter storage is locked
// unsigned char* std::__copy_move_backward_a<true, unsigned char*, unsigned char*>(unsigned char*,
// unsigned char*, unsigned char*)

uchar * std::__copy_move_backward_a_true_unsigned_char__unsigned_char__
                  (uchar *param_1,uchar *param_2,uchar *param_3)

{
  uchar *puVar1;
  
  puVar1 = __copy_move_backward<true,true,std::random_access_iterator_tag>::
           __copy_move_b_unsigned_char_(param_1,param_2,param_3);
  return puVar1;
}



// unsigned char** std::__copy_move<false, true, std::random_access_iterator_tag>::__copy_m<unsigned
// char*>(unsigned char* const*, unsigned char* const*, unsigned char**)

uchar ** std::__copy_move<false,true,std::random_access_iterator_tag>::__copy_m_unsigned_char__
                   (uchar **param_1,uchar **param_2,uchar **param_3)

{
  long lVar1;
  
  lVar1 = (long)((long)param_2 - (long)param_1) >> 3;
  if (lVar1 != 0) {
    memmove(param_3,param_1,lVar1 * 8);
  }
  return param_3 + lVar1;
}



// unsigned char** std::__copy_move_backward<false, true,
// std::random_access_iterator_tag>::__copy_move_b<unsigned char*>(unsigned char* const*, unsigned
// char* const*, unsigned char**)

uchar ** std::__copy_move_backward<false,true,std::random_access_iterator_tag>::
         __copy_move_b_unsigned_char__(uchar **param_1,uchar **param_2,uchar **param_3)

{
  long lVar1;
  
  lVar1 = (long)((long)param_2 - (long)param_1) >> 3;
  if (lVar1 != 0) {
    memmove(param_3 + -lVar1,param_1,lVar1 * 8);
  }
  return param_3 + -lVar1;
}



// std::_Deque_iterator<unsigned char, unsigned char&, unsigned char*> std::__copy_move<false,
// false, std::random_access_iterator_tag>::__copy_m<std::_Deque_iterator<unsigned char, unsigned
// char const&, unsigned char const*>, std::_Deque_iterator<unsigned char, unsigned char&, unsigned
// char*>>(std::_Deque_iterator<unsigned char, unsigned char const&, unsigned char const*>,
// std::_Deque_iterator<unsigned char, unsigned char const&, unsigned char const*>,
// std::_Deque_iterator<unsigned char, unsigned char&, unsigned char*>)

_Deque_iterator __thiscall
std::__copy_move<false,false,std::random_access_iterator_tag>::
__copy_m_std___Deque_iterator_unsigned_char_unsigned_char_const__unsigned_char_const___std___Deque_iterator_unsigned_char_unsigned_char__unsigned_char___
          (__copy_move_false_false_std__random_access_iterator_tag_ *this,_Deque_iterator param_1,
          _Deque_iterator param_2,_Deque_iterator param_3)

{
  _Deque_iterator_unsigned_char_unsigned_char__unsigned_char__ *this_00;
  difference_type dVar1;
  undefined4 extraout_var;
  undefined *puVar2;
  undefined *puVar3;
  undefined7 in_register_00000009;
  undefined7 in_register_00000011;
  undefined7 in_register_00000031;
  _Deque_iterator *this_01;
  long local_20;
  
  this_01 = (_Deque_iterator *)CONCAT71(in_register_00000031,param_1);
  this_00 = (_Deque_iterator_unsigned_char_unsigned_char__unsigned_char__ *)
            CONCAT71(in_register_00000009,param_3);
  dVar1 = operator_((_Deque_iterator *)CONCAT71(in_register_00000011,param_2),this_01);
  for (local_20 = CONCAT44(extraout_var,dVar1); 0 < local_20; local_20 = local_20 + -1) {
    puVar2 = (undefined *)
             _Deque_iterator<unsigned_char,unsigned_char_const&,unsigned_char_const*>::operator_
                       ((_Deque_iterator_unsigned_char_unsigned_char_const__unsigned_char_const__ *)
                        this_01);
    puVar3 = (undefined *)
             _Deque_iterator<unsigned_char,unsigned_char&,unsigned_char*>::operator_(this_00);
    *puVar3 = *puVar2;
    _Deque_iterator<unsigned_char,unsigned_char_const&,unsigned_char_const*>::operator__
              ((_Deque_iterator_unsigned_char_unsigned_char_const__unsigned_char_const__ *)this_01);
    _Deque_iterator<unsigned_char,unsigned_char&,unsigned_char*>::operator__(this_00);
  }
  _Deque_iterator<unsigned_char,unsigned_char&,unsigned_char*>::_Deque_iterator
            ((_Deque_iterator_unsigned_char_unsigned_char__unsigned_char__ *)this,
             (_Deque_iterator *)this_00);
  return SUB81(this,0);
}



// WARNING: Unknown calling convention yet parameter storage is locked
// decltype (__miter_base(({parm#1}.base)())) std::__miter_base<std::_Deque_iterator<unsigned char,
// unsigned char&, unsigned char*>>(std::move_iterator<std::_Deque_iterator<unsigned char, unsigned
// char&, unsigned char*>>)

undefined8
std::__miter_base_std___Deque_iterator_unsigned_char_unsigned_char__unsigned_char___
          (move_iterator param_1)

{
  long lVar1;
  undefined4 in_register_0000003c;
  long in_FS_OFFSET;
  
  lVar1 = *(long *)(in_FS_OFFSET + 0x28);
  move_iterator<std::_Deque_iterator<unsigned_char,unsigned_char&,unsigned_char*>>::base();
  __miter_base_std___Deque_iterator_unsigned_char_unsigned_char__unsigned_char___(SUB41(param_1,0));
  if (lVar1 != *(long *)(in_FS_OFFSET + 0x28)) {
                    // WARNING: Subroutine does not return
    __stack_chk_fail();
  }
  return CONCAT44(in_register_0000003c,param_1);
}



// WARNING: Unknown calling convention yet parameter storage is locked
// std::_Deque_iterator<unsigned char, unsigned char&, unsigned char*> std::__copy_move_a2<true,
// std::_Deque_iterator<unsigned char, unsigned char&, unsigned char*>,
// std::_Deque_iterator<unsigned char, unsigned char&, unsigned
// char*>>(std::_Deque_iterator<unsigned char, unsigned char&, unsigned char*>,
// std::_Deque_iterator<unsigned char, unsigned char&, unsigned char*>,
// std::_Deque_iterator<unsigned char, unsigned char&, unsigned char*>)

_Deque_iterator
std::
__copy_move_a2_true_std___Deque_iterator_unsigned_char_unsigned_char__unsigned_char___std___Deque_iterator_unsigned_char_unsigned_char__unsigned_char___
          (_Deque_iterator param_1,_Deque_iterator param_2,_Deque_iterator param_3)

{
  _Deque_iterator *in_RCX;
  undefined7 in_register_00000011;
  undefined7 in_register_00000031;
  undefined7 in_register_00000039;
  long in_FS_OFFSET;
  _Deque_iterator_unsigned_char_unsigned_char__unsigned_char__ local_f8 [64];
  _Deque_iterator_unsigned_char_unsigned_char__unsigned_char__ local_b8 [64];
  _Deque_iterator_unsigned_char_unsigned_char__unsigned_char__ local_78 [104];
  long local_10;
  
  local_10 = *(long *)(in_FS_OFFSET + 0x28);
  _Deque_iterator<unsigned_char,unsigned_char&,unsigned_char*>::_Deque_iterator(local_78,in_RCX);
  __niter_base_std___Deque_iterator_unsigned_char_unsigned_char__unsigned_char___
            ((_Deque_iterator)0xa8);
  _Deque_iterator<unsigned_char,unsigned_char&,unsigned_char*>::_Deque_iterator
            (local_b8,(_Deque_iterator *)CONCAT71(in_register_00000011,param_3));
  __niter_base_std___Deque_iterator_unsigned_char_unsigned_char__unsigned_char___
            ((_Deque_iterator)0x68);
  _Deque_iterator<unsigned_char,unsigned_char&,unsigned_char*>::_Deque_iterator
            (local_f8,(_Deque_iterator *)CONCAT71(in_register_00000031,param_2));
  __niter_base_std___Deque_iterator_unsigned_char_unsigned_char__unsigned_char___
            ((_Deque_iterator)0x28);
  __copy_move_a_true_std___Deque_iterator_unsigned_char_unsigned_char__unsigned_char___std___Deque_iterator_unsigned_char_unsigned_char__unsigned_char___
            ((_Deque_iterator)0xc8,(_Deque_iterator)0x28,(_Deque_iterator)0x68);
  __niter_wrap_std___Deque_iterator_unsigned_char_unsigned_char__unsigned_char___
            ((_Deque_iterator *)CONCAT71(in_register_00000039,param_1),SUB81(in_RCX,0));
  if (local_10 != *(long *)(in_FS_OFFSET + 0x28)) {
                    // WARNING: Subroutine does not return
    __stack_chk_fail();
  }
  return param_1;
}



// unsigned char* std::__copy_move<true, true, std::random_access_iterator_tag>::__copy_m<unsigned
// char>(unsigned char const*, unsigned char const*, unsigned char*)

uchar * std::__copy_move<true,true,std::random_access_iterator_tag>::__copy_m_unsigned_char_
                  (uchar *param_1,uchar *param_2,uchar *param_3)

{
  uchar *__n;
  
  __n = param_2 + -(long)param_1;
  if (__n != (uchar *)0x0) {
    memmove(param_3,param_1,(size_t)__n);
  }
  return param_3 + (long)__n;
}



// unsigned char* std::__copy_move_backward<true, true,
// std::random_access_iterator_tag>::__copy_move_b<unsigned char>(unsigned char const*, unsigned
// char const*, unsigned char*)

uchar * std::__copy_move_backward<true,true,std::random_access_iterator_tag>::
        __copy_move_b_unsigned_char_(uchar *param_1,uchar *param_2,uchar *param_3)

{
  uchar *__n;
  
  __n = param_2 + -(long)param_1;
  if (__n != (uchar *)0x0) {
    memmove(param_3 + -(long)__n,param_1,(size_t)__n);
  }
  return param_3 + -(long)__n;
}



// std::_Deque_iterator<unsigned char, unsigned char const&, unsigned char
// const*>::TEMPNAMEPLACEHOLDERVALUE() const

undefined8 __thiscall
std::_Deque_iterator<unsigned_char,unsigned_char_const&,unsigned_char_const*>::operator_
          (_Deque_iterator_unsigned_char_unsigned_char_const__unsigned_char_const__ *this)

{
  return *(undefined8 *)this;
}



// std::_Deque_iterator<unsigned char, unsigned char&, unsigned char*>::TEMPNAMEPLACEHOLDERVALUE()

_Deque_iterator_unsigned_char_unsigned_char__unsigned_char__ * __thiscall
std::_Deque_iterator<unsigned_char,unsigned_char&,unsigned_char*>::operator__
          (_Deque_iterator_unsigned_char_unsigned_char__unsigned_char__ *this)

{
  *(long *)this = *(long *)this + 1;
  if (*(long *)this == *(long *)(this + 0x10)) {
    _M_set_node(this,(uchar **)(*(long *)(this + 0x18) + 8));
    *(undefined8 *)this = *(undefined8 *)(this + 8);
  }
  return this;
}



// std::move_iterator<std::_Deque_iterator<unsigned char, unsigned char&, unsigned char*>>::base()
// const

_Deque_iterator_unsigned_char_unsigned_char__unsigned_char__ *
std::move_iterator<std::_Deque_iterator<unsigned_char,unsigned_char&,unsigned_char*>>::base(void)

{
  _Deque_iterator *in_RSI;
  _Deque_iterator_unsigned_char_unsigned_char__unsigned_char__ *in_RDI;
  
  _Deque_iterator<unsigned_char,unsigned_char&,unsigned_char*>::_Deque_iterator(in_RDI,in_RSI);
  return in_RDI;
}



// WARNING: Unknown calling convention yet parameter storage is locked
// std::_Deque_iterator<unsigned char, unsigned char&, unsigned char*>
// std::__miter_base<std::_Deque_iterator<unsigned char, unsigned char&, unsigned
// char*>>(std::_Deque_iterator<unsigned char, unsigned char&, unsigned char*>)

_Deque_iterator
std::__miter_base_std___Deque_iterator_unsigned_char_unsigned_char__unsigned_char___
          (_Deque_iterator param_1)

{
  _Deque_iterator *in_RSI;
  undefined7 in_register_00000039;
  
  _Deque_iterator<unsigned_char,unsigned_char&,unsigned_char*>::_Deque_iterator
            ((_Deque_iterator_unsigned_char_unsigned_char__unsigned_char__ *)
             CONCAT71(in_register_00000039,param_1),in_RSI);
  return param_1;
}



// WARNING: Unknown calling convention yet parameter storage is locked
// std::_Deque_iterator<unsigned char, unsigned char&, unsigned char*> std::__copy_move_a<true,
// std::_Deque_iterator<unsigned char, unsigned char&, unsigned char*>,
// std::_Deque_iterator<unsigned char, unsigned char&, unsigned
// char*>>(std::_Deque_iterator<unsigned char, unsigned char&, unsigned char*>,
// std::_Deque_iterator<unsigned char, unsigned char&, unsigned char*>,
// std::_Deque_iterator<unsigned char, unsigned char&, unsigned char*>)

_Deque_iterator
std::
__copy_move_a_true_std___Deque_iterator_unsigned_char_unsigned_char__unsigned_char___std___Deque_iterator_unsigned_char_unsigned_char__unsigned_char___
          (_Deque_iterator param_1,_Deque_iterator param_2,_Deque_iterator param_3)

{
  _Deque_iterator *in_RCX;
  undefined7 in_register_00000011;
  undefined7 in_register_00000031;
  undefined7 in_register_00000039;
  long in_FS_OFFSET;
  _Deque_iterator_unsigned_char_unsigned_char__unsigned_char__ local_78 [32];
  _Deque_iterator_unsigned_char_unsigned_char__unsigned_char__ local_58 [32];
  _Deque_iterator_unsigned_char_unsigned_char__unsigned_char__ local_38 [40];
  long local_10;
  
  local_10 = *(long *)(in_FS_OFFSET + 0x28);
  _Deque_iterator<unsigned_char,unsigned_char&,unsigned_char*>::_Deque_iterator(local_38,in_RCX);
  _Deque_iterator<unsigned_char,unsigned_char&,unsigned_char*>::_Deque_iterator
            (local_58,(_Deque_iterator *)CONCAT71(in_register_00000011,param_3));
  _Deque_iterator<unsigned_char,unsigned_char&,unsigned_char*>::_Deque_iterator
            (local_78,(_Deque_iterator *)CONCAT71(in_register_00000031,param_2));
  __copy_move<true,false,std::random_access_iterator_tag>::
  __copy_m_std___Deque_iterator_unsigned_char_unsigned_char__unsigned_char___std___Deque_iterator_unsigned_char_unsigned_char__unsigned_char___
            ((__copy_move_true_false_std__random_access_iterator_tag_ *)
             CONCAT71(in_register_00000039,param_1),(_Deque_iterator)0x88,(_Deque_iterator)0xa8,
             (_Deque_iterator)0xc8);
  if (local_10 != *(long *)(in_FS_OFFSET + 0x28)) {
                    // WARNING: Subroutine does not return
    __stack_chk_fail();
  }
  return param_1;
}



// std::_Deque_iterator<unsigned char, unsigned char&, unsigned char*> std::__copy_move<true, false,
// std::random_access_iterator_tag>::__copy_m<std::_Deque_iterator<unsigned char, unsigned char&,
// unsigned char*>, std::_Deque_iterator<unsigned char, unsigned char&, unsigned
// char*>>(std::_Deque_iterator<unsigned char, unsigned char&, unsigned char*>,
// std::_Deque_iterator<unsigned char, unsigned char&, unsigned char*>,
// std::_Deque_iterator<unsigned char, unsigned char&, unsigned char*>)

_Deque_iterator __thiscall
std::__copy_move<true,false,std::random_access_iterator_tag>::
__copy_m_std___Deque_iterator_unsigned_char_unsigned_char__unsigned_char___std___Deque_iterator_unsigned_char_unsigned_char__unsigned_char___
          (__copy_move_true_false_std__random_access_iterator_tag_ *this,_Deque_iterator param_1,
          _Deque_iterator param_2,_Deque_iterator param_3)

{
  _Deque_iterator_unsigned_char_unsigned_char__unsigned_char__ *this_00;
  difference_type dVar1;
  undefined4 extraout_var;
  uchar *puVar2;
  type *ptVar3;
  type *ptVar4;
  undefined7 in_register_00000009;
  undefined7 in_register_00000011;
  undefined7 in_register_00000031;
  _Deque_iterator *this_01;
  long local_20;
  
  this_01 = (_Deque_iterator *)CONCAT71(in_register_00000031,param_1);
  this_00 = (_Deque_iterator_unsigned_char_unsigned_char__unsigned_char__ *)
            CONCAT71(in_register_00000009,param_3);
  dVar1 = operator_((_Deque_iterator *)CONCAT71(in_register_00000011,param_2),this_01);
  for (local_20 = CONCAT44(extraout_var,dVar1); 0 < local_20; local_20 = local_20 + -1) {
    puVar2 = (uchar *)_Deque_iterator<unsigned_char,unsigned_char&,unsigned_char*>::operator_
                                ((_Deque_iterator_unsigned_char_unsigned_char__unsigned_char__ *)
                                 this_01);
    ptVar3 = move_unsigned_char__(puVar2);
    ptVar4 = (type *)_Deque_iterator<unsigned_char,unsigned_char&,unsigned_char*>::operator_
                               (this_00);
    *ptVar4 = *ptVar3;
    _Deque_iterator<unsigned_char,unsigned_char&,unsigned_char*>::operator__
              ((_Deque_iterator_unsigned_char_unsigned_char__unsigned_char__ *)this_01);
    _Deque_iterator<unsigned_char,unsigned_char&,unsigned_char*>::operator__(this_00);
  }
  _Deque_iterator<unsigned_char,unsigned_char&,unsigned_char*>::_Deque_iterator
            ((_Deque_iterator_unsigned_char_unsigned_char__unsigned_char__ *)this,
             (_Deque_iterator *)this_00);
  return SUB81(this,0);
}



// WARNING: Unknown calling convention yet parameter storage is locked
// std::remove_reference<unsigned char&>::type&& std::move<unsigned char&>(unsigned char&)

type * std::move_unsigned_char__(uchar *param_1)

{
  return (type *)param_1;
}



void __libc_csu_init(EVP_PKEY_CTX *param_1,undefined8 param_2,undefined8 param_3)

{
  long lVar1;
  
  _init(param_1);
  lVar1 = 0;
  do {
    (*(code *)(&__frame_dummy_init_array_entry)[lVar1])((ulong)param_1 & 0xffffffff,param_2,param_3)
    ;
    lVar1 = lVar1 + 1;
  } while (lVar1 != 2);
  return;
}



void __libc_csu_fini(void)

{
  return;
}



void _fini(void)

{
  return;
}


