## Groundead

A spacecraft needs to be put into emergency mode.  Issue the correct commands to save it from crashing.

```
     .-.                                                         -----------
   (;;;)                                                        /            '
    |_|                                  ,-------,-|           |C>      )    |
      ' _.--l--._                       '      ,','|          /    || ,'     |
     .      |     `.                   '-----,','  |         (,    ||        ,
   .` `.    |    .` `.                 |     ||    |           -- ||||      |
 .`     `   |  .`     `.               |     ||    |           |||||||     _|
' __      `.|.`      __ `              |     ||    |______      `````|____/ |
|   ''--._  V  _.--''   |              |     ||    |     ,|         _/_____/ |
|        _ ( ) _        |              |     ||  ,'    ,' |        /          |
| __..--'   ^   '--..__ | _           '|     ||,'    ,'   |       |            |
'         .`|`.         '-.)        ,  |_____|'    ,'     |       /           | |
 `.     .`  |  `.     .`           , _____________,'      ,',_____|      |    | |
   `. .`    |    `. .`             |             |     ,','      |      |    | |
     `._    |    _.`|       -------|             |   ,','    ____|_____/    /  |
         `--l--`  | |     ;        |             | ,','  /  |              /   |
                  | |    ;         |_____________|','    |   -------------/   |
               ---| |-------.                      |===========,'
             /    ..       / |                                   
            --------------   |
           |              | /
           |              |/
            --------------


Ground Station ONLINE
```

### Running

The `challenge` binary can be run locally in an Ubuntu 20.04 environment.  Alternatively, build the Docker container with

```bash
docker build -t groundead .
```

and run it with

```
docker run --rm -it groundead:latest
```

### Code

Deocmpiled code exported from [Ghidra](https://ghidra-sre.org/) can be found in `challenge.c`.