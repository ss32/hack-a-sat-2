
King's Ransom 2
Presents from Marco, 223 points

Back to Scoreboard

A vulnerable service with your "bank account" information running on the target system. Too bad it has already been exploited by a piece of ransomware. The ransomware took over the target, encrypted some files on the file system, and resumed the executive loop.

Go farther.

Ticket

Present this ticket when connecting to the challenge:
ticket{tango21782golf2:GJTrEiF0c4up1vl3qPmPSbLxogkG9sa4Ve7WnzwVfhy8SyJXhOlE1quvDJgpmriRaw}
Don't share your ticket with other teams.
Connecting

Connect to the challenge on:
star-power.satellitesabove.me:5011

Using netcat, you might run:
nc star-power.satellitesabove.me 5011 