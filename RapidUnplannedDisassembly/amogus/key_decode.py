# -*- coding: utf-8 -*-
"""
Created on Sat Jun 26 23:26:46 2021

@author: mason
"""
from Crypto.Cipher import PKCS1_OAEP
from Crypto.PublicKey import RSA
#import binascii

def decode_rsa(ciphertext):
    key = RSA.importKey("05f1e0f77ce8fbb75532e8023056e4e6")#open(key_path).read())
    cipher = PKCS1_OAEP.new(key)
    # before decrypt convert the hex string to byte_array 
    message = cipher.decrypt(bytearray.fromhex(ciphertext))
    return message

ciphertext = "00000000ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff200000008b37328ffcc1ef3f739d465a2a6fbf3f00000000000000000000000000000000"
#cipher_bytes = binascii.hexlify(ciphertext.encode())
print(decode_rsa(ciphertext))