## Grade F Prime Beef
Rapid Unplanned Disassembly, 84 points

Back to Scoreboard

Exploit the system, get the passcode, retrieve the flag.


### Connecting

Connect to the challenge on:
recent-puzzle.satellitesabove.me:5015

Using netcat, you might run:
nc recent-puzzle.satellitesabove.me 5015


```
Starting spacecraft
Starting ground station
Challenge Web Page Starting at http://18.222.93.58:10850
CLI available at 18.222.93.58:10851
Time remaining to solve: 900 seconds
```