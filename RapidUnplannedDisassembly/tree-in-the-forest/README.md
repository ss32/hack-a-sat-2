## Rapid Unplanned Disassembly, 31 points

Back to Scoreboard

CC=g++-9.3.0

challenge: src/parser.c
    $(CC) src/parser.c -o $@

Connecting

Connect to the challenge on:
lucky-tree.satellitesabove.me:5008

Using netcat, you might run:
nc lucky-tree.satellitesabove.me 5008 